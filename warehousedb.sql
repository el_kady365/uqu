-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 23, 2017 at 02:07 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `uqu_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `add_orders_items_form_7`
--

CREATE TABLE `add_orders_items_form_7` (
  `id` int(11) NOT NULL,
  `order_item_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order_item_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order_item_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order_item_category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order_item_count` int(11) NOT NULL,
  `order_item_amount_recieved` int(11) NOT NULL,
  `order_item_price` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order_total_price` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notes` text COLLATE utf8_unicode_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `add_orders_items_form_7`
--

INSERT INTO `add_orders_items_form_7` (`id`, `order_item_number`, `order_item_name`, `order_item_type`, `order_item_category`, `order_item_count`, `order_item_amount_recieved`, `order_item_price`, `order_total_price`, `notes`, `created`, `updated`) VALUES
(3, '#3', 'شاشة Dell', 'مستديم', 'حاسب الي', 3, 1, '1200', '1222', '--', '2017-03-23 12:05:34', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `recieved_orders_items_form7`
--

CREATE TABLE `recieved_orders_items_form7` (
  `id` int(11) NOT NULL,
  `order_item_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order_item_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order_item_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order_item_category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order_item_count` int(11) NOT NULL,
  `order_item_amount_recieved` int(11) NOT NULL,
  `order_item_price` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `order_total_price` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `notes` text COLLATE utf8_unicode_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `recieved_orders_items_form7`
--

INSERT INTO `recieved_orders_items_form7` (`id`, `order_item_number`, `order_item_name`, `order_item_type`, `order_item_category`, `order_item_count`, `order_item_amount_recieved`, `order_item_price`, `order_total_price`, `notes`, `created`, `updated`) VALUES
(1, '#3', 'اسم الصنف ', 'نوع الصنف', 'الوحده', 3, 3, '1299 ', '1299', '--', '2017-03-23 13:02:36', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `warehouse_categories_items`
--

CREATE TABLE `warehouse_categories_items` (
  `id` int(11) NOT NULL,
  `category_item_number` int(11) NOT NULL,
  `category_item_name` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `warehouse_categories_items`
--

INSERT INTO `warehouse_categories_items` (`id`, `category_item_number`, `category_item_name`, `created`, `updated`) VALUES
(1, 33, 'أجهزة حاسب آلي', '2017-03-21 09:01:24', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `warehouse_custody_employers`
--

CREATE TABLE `warehouse_custody_employers` (
  `id` int(11) NOT NULL,
  `custody_item_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custody_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custody_serial` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custody_state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custody_notes` text COLLATE utf8_unicode_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `warehouse_custody_employers`
--

INSERT INTO `warehouse_custody_employers` (`id`, `custody_item_name`, `custody_number`, `custody_serial`, `custody_state`, `custody_notes`, `created`, `updated`) VALUES
(1, 'شاشة Dell', '12399867', '12399867e', 'مفقود', 'ملاحظات', '2017-03-21 14:48:52', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `warehouse_employers`
--

CREATE TABLE `warehouse_employers` (
  `id` int(11) NOT NULL,
  `emp_number` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `emp_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `emp_code` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `emp_management` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `emp_site` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `warehouse_employers`
--

INSERT INTO `warehouse_employers` (`id`, `emp_number`, `emp_name`, `emp_code`, `emp_management`, `emp_site`, `created`, `updated`) VALUES
(1, '1', 'اسم الموظف ', '234234', 'الاداره ', 'الموقع', '2017-03-23 14:48:15', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `warehouse_items`
--

CREATE TABLE `warehouse_items` (
  `id` int(11) NOT NULL,
  `warehouse_item_number` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `warehouse_item_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `warehouse_category_item_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `warehouse_items`
--

INSERT INTO `warehouse_items` (`id`, `warehouse_item_number`, `warehouse_item_name`, `warehouse_category_item_name`, `created`, `updated`) VALUES
(1, '02', 'حاسب آلي مكتبي دل 351', 'أجهزة حاسب آلي', '2017-03-21 11:31:02', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `warehouse_items_info`
--

CREATE TABLE `warehouse_items_info` (
  `id` int(11) NOT NULL,
  `info_item_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `info_owner_number` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `info_serial_number` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `contract_number` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `start_date_guarantee` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `end_date_guarantee` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `supplier_name` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `Employer_number` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `notes` text COLLATE utf8_unicode_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `warehouse_items_info`
--

INSERT INTO `warehouse_items_info` (`id`, `info_item_name`, `info_owner_number`, `info_serial_number`, `contract_number`, `start_date_guarantee`, `end_date_guarantee`, `supplier_name`, `state`, `Employer_number`, `notes`, `created`, `updated`) VALUES
(1, 'اسم الصنف', 'رقم الملكية', 'رقم التسلسل', 'رقم العقد', '2017', '2017-03-21', 'رقم الموزع', 'الحاله', 'رقم الموظف', 'ملاحظات', '2017-03-21 13:47:48', '0000-00-00 00:00:00'),
(2, 'اسم الصنف', 'رقم الملكية', 'رقم التسلسل', 'رقم العقد', '11-2-2017', '11-2-2017', 'رقم الموزع', 'الحاله', 'رقم الموظف', 'ملاحظاااات', '2017-03-21 14:16:29', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `warehouse_managements`
--

CREATE TABLE `warehouse_managements` (
  `id` int(11) NOT NULL,
  `management_number` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `warehouse_managements`
--

INSERT INTO `warehouse_managements` (`id`, `management_number`, `title`, `created`, `updated`) VALUES
(3, '01', 'الإدارة العليا تعديلب', '2017-03-20 13:38:33', '0000-00-00 00:00:00'),
(4, '02', 'الاداره', '2017-03-20 14:08:54', '0000-00-00 00:00:00'),
(5, '#1', 'الإدارة الماليه', '2017-03-22 12:05:44', '0000-00-00 00:00:00'),
(6, '#1', 'الإدارة الماليه', '2017-03-22 12:05:45', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `warehouse_sites`
--

CREATE TABLE `warehouse_sites` (
  `id` int(11) NOT NULL,
  `site_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `site_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `warehouse_sites`
--

INSERT INTO `warehouse_sites` (`id`, `site_number`, `site_name`, `created`, `updated`) VALUES
(1, '02', 'اسم الموقع', '2017-03-21 15:04:02', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `warehouse_stores`
--

CREATE TABLE `warehouse_stores` (
  `id` int(11) NOT NULL,
  `store_number` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `store_title` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `warehouse_stores`
--

INSERT INTO `warehouse_stores` (`id`, `store_number`, `store_title`, `created`, `updated`) VALUES
(1, '#1', 'اسم المخزن', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `warehouse_suppliers`
--

CREATE TABLE `warehouse_suppliers` (
  `id` int(11) NOT NULL,
  `supplier_number` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `supplier_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `warehouse_suppliers`
--

INSERT INTO `warehouse_suppliers` (`id`, `supplier_number`, `supplier_name`, `created`, `updated`) VALUES
(1, '#3', 'اسم المورد', '2017-03-23 15:00:35', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `add_orders_items_form_7`
--
ALTER TABLE `add_orders_items_form_7`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recieved_orders_items_form7`
--
ALTER TABLE `recieved_orders_items_form7`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `warehouse_categories_items`
--
ALTER TABLE `warehouse_categories_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `warehouse_custody_employers`
--
ALTER TABLE `warehouse_custody_employers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `warehouse_employers`
--
ALTER TABLE `warehouse_employers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `warehouse_items`
--
ALTER TABLE `warehouse_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `warehouse_items_info`
--
ALTER TABLE `warehouse_items_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `warehouse_managements`
--
ALTER TABLE `warehouse_managements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `warehouse_sites`
--
ALTER TABLE `warehouse_sites`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `warehouse_stores`
--
ALTER TABLE `warehouse_stores`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `warehouse_suppliers`
--
ALTER TABLE `warehouse_suppliers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `add_orders_items_form_7`
--
ALTER TABLE `add_orders_items_form_7`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `recieved_orders_items_form7`
--
ALTER TABLE `recieved_orders_items_form7`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `warehouse_categories_items`
--
ALTER TABLE `warehouse_categories_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `warehouse_custody_employers`
--
ALTER TABLE `warehouse_custody_employers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `warehouse_employers`
--
ALTER TABLE `warehouse_employers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `warehouse_items`
--
ALTER TABLE `warehouse_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `warehouse_items_info`
--
ALTER TABLE `warehouse_items_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `warehouse_managements`
--
ALTER TABLE `warehouse_managements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `warehouse_sites`
--
ALTER TABLE `warehouse_sites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `warehouse_stores`
--
ALTER TABLE `warehouse_stores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `warehouse_suppliers`
--
ALTER TABLE `warehouse_suppliers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

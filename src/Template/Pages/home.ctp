<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
    <div class="panel panel-red">
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-hover table-striped table-bordered no-margin">
                    <thead>
                        <tr>
                               <th><?= $this->Paginator->sort('id', __('Transaction Number')) ?></th>
                                <th><?= $this->Paginator->sort('transactions_type_id') ?></th>
                                <th><?= $this->Paginator->sort('transaction_item', __('The Item')) ?></th>
                                <th><?= $this->Paginator->sort('price', __('The Price')) ?></th>
                                   <th><?= $this->Paginator->sort('balance', __('The Balance')) ?></th>
                                <th><?= $this->Paginator->sort('reference', __('The Reference')) ?></th>
                                <th><?= $this->Paginator->sort('date', __('The Date')) ?></th>
                                <th><?= $this->Paginator->sort('reference_no', __('The Reference No.')) ?></th>
                                <th><?= $this->Paginator->sort('vertical_coding_number', __('Coding No.')) ?></th>


                                <th class="actions"><?= __('Actions') ?></th>
                            </tr>
                    </thead>
                    <tbody>
                  
                          <?php
                            foreach ($x as $transaction):
                                var_dump($transaction->TransactionsTypes);
                                ?>
                                <tr>
                                    <td><?= $this->Number->format($transaction->id) ?></td>
                                    <td><?= h($transaction->transactions_type->title) ?></td>
                                    <td><?=$transaction->$transactionsType->budget_item->item_number ?></td>
                                    <td><?= $this->Number->currency($transaction->price, 'SAR', ['pattern' => '#,###.00 ¤']) ?></td>                                  
                                    <td><?= h($transaction->amount) ?></td>
                                    <td><?= h($transaction->dean_signature ) ?></td>
                                     <td><?= h($transaction->date) ?></td>
                                    <td><?= h($transaction->finanical_commitment_letter_sent) ?></td>
                                    <td><?= h($transaction->vertical_coding_number) ?></td>
                                    <td class="actions">
                                        <?= $this->Html->link('<i class="icon-tune"></i>', ['action' => 'edit', $transaction->id], ['class' => 'btn btn-success icon-btn btn-rounded', 'escape' => false]) ?>
                                        <?= $this->Html->link('<i class="icon-cancel2"></i>', ['action' => 'delete', $transaction->id], ['class' => 'btn btn-danger icon-btn btn-rounded', 'escape' => false, 'confirm' => __('Are you sure you want to delete # {0}?', $transaction->id)]) ?>


                                    </td>
                                </tr>
                            <?php endforeach; ?>
                   
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <div class="panel">
        <div class="panel-body">
            <ul class="app-downloads">
                <li>
                    <p class="clearfix"><i class="icon-radio-checked text-danger"></i><b>اشتراك بقواعد بيانات</b><span>5769</span>
                    </p>
                    <div class="progress progress-md">
                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="89" aria-valuemin="0" aria-valuemax="100" style="width: 89%"><span class="sr-only">89% Complete (success)</span>
                        </div>
                    </div>
                </li>
                <li>
                    <p class="clearfix"><i class="icon-radio-checked text-warning"></i><b>عقود الترفيف</b><span>2126</span>
                    </p>
                    <div class="progress progress-md">
                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100" style="width: 55%"><span class="sr-only">55% Complete (success)</span>
                        </div>
                    </div>
                </li>
                <li>
                    <p class="clearfix"><i class="icon-radio-checked text-success"></i><b>شراء كتب</b><span>1068</span>
                    </p>
                    <div class="progress progress-md">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="29" aria-valuemin="0" aria-valuemax="100" style="width: 29%"><span class="sr-only">29% Complete (success)</span>
                        </div>
                    </div>
                </li>
                <li>
                    <p class="clearfix"><i class="icon-radio-checked text-info"></i><b>شراء مخطوطات</b><span>285</span>
                    </p>
                    <div class="progress progress-md">
                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 10%"><span class="sr-only">10% Complete (success)</span>
                        </div>
                    </div>
                </li>
                <li>
                    <p class="clearfix"><i class="icon-radio-checked text-info"></i><b>اشتراك بدورية</b><span>210</span>
                    </p>
                    <div class="progress progress-md">
                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="8" aria-valuemin="0" aria-valuemax="100" style="width: 8%"><span class="sr-only">10% Complete (success)</span>
                        </div>
                    </div>
                </li>
                <li>
                    <p class="clearfix"><i class="icon-radio-checked text-info"></i><b>صيانة الكتب وتجليدها</b><span>115</span>
                    </p>
                    <div class="progress progress-md">
                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="4" aria-valuemin="0" aria-valuemax="100" style="width: 4%"><span class="sr-only">10% Complete (success)</span>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>


    <div class="panel">
        <div class="panel-heading">
            <h4><b>الرصيد الجديد</b></h4>
        </div>
        <div class="panel-body center-text">
            <h2 class="text-danger no-margin">22,773,500 ريال</h2>
        </div>
    </div>

</div>
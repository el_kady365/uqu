<header>
    <a href="<?php echo $this->Url->build('/'); ?>" class="logo"><img src="<?php echo $this->Url->build('/css/img/logo.png') ?>" alt="عمادة شؤون المكتبات">
    </a>
    <ul id="header-actions" class="clearfix">
        <li class="list-box hidden-xs dropdown"><a id="drop2" href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-warning2"></i> </a><span class="info-label blue-bg">5</span>
            <ul class="dropdown-menu imp-notify">
                <li class="dropdown-header">لديك 3 تنبيهات جديدة</li>
                <li>
                    <div class="icon"><img src="<?php echo $this->Url->build('/css/img/info.png') ?>" alt="">
                    </div>
                    <div class="details"><strong class="text-danger">تم رفض المعاملة</strong> <span>تم الموافقه علي العقد رقم 2322 للمعاملة المالية رقم 623</span>
                    </div>
                </li>
                <li>
                    <div class="icon"><img src="<?php echo $this->Url->build('/css/img/info.png') ?>" alt="Arise Admin">
                    </div>
                    <div class="details"><strong class="text-success">استلام موافقه ادارة العقود</strong> <span>تم الموافقه علي العقد رقم 2322 للمعاملة المالية رقم 623</span>
                    </div>
                </li>
                <li>
                    <div class="icon"><img src="<?php echo $this->Url->build('/css/img/info.png') ?>" alt="Arise Admin">
                    </div>
                    <div class="details"><strong class="text-info">صرف المستحقات المالية</strong> <span>تم الموافقه علي صرف المستحقات المالية للمعاملة رقم 12322</span>
                    </div>
                </li>
                <li class="dropdown-footer">عرض كافة التنبيهات</li>
            </ul>
        </li>
        <li class="list-box user-admin dropdown">
            <div class="admin-details">
                <div class="name"><b>عبدالقادر عثمان</b></div>
                <div class="designation">قسم المكتبة الرقمية</div>
            </div><a id="drop4" href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-account_circle"></i></a>
            <ul class="dropdown-menu sm">
                <li class="dropdown-content">
                    <a href="#">الملف الشخصي</a>
                    <a href="#">تغيير كلمة المرور</a>
                    <a href="#">الاعدادات</a>
                    <a href="#">تسجيل خروج</a>
                </li>
            </ul>
        </li>
    </ul>
    <div class="custom-search hidden-sm hidden-xs">
        <input type="text" class="search-query" placeholder="البحث ..."> <i class="icon-search3"></i>
    </div>
</header>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"><span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
            </button>
        </div>
        <?php echo $this->element('header_menu'); ?>
    </div>
</nav>

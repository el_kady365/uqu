
<?php
$admin_menu = array(
    'Pages' => array('icon' => '<i class="fa fa-file"></i>',
        'subs' => array(
            __('List Pages', true) => array('url' => array('controller' => 'pages', 'action' => 'index')),
            __('Add Page', true) => array('url' => array('controller' => 'pages', 'action' => 'add')),
        )
    ),
    
    'Admins' => array('icon' => '<i class="fa fa-users"></i>',
        'subs' => array(
            __('List Admins', true) => array('url' => array('controller' => 'users', 'action' => 'index', 1)),
            __('Add User', true) => array('url' => array('controller' => 'users', 'action' => 'add', 1)),
        )
    ),
    'Settings' => array('icon' => '<i class="fa fa-cogs"></i>',
        'subs' => array(
            __('General configuration', true) => array('url' => array('controller' => 'settings', 'action' => 'edit-configs', 'general')),
            __('Social network links', true) => array('url' => array('controller' => 'settings', 'action' => 'edit-configs', 'social_networks')),
            __('Contact information', true) => array('url' => array('controller' => 'settings', 'action' => 'edit-configs', 'contact')),
        )
    ),
);
?>


<ul class="mainNav">

    <?php
//    if ($admin['Admin']['publisher'] == 1) {
//        $menu = $publisher_menu;
//    } else {
//        $menu = $admin_menu;
//    }
    side_menu($this, $admin_menu);
    ?>

</ul>

<?php echo $this->append('script'); ?>
<script type="text/javascript">
    $(document).ready(function () {
        var $id = '<?php echo isset($menuId) ? $menuId : \Cake\Utility\Inflector::camelize($this->request->params['controller']); ?>';
        $('#' + $id).addClass('active');
    });
</script>
<?php echo $this->end(); ?>

<?php

function side_menu($view, $menu = array()) {
    foreach ($menu as $key => $item) {
        $class = (!empty($item['subs'])) ? 'folder' : 'file';
        $class2 = (!empty($item['class'])) ? $item['class'] : '';
//        if (hasCabalities($view, $item)) {
        ?>
        <li id="<?php echo (isset($item['id'])) ? \Cake\Utility\Inflector::camelize($item['id']) : \Cake\Utility\Inflector::camelize($key); ?>">
            <a href="<?php echo (!empty($item['url'])) ? Cake\Routing\Router::url($item['url']) : "#" ?>">
                <?php
                if (!empty($item['icon'])) {
                    echo $item['icon'];
                }
                if ($class == 'folder') {
                    echo $view->Html->tag('span', __($key, true));
                } else {
                    echo __($key, true);
                }
                ?>

            </a>


            <?php
            if ($class == 'folder') {
                echo '<ul>';
                side_menu($view, $item['subs']);
                echo '</ul>';
            }
            ?>
        </li>

        <?php
//        }
    }
}

function hasCabalities($view, $items) {
    $user = $view->viewVars['user'];
//    debug($items);
    if (!empty($items['subs'])) {
        foreach ($items['subs'] as $key => $item) {
            if (isset($item['url'])) {
                return hasCapablity($item['url']['controller'], $item['url']['action'], $user);
            }
        }
    } else {
        return hasCapablity($items['url']['controller'], $items['url']['action'], $user);
    }
//    return false;
}
?>







<?php

use Cake\Utility\Inflector;

$admin_menu = array(
    'Financial Department' => array('icon' => 'icon-archive',
        'subs' => array(
            'Budget Year Control' => array('url' => array('plugin' => 'Budgets', 'controller' => 'financial-years', 'action' => 'index'), 'icon' => 'icon-cog2'),
            'Budget Control' => array('url' => array('plugin' => 'Budgets', 'controller' => 'financial-years', 'action' => 'index'), 'icon' => 'icon-layers3'),
            'Budget Items' => array('url' => array('plugin' => 'Budgets', 'controller' => 'budget-items', 'action' => 'index'), 'icon' => 'icon-folder3'),
            'Transactions Types' => array('url' => array('plugin' => 'Budgets', 'controller' => 'transactions-types', 'action' => 'index'), 'icon' => 'icon-view_headline'),
            'Transactions' => array('url' => array('plugin' => 'Budgets', 'controller' => 'transactions', 'action' => 'index'), 'icon' => 'icon-assignment'),
            'Add New Transaction' => array('url' => array('plugin' => 'Budgets', 'controller' => 'transactions', 'action' => 'add'), 'icon' => 'icon-circle-plus'),
        )
    ),
    'Warehouse Department' => array('icon' => 'icon-home2',
        'subs' => array(
            'Warehouses Managements' => array('url' => array('plugin' => 'Warehouses', 'controller' => 'warehouse-managements', 'action' => 'index'), 'icon' => 'icon-library_books'),
            'Warehouses Stores' => array('url' => array('plugin' => 'Warehouses', 'controller' => 'warehouse-stores', 'action' => 'index'), 'icon' => 'icon-shop'),
            'Warehouses Employers' => array('url' => array('plugin' => 'Warehouses', 'controller' => 'warehouse-employers', 'action' => 'index'), 'icon' => 'icon-users3'),
            'Warehouses Custody Employers' => array('url' => array('plugin' => 'Warehouses', 'controller' => 'warehouse-custody-employers', 'action' => 'index'), 'icon' => 'icon-v-card'),
            'Warehouses Category Item' => array('url' => array('plugin' => 'Warehouses', 'controller' => 'warehouse-categories-items', 'action' => 'index'), 'icon' => 'icon-apps'),
            'Warehouses Item' => array('url' => array('plugin' => 'Warehouses', 'controller' => 'warehouse-items', 'action' => 'index'), 'icon' => 'icon-apps'),
            'Warehouses Item Info' => array('url' => array('plugin' => 'Warehouses', 'controller' => 'warehouse-items-info', 'action' => 'index'), 'icon' => 'icon-info3'),
            'Warehouses Insert Form Orders' => array('url' => array('plugin' => 'Warehouses', 'controller' => 'warehouse-add_orders_items_form_7', 'action' => 'index'), 'icon' => 'icon-library_books'),
            'Warehouses Recieved Form Orders' => array('url' => array('plugin' => 'Warehouses', 'controller' => 'recieved_orders_items_form7', 'action' => 'index'), 'icon' => 'icon-library_books'),
            'Warehouses Sites' => array('url' => array('plugin' => 'Warehouses', 'controller' => 'warehouse-sites', 'action' => 'index'), 'icon' => 'icon-location4'),
            'Warehouses Suppliers' => array('url' => array('plugin' => 'Warehouses', 'controller' => 'warehouse-suppliers', 'action' => 'index'), 'icon' => 'icon-supervisor_account'),
        )
    ),
);

$admin_menu_sec = array(
);
?>

<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    <ul class="nav navbar-nav">
<?php header_menu($this, $admin_menu) ?>	
<?php header_menu($this, $admin_menu_sec) ?>	

    </ul>
</div>
<?php

function header_menu($view, $menu = array()) {
    foreach ($menu as $key => $item) {
        $class = (!empty($item['subs'])) ? 'folder' : 'file';
        $class2 = (!empty($item['class'])) ? $item['class'] : '';
//        if (hasCabalities($view, $item)) {
        ?>
        <li <?php echo $class == 'folder' ? 'class="dropdown"' : '' ?> id="<?php echo (isset($item['id'])) ? Inflector::camelize($item['id']) : Inflector::camelize($key); ?>">

            <a <?php echo $class == 'folder' ? 'class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"' : ''; ?> href="<?php echo (!empty($item['url'])) ? Cake\Routing\Router::url($item['url']) : "#" ?>">
        <?php
        if (!empty($item['icon'])) {
            echo '<i class="' . $item['icon'] . '"></i> ';
        }
        if ($class == 'folder') {
            echo $view->Html->tag('b', __($key));
            echo '<span class="caret"></span>';
        } else {
            echo __($key, true);
        }
        ?>

            </a>


                <?php
                if ($class == 'folder') {
                    echo '<ul class="dropdown-menu">';
                    header_menu($view, $item['subs']);
                    echo '</ul>';
                }
                ?>
        </li>


            <?php
//        }
        }
    }
    ?>
<?php echo $this->append('script'); ?>
<script type="text/javascript">
    $(document).ready(function () {
        var $id = '<?php echo isset($menuId) ? $menuId : Inflector::camelize($this->request->params['controller']); ?>';
        $('#' + $id).addClass('active');
    });
</script>
<?php echo $this->end(); ?>
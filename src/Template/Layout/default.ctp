<?php
$this->assign('title', $title_for_layout);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo $this->fetch('title') ?></title>
        <?php echo $this->Html->meta('', '', ['rel' => 'icon', 'link' => '/css/img/fav.png']) ?>
        <!--<meta name="description" content="An interactive getting started guide for Brackets.">-->
        <?php echo $this->Html->css(array('bootstrap.min', 'bootstrap-rtl.min', 'fonts/icomoon/icomoon', 'fonts/font-awesome', 'select2.min', 'main', 'jquery.calendars.picker')) ?>
        <?php echo $this->fetch('css') ?>
        <!--[if lt IE 9]>
        <?php echo $this->Html->script(array('html5shiv', 'respond.min')); ?>
        <![endif]-->
    </head>
    <body>
        <?php echo $this->element('header'); ?>
        <div class="dashboard-wrapper">
            <div class="container-fluid">
                <div class="top-bar clearfix">
                    <div class="row gutter">
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <div class="page-title">
                                <h2><span><?php echo $currentDepartment['text'] ?></span></h2>
                                <h4> <?php echo $this->element('crumbs'); ?></h4>
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12">
                            <ul class="left-stats" id="mini-nav-right">
                                <?php
                                if (!empty($buttons)) {
                                    foreach ($buttons as $btn) {
                                        ?>
                                        <li>
                                            <a href="<?php echo isset($btn['url']) ? $btn['url'] : '#' ?>" class="<?php echo $btn['class'] ?>">
                                                <?php if (isset($btn['icon'])) { ?>
                                                    <i class="<?php echo $btn['icon'] ?>"></i>                                                    
                                                <?php }
                                                ?>
                                                <b> <?php echo $btn['title'] ?></b>
                                            </a>

                                        </li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>

                    </div>
                </div>
                <div class="row gutter">
                    <?php echo $this->Flash->render(); ?>
                    <?php echo $this->fetch('content'); ?>
                </div>



            </div>
        </div>
        <footer>جميع الحقوق محفوظة لجامعة ام القرى - <b>عمادة شؤون المكتبات</b> <span><?php
                $date = new Cake\I18n\Date();
                echo $date->i18nFormat('Y', null, $locale . '@calendar=islamic');
                ?></span>
        </footer>


        <?php
        echo $this->Html->script(['jquery', 'bootstrap.min', 'scrollup/jquery.scrollUp', 'sparkline/retina', 'sparkline/custom-sparkline', 'select2.full.min', 'custom',
            'jquery.plugin',
            'jquery.calendars',
            'jquery.calendars.plus',
            'jquery.calendars.picker',
            'jquery.calendars-ar',
            'jquery.calendars.picker-ar',
            'jquery.calendars.islamic',
            'jquery.calendars.islamic-ar']);
        ?>
        <script>
            $(function () {
                $('#CancelBtn').on('click', function () {
                    var $url = '<?php echo $this->Url->build(['action' => 'index']); ?>';
                    window.location = $url;
                    return false;
                });
                $('.error-message').addClass('help-block no-margin').prepend('<i class="fa fa-times"></i> ');
                var calendar = $.calendars.instance('islamic', 'ar');
                var calendargorgian = $.calendars.instance('gregorian', 'en');
                $('.hasDatePicker').calendarsPicker({calendar: calendar, dateFormat: 'yyyy-mm-dd', regionalOptions: 'ar', 'isRTL': true});
                $('.hasGorgianDatePicker').calendarsPicker({calendar: calendargorgian, dateFormat: 'yyyy-mm-dd', regionalOptions: 'ar', 'isRTL': true});
                $('#mini-nav-right li a').each(function () {
                    if ($(this).attr('href') == '<?php echo $this->request->here ?>') {
                        $(this).removeClass('btn-info').addClass('btn-success');
                    }
                })


            })
        </script>
        <?php echo $this->fetch('script'); ?>



    </body>
</html>

<?php

use Cake\Routing\Route\DashedRoute;
use Cake\Routing\Router;

Router::plugin('Budgets', ['path' => '/budgets'], function ($routes) {
    $routes->connect('/', ['controller' => 'Pages', 'action' => 'home']);
    $routes->connect('/financial-commitment-letter/*', ['controller' => 'FinancialCommitmentLetters', 'action' => 'add']);
    $routes->connect('/trust-order/*', ['controller' => 'TrustOrders', 'action' => 'add']);
    $routes->connect('/contract/*', ['controller' => 'Contracts', 'action' => 'add']);
    $routes->connect('/:controller/', []);
    $routes->fallbacks(DashedRoute::class);
});

<?php
namespace Budgets\Test\TestCase\Model\Table;

use Budgets\Model\Table\FinancialItemsYearsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * Budgets\Model\Table\FinancialItemsYearsTable Test Case
 */
class FinancialItemsYearsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Budgets\Model\Table\FinancialItemsYearsTable
     */
    public $FinancialItemsYears;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.budgets.financial_items_years',
        'plugin.budgets.financial_years',
        'plugin.budgets.budget_items'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('FinancialItemsYears') ? [] : ['className' => 'Budgets\Model\Table\FinancialItemsYearsTable'];
        $this->FinancialItemsYears = TableRegistry::get('FinancialItemsYears', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FinancialItemsYears);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

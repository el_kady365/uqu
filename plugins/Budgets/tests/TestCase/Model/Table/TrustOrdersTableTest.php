<?php
namespace Budgets\Test\TestCase\Model\Table;

use Budgets\Model\Table\TrustOrdersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * Budgets\Model\Table\TrustOrdersTable Test Case
 */
class TrustOrdersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Budgets\Model\Table\TrustOrdersTable
     */
    public $TrustOrders;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.budgets.trust_orders',
        'plugin.budgets.transactions',
        'plugin.budgets.transactions_types',
        'plugin.budgets.budget_items',
        'plugin.budgets.contractors',
        'plugin.budgets.purposes',
        'plugin.budgets.contracts',
        'plugin.budgets.financial_commitment_letters'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TrustOrders') ? [] : ['className' => 'Budgets\Model\Table\TrustOrdersTable'];
        $this->TrustOrders = TableRegistry::get('TrustOrders', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TrustOrders);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

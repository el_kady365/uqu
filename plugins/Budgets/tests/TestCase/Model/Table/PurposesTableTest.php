<?php
namespace Budgets\Test\TestCase\Model\Table;

use Budgets\Model\Table\PurposesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * Budgets\Model\Table\PurposesTable Test Case
 */
class PurposesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Budgets\Model\Table\PurposesTable
     */
    public $Purposes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.budgets.purposes',
        'plugin.budgets.transactions',
        'plugin.budgets.transactions_types',
        'plugin.budgets.budget_items',
        'plugin.budgets.contractors',
        'plugin.budgets.financial_commitment_letters',
        'plugin.budgets.trust_orders',
        'plugin.budgets.contracts',
        'plugin.budgets.users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Purposes') ? [] : ['className' => 'Budgets\Model\Table\PurposesTable'];
        $this->Purposes = TableRegistry::get('Purposes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Purposes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

<?php
namespace Budgets\Test\TestCase\Model\Table;

use Budgets\Model\Table\FinancialAttachmentsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * Budgets\Model\Table\FinancialAttachmentsTable Test Case
 */
class FinancialAttachmentsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Budgets\Model\Table\FinancialAttachmentsTable
     */
    public $FinancialAttachments;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.budgets.financial_attachments',
        'plugin.budgets.financial_items',
        'plugin.budgets.financial_attachements'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('FinancialAttachments') ? [] : ['className' => 'Budgets\Model\Table\FinancialAttachmentsTable'];
        $this->FinancialAttachments = TableRegistry::get('FinancialAttachments', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FinancialAttachments);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

<?php
namespace Budgets\Test\TestCase\Model\Table;

use Budgets\Model\Table\FinancialItemsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * Budgets\Model\Table\FinancialItemsTable Test Case
 */
class FinancialItemsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Budgets\Model\Table\FinancialItemsTable
     */
    public $FinancialItems;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.budgets.financial_items',
        'plugin.budgets.financial_attachements'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('FinancialItems') ? [] : ['className' => 'Budgets\Model\Table\FinancialItemsTable'];
        $this->FinancialItems = TableRegistry::get('FinancialItems', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FinancialItems);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

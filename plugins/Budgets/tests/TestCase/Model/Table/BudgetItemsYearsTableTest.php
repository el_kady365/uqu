<?php
namespace Budgets\Test\TestCase\Model\Table;

use Budgets\Model\Table\BudgetItemsYearsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * Budgets\Model\Table\BudgetItemsYearsTable Test Case
 */
class BudgetItemsYearsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Budgets\Model\Table\BudgetItemsYearsTable
     */
    public $BudgetItemsYears;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.budgets.budget_items_years',
        'plugin.budgets.financial_years',
        'plugin.budgets.budget_items'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('BudgetItemsYears') ? [] : ['className' => 'Budgets\Model\Table\BudgetItemsYearsTable'];
        $this->BudgetItemsYears = TableRegistry::get('BudgetItemsYears', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->BudgetItemsYears);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

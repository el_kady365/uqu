<?php
namespace Budgets\Test\TestCase\Model\Table;

use Budgets\Model\Table\CreateOrderMajorTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * Budgets\Model\Table\CreateOrderMajorTable Test Case
 */
class CreateOrderMajorTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Budgets\Model\Table\CreateOrderMajorTable
     */
    public $CreateOrderMajor;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.budgets.create_order_major'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CreateOrderMajor') ? [] : ['className' => 'Budgets\Model\Table\CreateOrderMajorTable'];
        $this->CreateOrderMajor = TableRegistry::get('CreateOrderMajor', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CreateOrderMajor);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

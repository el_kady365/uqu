<?php
namespace Budgets\Test\TestCase\Model\Table;

use Budgets\Model\Table\FinancialYearsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * Budgets\Model\Table\FinancialYearsTable Test Case
 */
class FinancialYearsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Budgets\Model\Table\FinancialYearsTable
     */
    public $FinancialYears;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.budgets.financial_years',
        'plugin.budgets.budget_items'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('FinancialYears') ? [] : ['className' => 'Budgets\Model\Table\FinancialYearsTable'];
        $this->FinancialYears = TableRegistry::get('FinancialYears', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FinancialYears);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

<?php
namespace Budgets\Test\TestCase\Model\Table;

use Budgets\Model\Table\TransactionsTypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * Budgets\Model\Table\TransactionsTypesTable Test Case
 */
class TransactionsTypesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Budgets\Model\Table\TransactionsTypesTable
     */
    public $TransactionsTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.budgets.transactions_types',
        'plugin.budgets.budget_items'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TransactionsTypes') ? [] : ['className' => 'Budgets\Model\Table\TransactionsTypesTable'];
        $this->TransactionsTypes = TableRegistry::get('TransactionsTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TransactionsTypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

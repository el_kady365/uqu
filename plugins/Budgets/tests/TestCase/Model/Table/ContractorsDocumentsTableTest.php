<?php
namespace Budgets\Test\TestCase\Model\Table;

use Budgets\Model\Table\ContractorsDocumentsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * Budgets\Model\Table\ContractorsDocumentsTable Test Case
 */
class ContractorsDocumentsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Budgets\Model\Table\ContractorsDocumentsTable
     */
    public $ContractorsDocuments;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.budgets.contractors_documents',
        'plugin.budgets.contractors',
        'plugin.budgets.transactions',
        'plugin.budgets.transactions_types',
        'plugin.budgets.budget_items',
        'plugin.budgets.purposes',
        'plugin.budgets.financial_years',
        'plugin.budgets.budget_items_years',
        'plugin.budgets.financial_commitment_letters',
        'plugin.budgets.trust_orders',
        'plugin.budgets.invoices',
        'plugin.budgets.contracts',
        'plugin.budgets.financial_items',
        'plugin.budgets.financial_attachments'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ContractorsDocuments') ? [] : ['className' => 'Budgets\Model\Table\ContractorsDocumentsTable'];
        $this->ContractorsDocuments = TableRegistry::get('ContractorsDocuments', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ContractorsDocuments);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

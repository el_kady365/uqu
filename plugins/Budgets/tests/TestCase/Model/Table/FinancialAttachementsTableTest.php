<?php
namespace Budgets\Test\TestCase\Model\Table;

use Budgets\Model\Table\FinancialAttachementsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * Budgets\Model\Table\FinancialAttachementsTable Test Case
 */
class FinancialAttachementsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Budgets\Model\Table\FinancialAttachementsTable
     */
    public $FinancialAttachements;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.budgets.financial_attachements',
        'plugin.budgets.financial_items'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('FinancialAttachements') ? [] : ['className' => 'Budgets\Model\Table\FinancialAttachementsTable'];
        $this->FinancialAttachements = TableRegistry::get('FinancialAttachements', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FinancialAttachements);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

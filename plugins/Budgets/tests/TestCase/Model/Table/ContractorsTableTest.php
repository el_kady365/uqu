<?php
namespace Budgets\Test\TestCase\Model\Table;

use Budgets\Model\Table\ContractorsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * Budgets\Model\Table\ContractorsTable Test Case
 */
class ContractorsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Budgets\Model\Table\ContractorsTable
     */
    public $Contractors;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.budgets.contractors',
        'plugin.budgets.transactions',
        'plugin.budgets.transactions_types',
        'plugin.budgets.budget_items',
        'plugin.budgets.purposes',
        'plugin.budgets.financial_commitment_letters',
        'plugin.budgets.trust_orders',
        'plugin.budgets.contracts',
        'plugin.budgets.users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Contractors') ? [] : ['className' => 'Budgets\Model\Table\ContractorsTable'];
        $this->Contractors = TableRegistry::get('Contractors', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Contractors);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

<?php
namespace Budgets\Test\TestCase\Model\Table;

use Budgets\Model\Table\FinancialCommitmentLettersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * Budgets\Model\Table\FinancialCommitmentLettersTable Test Case
 */
class FinancialCommitmentLettersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Budgets\Model\Table\FinancialCommitmentLettersTable
     */
    public $FinancialCommitmentLetters;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.budgets.financial_commitment_letters',
        'plugin.budgets.transactions',
        'plugin.budgets.transactions_types',
        'plugin.budgets.budget_items',
        'plugin.budgets.contractors',
        'plugin.budgets.purposes',
        'plugin.budgets.trust_orders',
        'plugin.budgets.contracts',
        'plugin.budgets.users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('FinancialCommitmentLetters') ? [] : ['className' => 'Budgets\Model\Table\FinancialCommitmentLettersTable'];
        $this->FinancialCommitmentLetters = TableRegistry::get('FinancialCommitmentLetters', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FinancialCommitmentLetters);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

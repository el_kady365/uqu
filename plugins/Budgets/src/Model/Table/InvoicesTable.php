<?php

namespace Budgets\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Invoices Model
 *
 * @property \Cake\ORM\Association\BelongsTo $TrustOrders
 * @property \Cake\ORM\Association\BelongsTo $Transactions
 *
 * @method \Budgets\Model\Entity\Invoice get($primaryKey, $options = [])
 * @method \Budgets\Model\Entity\Invoice newEntity($data = null, array $options = [])
 * @method \Budgets\Model\Entity\Invoice[] newEntities(array $data, array $options = [])
 * @method \Budgets\Model\Entity\Invoice|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Budgets\Model\Entity\Invoice patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Budgets\Model\Entity\Invoice[] patchEntities($entities, array $data, array $options = [])
 * @method \Budgets\Model\Entity\Invoice findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class InvoicesTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('invoices');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->fileSettings = [
            'pdf_file' => [// The name of your upload field
                'folder' => 'files/invoices', // Customise the root upload folder here, or omit to use the default
                'required' => true,
                'allowedMimeTypes' => ['application/pdf'],
            ]
        ];
        $this->addBehavior('File', $this->fileSettings);
        $this->addBehavior('Timestamp');

        $this->belongsTo('TrustOrders', [
            'foreignKey' => 'trust_order_id',
            'className' => 'Budgets.TrustOrders'
        ]);
        $this->belongsTo('Transactions', [
            'foreignKey' => 'transaction_id',
            'className' => 'Budgets.Transactions'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->allowEmpty('invoice_islamic_date');

        $validator
                ->date('invoice_gorgian_date')
                ->allowEmpty('invoice_gorgian_date');

        $validator
                ->allowEmpty('invoice_no');

        $validator
                ->allowEmpty('order_no');

        $validator
                ->integer('addresses_count')
                ->allowEmpty('addresses_count');

        $validator
                ->integer('copies_count')
                ->allowEmpty('copies_count');

        $validator
                ->allowEmpty('starting_vertical_coding_no');

        $validator
                ->numeric('price')
                ->allowEmpty('price');

        $validator
                ->date('shipping_invoice_gorgian_date')
                ->allowEmpty('shipping_invoice_gorgian_date');

        $validator
                ->allowEmpty('shipping_invoice_no');

        $validator
                ->numeric('shipping_price')
                ->allowEmpty('shipping_price');

        $validator
                ->allowEmpty('notes');

        $validator
                ->allowEmpty('pdf_file');

        $validator
                ->allowEmpty('trust_supplier_name');

        $validator
                ->allowEmpty('trust_financial_year');

        $validator
                ->numeric('trust_approximately_price')
                ->allowEmpty('trust_approximately_price');

        $validator
                ->allowEmpty('trust_container_type');

        $validator
                ->allowEmpty('trust_beneficiary');

        $validator
                ->allowEmpty('user_full_name');

        $validator
                ->allowEmpty('last_update_by');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['trust_order_id'], 'TrustOrders'));
        $rules->add($rules->existsIn(['transaction_id'], 'Transactions'));

        return $rules;
    }

}

<?php
namespace Budgets\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * FinancialCommitmentLetters Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Transactions
 * @property \Cake\ORM\Association\HasMany $Transactions
 *
 * @method \Budgets\Model\Entity\FinancialCommitmentLetter get($primaryKey, $options = [])
 * @method \Budgets\Model\Entity\FinancialCommitmentLetter newEntity($data = null, array $options = [])
 * @method \Budgets\Model\Entity\FinancialCommitmentLetter[] newEntities(array $data, array $options = [])
 * @method \Budgets\Model\Entity\FinancialCommitmentLetter|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Budgets\Model\Entity\FinancialCommitmentLetter patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Budgets\Model\Entity\FinancialCommitmentLetter[] patchEntities($entities, array $data, array $options = [])
 * @method \Budgets\Model\Entity\FinancialCommitmentLetter findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class FinancialCommitmentLettersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('financial_commitment_letters');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Transactions', [
            'foreignKey' => 'transaction_id',
            'className' => 'Budgets.Transactions'
        ]);
        
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('letter_no');

        $validator
            ->allowEmpty('financial_commitment_no');

        $validator
            
            ->notEmpty('date',__('Required'));

        $validator
            ->allowEmpty('sender');

        $validator
            ->allowEmpty('receiver');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['transaction_id'], 'Transactions'));

        return $rules;
    }
}

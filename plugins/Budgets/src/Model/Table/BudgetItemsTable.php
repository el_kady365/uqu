<?php

namespace Budgets\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * BudgetItems Model
 *
 * @method \App\Model\Entity\BudgetItem get($primaryKey, $options = [])
 * @method \App\Model\Entity\BudgetItem newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\BudgetItem[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\BudgetItem|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\BudgetItem patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\BudgetItem[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\BudgetItem findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class BudgetItemsTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('budget_items');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->notEmpty('item_number', __('Required'));

        $validator
                ->notEmpty('item_title', __('Required'));

        $validator
                ->boolean('active')
                ->allowEmpty('active');

        return $validator;
    }

}

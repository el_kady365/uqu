<?php

namespace Budgets\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * FinancialYears Model
 *
 * @property \Cake\ORM\Association\BelongsTo $FinancialYears
 * @property \Cake\ORM\Association\HasMany $BudgetItems
 * @property \Cake\ORM\Association\HasMany $FinancialYears
 *
 * @method \Budgets\Model\Entity\FinancialYear get($primaryKey, $options = [])
 * @method \Budgets\Model\Entity\FinancialYear newEntity($data = null, array $options = [])
 * @method \Budgets\Model\Entity\FinancialYear[] newEntities(array $data, array $options = [])
 * @method \Budgets\Model\Entity\FinancialYear|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Budgets\Model\Entity\FinancialYear patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Budgets\Model\Entity\FinancialYear[] patchEntities($entities, array $data, array $options = [])
 * @method \Budgets\Model\Entity\FinancialYear findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class FinancialYearsTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('financial_years');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');


        $this->hasMany('BudgetItemsYears', [
            'foreignKey' => 'financial_year_id',
            'className' => 'Budgets.BudgetItemsYears',
             'dependent' =>  true ,
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->requirePresence('year', 'create')
                ->notEmpty('year');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
//        $rules->add($rules->existsIn(['financial_year_id'], 'FinancialYears'));

        return $rules;
    }

}

<?php
namespace Budgets\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * FinancialItems Model
 *
 * @property \Cake\ORM\Association\HasMany $FinancialAttachments
 *
 * @method \Budgets\Model\Entity\FinancialItem get($primaryKey, $options = [])
 * @method \Budgets\Model\Entity\FinancialItem newEntity($data = null, array $options = [])
 * @method \Budgets\Model\Entity\FinancialItem[] newEntities(array $data, array $options = [])
 * @method \Budgets\Model\Entity\FinancialItem|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Budgets\Model\Entity\FinancialItem patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Budgets\Model\Entity\FinancialItem[] patchEntities($entities, array $data, array $options = [])
 * @method \Budgets\Model\Entity\FinancialItem findOrCreate($search, callable $callback = null, $options = [])
 */
class FinancialItemsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('financial_items');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->hasMany('FinancialAttachments', [
            'foreignKey' => 'financial_item_id',
            'className' => 'Budgets.FinancialAttachments',
            'dependent' =>  true ,
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('order_number', 'create')
            ->notEmpty('order_number');

        $validator
            ->requirePresence('financial_amount', 'create')
            ->notEmpty('financial_amount');

        return $validator;
    }
}

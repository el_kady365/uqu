<?php

namespace Budgets\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TrustOrders Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Transactions
 * @property \Cake\ORM\Association\HasMany $Transactions
 *
 * @method \Budgets\Model\Entity\TrustOrder get($primaryKey, $options = [])
 * @method \Budgets\Model\Entity\TrustOrder newEntity($data = null, array $options = [])
 * @method \Budgets\Model\Entity\TrustOrder[] newEntities(array $data, array $options = [])
 * @method \Budgets\Model\Entity\TrustOrder|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Budgets\Model\Entity\TrustOrder patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Budgets\Model\Entity\TrustOrder[] patchEntities($entities, array $data, array $options = [])
 * @method \Budgets\Model\Entity\TrustOrder findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TrustOrdersTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('trust_orders');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Transactions', [
            'foreignKey' => 'transaction_id',
            'className' => 'Budgets.Transactions'
        ]);

        $this->hasOne('Invoices', [
            'foreignKey' => 'trust_order_id',
            'className' => 'Budgets.Invoices'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->notEmpty('trust_order_no', __('Required'));

        $validator
//            ->date('date')
                ->notEmpty('date', __('Required'));

        $validator
                ->allowEmpty('trusted_person');

        $validator
                ->allowEmpty('content');

        $validator
                ->boolean('permisson_signature')
                ->allowEmpty('permisson_signature');

        $validator
                ->boolean('send_trust_order')
                ->allowEmpty('send_trust_order');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['transaction_id'], 'Transactions'));

        return $rules;
    }

}

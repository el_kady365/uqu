<?php

namespace Budgets\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TransactionsTypes Model
 *
 * @property \Cake\ORM\Association\BelongsTo $BudgetItems
 *
 * @method \Budgets\Model\Entity\TransactionsType get($primaryKey, $options = [])
 * @method \Budgets\Model\Entity\TransactionsType newEntity($data = null, array $options = [])
 * @method \Budgets\Model\Entity\TransactionsType[] newEntities(array $data, array $options = [])
 * @method \Budgets\Model\Entity\TransactionsType|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Budgets\Model\Entity\TransactionsType patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Budgets\Model\Entity\TransactionsType[] patchEntities($entities, array $data, array $options = [])
 * @method \Budgets\Model\Entity\TransactionsType findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TransactionsTypesTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('transactions_types');
        $this->displayField('title');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('BudgetItems', [
             'foreignKey' => 'budget_item_id',
             'className' => 'Budgets.BudgetItems'
        ]);
        $this->hasMany('Transactions', [
            'foreignKey' => 'transactions_type_id',
            'className' => 'Budgets.Transactions'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->notEmpty('title', __('Required'));

        $validator
                ->notEmpty('budget_item_id', __('Required'));
        $validator->requirePresence(['with_contract' => ['message' => __('Required')], 'with_trust_order' => ['message' => __('Required')]]);
//        $validator
//                ->boolean('with_contract')
//                ->notEmpty('with_contract', __('Required'));
//
//        $validator
//                ->boolean('with_trust_order')
//                ->notEmpty('with_trust_order', __('Required'));

        $validator
                ->boolean('active')
                ->allowEmpty('active');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['budget_item_id'], 'BudgetItems'));

        return $rules;
    }

}

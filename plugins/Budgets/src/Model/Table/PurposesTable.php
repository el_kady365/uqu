<?php
namespace Budgets\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Purposes Model
 *
 * @property \Cake\ORM\Association\HasMany $Transactions
 *
 * @method \Budgets\Model\Entity\Purpose get($primaryKey, $options = [])
 * @method \Budgets\Model\Entity\Purpose newEntity($data = null, array $options = [])
 * @method \Budgets\Model\Entity\Purpose[] newEntities(array $data, array $options = [])
 * @method \Budgets\Model\Entity\Purpose|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Budgets\Model\Entity\Purpose patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Budgets\Model\Entity\Purpose[] patchEntities($entities, array $data, array $options = [])
 * @method \Budgets\Model\Entity\Purpose findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PurposesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('purposes');
        $this->displayField('title');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Transactions', [
            'foreignKey' => 'purpose_id',
            'className' => 'Budgets.Transactions'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('title');

        return $validator;
    }
}

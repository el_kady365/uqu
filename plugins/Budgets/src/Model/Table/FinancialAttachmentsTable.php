<?php
namespace Budgets\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * FinancialAttachments Model
 *
 * @property \Cake\ORM\Association\BelongsTo $FinancialItems
 *
 * @method \Budgets\Model\Entity\FinancialAttachment get($primaryKey, $options = [])
 * @method \Budgets\Model\Entity\FinancialAttachment newEntity($data = null, array $options = [])
 * @method \Budgets\Model\Entity\FinancialAttachment[] newEntities(array $data, array $options = [])
 * @method \Budgets\Model\Entity\FinancialAttachment|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Budgets\Model\Entity\FinancialAttachment patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Budgets\Model\Entity\FinancialAttachment[] patchEntities($entities, array $data, array $options = [])
 * @method \Budgets\Model\Entity\FinancialAttachment findOrCreate($search, callable $callback = null, $options = [])
 */
class FinancialAttachmentsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('financial_attachments');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('FinancialItems', [
            'foreignKey' => 'financial_item_id',
            'joinType' => 'INNER',
            'className' => 'Budgets.FinancialItems'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('attachements_name', 'create')
            ->notEmpty('attachements_name');

        $validator
            ->requirePresence('attachements_number', 'create')
            ->notEmpty('attachements_number');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['financial_item_id'], 'FinancialItems'));

        return $rules;
    }
}

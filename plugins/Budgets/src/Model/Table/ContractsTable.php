<?php

namespace Budgets\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Contracts Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Transactions
 *
 * @method \Budgets\Model\Entity\Contract get($primaryKey, $options = [])
 * @method \Budgets\Model\Entity\Contract newEntity($data = null, array $options = [])
 * @method \Budgets\Model\Entity\Contract[] newEntities(array $data, array $options = [])
 * @method \Budgets\Model\Entity\Contract|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Budgets\Model\Entity\Contract patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Budgets\Model\Entity\Contract[] patchEntities($entities, array $data, array $options = [])
 * @method \Budgets\Model\Entity\Contract findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ContractsTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('contracts');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Transactions', [
            'foreignKey' => 'transaction_id',
            'className' => 'Budgets.Transactions'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->notEmpty('contract_no', __('Required'));

        $validator
//            ->date('date')
                ->notEmpty('date', __('Required'));

        $validator
                ->allowEmpty('signed_by');

        $validator
                ->notEmpty('contract_title');

        $validator
                ->boolean('send_to_revision')
                ->allowEmpty('send_to_revision');

        $validator
                ->boolean('signature')
                ->allowEmpty('signature');

        $validator
                ->boolean('send_to_social_security')
                ->allowEmpty('send_to_social_security');

        $validator
                ->boolean('send_to_zakah_income')
                ->allowEmpty('send_to_zakah_income');

        $validator
                ->boolean('send_to_investigation')
                ->allowEmpty('send_to_investigation');

        $validator
                ->boolean('send_to_commecial_chamber')
                ->allowEmpty('send_to_commecial_chamber');

        $validator
                ->boolean('send_to_other')
                ->allowEmpty('send_to_other');

        $validator
                ->allowEmpty('other_sent');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['transaction_id'], 'Transactions'));
        return $rules;
    }

}

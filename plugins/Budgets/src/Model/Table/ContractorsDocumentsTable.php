<?php
namespace Budgets\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ContractorsDocuments Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Contractors
 *
 * @method \Budgets\Model\Entity\ContractorsDocument get($primaryKey, $options = [])
 * @method \Budgets\Model\Entity\ContractorsDocument newEntity($data = null, array $options = [])
 * @method \Budgets\Model\Entity\ContractorsDocument[] newEntities(array $data, array $options = [])
 * @method \Budgets\Model\Entity\ContractorsDocument|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Budgets\Model\Entity\ContractorsDocument patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Budgets\Model\Entity\ContractorsDocument[] patchEntities($entities, array $data, array $options = [])
 * @method \Budgets\Model\Entity\ContractorsDocument findOrCreate($search, callable $callback = null, $options = [])
 */
class ContractorsDocumentsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('contractors_documents');
        $this->displayField('name');
        $this->primaryKey('id');
        
     /*   $this->fileSettings = [
            'pdf_file' => [// The name of your upload field
                'folder' => 'files/contactor', // Customise the root upload folder here, or omit to use the default
                'required' => false,
                'allowedMimeTypes' => ['application/pdf'],
            ]
        ];
        $this->addBehavior('File', $this->fileSettings);
*/
        $this->belongsTo('Contractors', [
            'foreignKey' => 'contractor_id',
            'joinType' => 'INNER',
            'className' => 'Budgets.Contractors'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->date('expire_date')
            ->requirePresence('expire_date', 'create')
            ->allowEmpty('expire_date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['contractor_id'], 'Contractors'));

        return $rules;
    }
}

<?php
namespace Budgets\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Contractors Model
 *
 * @property \Cake\ORM\Association\HasMany $Transactions
 *
 * @method \Budgets\Model\Entity\Contractor get($primaryKey, $options = [])
 * @method \Budgets\Model\Entity\Contractor newEntity($data = null, array $options = [])
 * @method \Budgets\Model\Entity\Contractor[] newEntities(array $data, array $options = [])
 * @method \Budgets\Model\Entity\Contractor|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Budgets\Model\Entity\Contractor patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Budgets\Model\Entity\Contractor[] patchEntities($entities, array $data, array $options = [])
 * @method \Budgets\Model\Entity\Contractor findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ContractorsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('contractors');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Transactions', [
            'foreignKey' => 'contractor_id',
            'className' => 'Budgets.Transactions'
        ]);
        
        $this->hasMany('ContractorsDocuments', [
            'foreignKey' => 'contractor_id',
            'className' => 'Budgets.ContractorsDocuments',
            'dependent' =>  true 
            
        ]);
        
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('username');

        $validator
            ->allowEmpty('password');

        $validator
            ->allowEmpty('name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['username']));

        return $rules;
    }
}

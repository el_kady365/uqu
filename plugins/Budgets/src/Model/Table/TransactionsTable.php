<?php

namespace Budgets\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Transactions Model
 *
 * @property \Cake\ORM\Association\BelongsTo $TransactionTypes
 * @property \Cake\ORM\Association\BelongsTo $Contractors
 * @property \Cake\ORM\Association\BelongsTo $Transactions
 * @property \Cake\ORM\Association\BelongsTo $Purposes
 * @property \Cake\ORM\Association\BelongsTo $FinancialCommitmentLetters
 * @property \Cake\ORM\Association\BelongsTo $TrustOrders
 * @property \Cake\ORM\Association\BelongsTo $Contracts
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\HasMany $Transactions
 *
 * @method \Budgets\Model\Entity\Transaction get($primaryKey, $options = [])
 * @method \Budgets\Model\Entity\Transaction newEntity($data = null, array $options = [])
 * @method \Budgets\Model\Entity\Transaction[] newEntities(array $data, array $options = [])
 * @method \Budgets\Model\Entity\Transaction|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Budgets\Model\Entity\Transaction patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Budgets\Model\Entity\Transaction[] patchEntities($entities, array $data, array $options = [])
 * @method \Budgets\Model\Entity\Transaction findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TransactionsTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('transactions');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('TransactionsTypes', [
            'foreignKey' => 'transactions_type_id',
            'className' => 'Budgets.TransactionsTypes'
        ]);
        $this->belongsTo('Contractors', [
            'foreignKey' => 'contractor_id',
            'className' => 'Budgets.Contractors'
        ]);
        $this->belongsTo('Transactions', [
            'foreignKey' => 'transaction_id',
            'className' => 'Budgets.Transactions'
        ]);
        $this->belongsTo('Purposes', [
            'foreignKey' => 'purpose_id',
            'className' => 'Budgets.Purposes'
        ]);
        
        $this->belongsTo('FinancialYears', [
            'foreignKey' => 'financial_year_id',
            'className' => 'Budgets.FinancialYears'
        ]);
        
        $this->hasMany('Transactions', [
            'foreignKey' => 'transaction_id',
            'className' => 'Budgets.Transactions'
        ]);

        $this->hasOne('FinancialCommitmentLetters', [
            'foreignKey' => 'transaction_id',
            'className' => 'Budgets.FinancialCommitmentLetters',
            'dependent' => true
        ]);
        
        $this->hasOne('TrustOrders', [
            'foreignKey' => 'transaction_id',
            'className' => 'Budgets.TrustOrders'
        ]);
        
        $this->hasOne('Contracts', [
            'foreignKey' => 'transaction_id',
            'className' => 'Budgets.Contracts'
        ]);
        $this->hasOne('FinancialItems', [
            'foreignKey' => 'transaction_id',
            'className' => 'Budgets.FinancialItems'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->allowEmpty('vertical_coding_number');
        $validator
                ->notEmpty('transactions_type_id', __('Required'));

        $validator
//                ->dateTime('date')
                ->notEmpty('date', __('Required'));

        $validator
                ->numeric('price')
                ->allowEmpty('price');

        $validator
                ->allowEmpty('vertical_coding_number');

        $validator
                ->integer('copies_number')
                ->allowEmpty('copies_number');

        $validator
                ->allowEmpty('notes');

        $validator
                ->boolean('dean_signature')
                ->allowEmpty('dean_signature');

        $validator
                ->boolean('finanical_commitment_letter_sent')
                ->allowEmpty('finanical_commitment_letter_sent');

        $validator
                ->integer('status')
                ->allowEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['transaction_type_id'], 'TransactionsTypes'));
        $rules->add($rules->existsIn(['contractor_id'], 'Contractors'));
        $rules->add($rules->existsIn(['transaction_id'], 'Transactions'));
        $rules->add($rules->existsIn(['purpose_id'], 'Purposes'));
        $rules->add($rules->existsIn(['contract_id'], 'Contracts'));


        return $rules;
    }

}

<?php

namespace Budgets\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * BudgetItemsYears Model
 *
 * @property \Cake\ORM\Association\BelongsTo $FinancialYears
 * @property \Cake\ORM\Association\BelongsTo $BudgetItems
 *
 * @method \Budgets\Model\Entity\BudgetItemsYear get($primaryKey, $options = [])
 * @method \Budgets\Model\Entity\BudgetItemsYear newEntity($data = null, array $options = [])
 * @method \Budgets\Model\Entity\BudgetItemsYear[] newEntities(array $data, array $options = [])
 * @method \Budgets\Model\Entity\BudgetItemsYear|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Budgets\Model\Entity\BudgetItemsYear patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Budgets\Model\Entity\BudgetItemsYear[] patchEntities($entities, array $data, array $options = [])
 * @method \Budgets\Model\Entity\BudgetItemsYear findOrCreate($search, callable $callback = null, $options = [])
 */
class BudgetItemsYearsTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('budget_items_years');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('FinancialYears', [
            'foreignKey' => 'financial_year_id',
            'joinType' => 'INNER',
            'className' => 'Budgets.FinancialYears'
        ]);
        $this->belongsTo('BudgetItems', [
            'foreignKey' => 'budget_item_id',
            'joinType' => 'INNER',
            'className' => 'Budgets.BudgetItems'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->requirePresence('amount', 'create')
                ->notEmpty('amount');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['financial_year_id'], 'FinancialYears'));
        $rules->add($rules->existsIn(['budget_item_id'], 'BudgetItems'));

        return $rules;
    }

}

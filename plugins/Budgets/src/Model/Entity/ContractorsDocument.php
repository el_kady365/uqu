<?php
namespace Budgets\Model\Entity;

use Cake\ORM\Entity;

/**
 * ContractorsDocument Entity
 *
 * @property int $id
 * @property int $contractor_id
 * @property string $name
 * @property \Cake\I18n\Time $expire_date
 *
 * @property \Budgets\Model\Entity\Contractor $contractor
 */
class ContractorsDocument extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => true
    ];
}

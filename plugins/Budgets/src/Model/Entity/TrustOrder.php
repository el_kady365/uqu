<?php
namespace Budgets\Model\Entity;

use Cake\ORM\Entity;

/**
 * TrustOrder Entity
 *
 * @property int $id
 * @property string $trust_order_no
 * @property \Cake\I18n\Time $date
 * @property string $trusted_person
 * @property string $content
 * @property bool $permisson_signature
 * @property bool $send_trust_order
 * @property int $transaction_id
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $updated
 *
 * @property \Budgets\Model\Entity\Transaction[] $transactions
 */
class TrustOrder extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
//    function _getDate($date) {
//        if ($date) {       
//            return $date->format('Y-m-d');
//        }
//    }

    
    
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}

<?php

namespace Budgets\Model\Entity;

use Cake\ORM\Entity;

/**
 * TransactionsType Entity
 *
 * @property int $id
 * @property string $title
 * @property int $budget_item_id
 * @property bool $with_contract
 * @property bool $with_trust_order
 * @property bool $active
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $updated
 *
 * @property \Budgets\Model\Entity\BudgetItem $budget_item
 */
class TransactionsType extends Entity {

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    

    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

}

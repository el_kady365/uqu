<?php

namespace Budgets\Model\Entity;

use Cake\ORM\Entity;

/**
 * Transaction Entity
 *
 * @property int $id
 * @property int $transaction_type_id
 * @property \Cake\I18n\Time $date
 * @property int $contractor_id
 * @property int $transaction_id
 * @property float $price
 * @property int $purpose_id
 * @property int $finanical_commitment_letter_id
 * @property int $trust_order_id
 * @property int $contract_id
 * @property string $vertical_coding_number
 * @property int $copies_number
 * @property string $notes
 * @property bool $dean_signature
 * @property bool $finanical_commitment_letter_sent
 * @property int $status
 * @property int $user_id
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $updated
 *
 * @property \Budgets\Model\Entity\TransactionType $transaction_type
 * @property \Budgets\Model\Entity\Contractor $contractor
 * @property \Budgets\Model\Entity\Transaction[] $transactions
 * @property \Budgets\Model\Entity\Purpose $purpose
 * @property \Budgets\Model\Entity\FinanicalCommitmentLetter $finanical_commitment_letter
 * @property \Budgets\Model\Entity\TrustOrder $trust_order
 * @property \Budgets\Model\Entity\Contract $contract
 * @property \Budgets\Model\Entity\User $user
 */
class Transaction extends Entity {

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
//    function _getDate($date) {
//
//        if ($date) {
//            $date = new \Cake\I18n\Time($date);
//            return $date->i18nFormat('Y-M-dd', null, 'en_US');
//        }
//    }

//    function _getNextExchangeDate($date) {
//        if ($date)
//            return $date->format('Y-m-d');
//    }

    protected $_accessible = [
        '*' => true,
        'id' => true
    ];

}

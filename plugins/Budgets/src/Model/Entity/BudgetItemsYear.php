<?php

namespace Budgets\Model\Entity;

use Cake\ORM\Entity;

/**
 * BudgetItemsYear Entity
 *
 * @property int $id
 * @property int $financial_year_id
 * @property int $budget_item_id
 * @property string $amount
 *
 * @property \Budgets\Model\Entity\FinancialYear $financial_year
 * @property \Budgets\Model\Entity\BudgetItem $budget_item
 */
class BudgetItemsYear extends Entity {

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

}

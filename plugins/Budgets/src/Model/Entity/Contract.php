<?php
namespace Budgets\Model\Entity;

use Cake\ORM\Entity;

/**
 * Contract Entity
 *
 * @property int $id
 * @property string $contract_no
 * @property \Cake\I18n\Time $date
 * @property string $contractor
 * @property string $contract_title
 * @property bool $send_to_revision
 * @property bool $signature
 * @property bool $send_to_social_security
 * @property bool $send_to_zakah_income
 * @property bool $send_to_investigation
 * @property bool $send_to_commecial_chamber
 * @property bool $send_to_other
 * @property string $other_sent
 * @property int $transaction_id
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $updated
 *
 * @property \Budgets\Model\Entity\Transaction $transaction
 */
class Contract extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
//    function _getDate($date) {
//        if ($date) {       
//            return $date->format('Y-m-d');
//        }
//    }
    
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}

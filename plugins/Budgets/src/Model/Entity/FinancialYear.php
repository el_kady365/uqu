<?php
namespace Budgets\Model\Entity;

use Cake\ORM\Entity;

/**
 * FinancialYear Entity
 *
 * @property int $id
 * @property string $year
 * @property int $financial_year_id
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $updated
 *
 * @property \Budgets\Model\Entity\FinancialYear[] $financial_years
 * @property \Budgets\Model\Entity\BudgetItem[] $budget_items
 */
class FinancialYear extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => true
    ];
}

<?php
namespace Budgets\Model\Entity;

use Cake\ORM\Entity;

/**
 * FinancialAttachment Entity
 *
 * @property int $id
 * @property int $financial_item_id
 * @property string $attachements_name
 * @property string $attachements_number
 *
 * @property \Budgets\Model\Entity\FinancialItem $financial_item
 */
class FinancialAttachment extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => true
    ];
}

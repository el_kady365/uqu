<?php
namespace Budgets\Model\Entity;

use Cake\ORM\Entity;

/**
 * BudgetItem Entity
 *
 * @property int $id
 * @property string $item_number
 * @property string $item_title
 * @property bool $active
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $updated
 */
class BudgetItem extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected function _getNoTitle() {
        return $this->_properties['item_number'] . ' : ' .
                $this->_properties['item_title'];
    }
    
    protected $_accessible = [
        '*' => true,
        'id' => true
    ];
}

<?php
namespace Budgets\Model\Entity;

use Cake\ORM\Entity;

/**
 * Invoice Entity
 *
 * @property int $id
 * @property string $invoice_islamic_date
 * @property \Cake\I18n\Time $invoice_gorgian_date
 * @property string $invoice_no
 * @property string $order_no
 * @property int $addresses_count
 * @property int $copies_count
 * @property string $starting_vertical_coding_no
 * @property float $price
 * @property \Cake\I18n\Time $shipping_invoice_gorgian_date
 * @property string $shipping_invoice_no
 * @property float $shipping_price
 * @property string $notes
 * @property string $pdf_file
 * @property string $trust_supplier_name
 * @property string $trust_financial_year
 * @property int $trust_order_id
 * @property float $trust_approximately_price
 * @property string $trust_container_type
 * @property string $trust_beneficiary
 * @property int $transaction_id
 * @property string $user_full_name
 * @property string $last_update_by
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $updated
 *
 * @property \Budgets\Model\Entity\TrustOrder $trust_order
 * @property \Budgets\Model\Entity\Transaction $transaction
 */
class Invoice extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}

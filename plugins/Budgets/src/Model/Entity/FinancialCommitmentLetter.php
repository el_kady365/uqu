<?php

namespace Budgets\Model\Entity;

use Cake\ORM\Entity;

/**
 * FinancialCommitmentLetter Entity
 *
 * @property int $id
 * @property string $letter_no
 * @property string $financial_commitment_no
 * @property \Cake\I18n\Time $date
 * @property string $sender
 * @property string $receiver
 * @property int $transaction_id
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $updated
 *
 * @property \Budgets\Model\Entity\Transaction[] $transactions
 */
class FinancialCommitmentLetter extends Entity {

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
//    function _getDate($title) {
//        if ($title)
//            return $title->format('Y-m-d');
//    }

    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

}

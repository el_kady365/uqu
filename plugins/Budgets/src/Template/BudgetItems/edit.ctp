
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <?php if (!$budgetItem->id) {
                    echo __('Add Budget Item');
                    } else {
                    echo __('Edit Budget Item');
                    }
                    ?></h3>
            </div>
            <div class="panel-body">
                <?= $this->Form->create($budgetItem) ?>

                <?php
                                echo $this->Form->input('item_number', ['class' => 'form-control']);
                                            echo $this->Form->input('item_title', ['class' => 'form-control']);
                                            echo $this->Form->input('active',['label'=>['class'=>'checkbox-inline']]);
                                            ?>

                <?= $this->Form->button(__('Submit')) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
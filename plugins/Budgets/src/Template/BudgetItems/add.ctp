<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="text-danger">
                <?php
                if (!$budgetItem->id) {
                    echo __('Add Budget Item');
                } else {
                    echo __('Edit Budget Item');
                }
                ?></h3>
            <hr />
        </div>
        <div class="panel-body">
            <?= $this->Form->create($budgetItem) ?>

            <?php
            echo $this->Form->input('item_number', ['placeholder' => __('Enter Item Number'), 'class' => 'form-control']);
            echo $this->Form->input('item_title', ['placeholder' => __('Item Title'), 'class' => 'form-control']);
            echo $this->Form->input('active', ['label' => ['class' => 'checkbox-inline']]);
            ?>

            <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-success btn-lg', 'templateVars' => ['icon' => '<i class="icon-check"></i>']]) ?>
            <?= $this->Form->button(__('Cancel'), ['class' => 'btn btn-warning btn-lg', 'id' => 'CancelBtn', 'templateVars' => ['icon' => '<i class="icon-cancel"></i>']]) ?>
<?= $this->Form->end() ?>
        </div>
    </div>
</div>

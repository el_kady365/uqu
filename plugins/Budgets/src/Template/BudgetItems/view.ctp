<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Budget Item'), ['action' => 'edit', $budgetItem->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Budget Item'), ['action' => 'delete', $budgetItem->id], ['confirm' => __('Are you sure you want to delete # {0}?', $budgetItem->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Budget Items'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Budget Item'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="budgetItems view large-9 medium-8 columns content">
    <h3><?= h($budgetItem->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Item Number') ?></th>
            <td><?= h($budgetItem->item_number) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($budgetItem->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($budgetItem->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Updated') ?></th>
            <td><?= h($budgetItem->updated) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Active') ?></th>
            <td><?= $budgetItem->active ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Item Title') ?></h4>
        <?= $this->Text->autoParagraph(h($budgetItem->item_title)); ?>
    </div>
</div>

<?php
/**
 * @var \App\View\AppView $this
 */
?>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="text-danger">
                <?php
                if (!$financialYear->id) {
                    echo __('Add Financial Year');
                } else {
                    echo __('Edit Financial Year');
                }
                ?></h3>
            <hr />
        </div>
        <div class="panel-body">
            <?= $this->Form->create($financialYear) ?>

<div class="col-md-12">
            <?php
            echo $this->Form->input('year', ['placeholder' => __('Year'), 'class' => 'form-control']);
            ?>
</div>
            <?php
            if (!empty($financialYear->budget_items_years)) {
                foreach ($financialYear->budget_items_years as $i => $BudgetItemsYears) {
                    echo $this->Form->input('budget_items_years.' . $i . '.id', ['type' => 'hidden', 'value' => $BudgetItemsYears->id]);
                    echo $this->Form->input('budget_items_years.' . $i . '.budget_item_id', ['type' => 'hidden', 'value' => $BudgetItemsYears->budget_item_id]);
             ?>
                    <div class="col-md-6">
            <?php
                    echo $this->Form->input('budget_items_years.' . $i . '.item_number', ['readonly' => 'readonly', 'class' => 'form-control', 'value' => $BudgetItemsYears->budget_item->item_number,'label'=>$BudgetItemsYears->budget_item->item_title]);
                   ?>
                    </div>
             <div class="col-md-6">
            <?php
                    echo $this->Form->input('budget_items_years.' . $i . '.amount', ['class' => 'form-control']);
                    ?>
             </div>
            <?php
                }
            } else {
                foreach ($budgetItems as $i => $budgetItem) { 
                    
             
                    echo $this->Form->input('budget_items_years.' . $i . '.budget_item_id', ['type' => 'hidden', 'value' => $budgetItem->id]);
                    ?>
            <div class="col-md-6">
            <?php
                    echo $this->Form->input('budget_items_years.' . $i . '.item_number', ['readonly' => 'readonly', 'class' => 'form-control', 'value' => $budgetItem->item_number,'label'=>$budgetItem->item_title]);
               ?>
                 </div>
             <div class="col-md-6">
                <?php
                    echo $this->Form->input('budget_items_years.' . $i . '.amount', ['class' => 'form-control']);
                ?>
             </div>
             <?php
                }
            }
            ?>







            <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-success btn-lg', 'templateVars' => ['icon' => '<i class="icon-check"></i>']]) ?>
            <?= $this->Form->button(__('Cancel'), ['class' => 'btn btn-warning btn-lg', 'id' => 'CancelBtn', 'templateVars' => ['icon' => '<i class="icon-cancel"></i>']]) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div> 
<?php echo $this->append('script'); ?>
<script>
    $(function () {
        $('.add-item').on('click', function () {
            $('#Options').append(
                    '<div class="item-div" id="item' + $('.item-div').length + '">'
                    + $('.item-div').html().replace(/\[0\]/g, '[' + $('.item-div').length + ']').replace(/-0-/g, '-'
                    + $('.item-div').length + '-')
                    + '</div>'
                    );
            $('.item-div:last input').val('');

            return false;
        });

    })

    $(document).on('click', '.remove-item', function () {
        if ($('.item-div').length > 1) {
            //  $(this).closest('.item-div').remove();
        }
        if ($.trim($(".item-div").html()) == '')
        {
        }
        return false;
    });
</script>
<?php echo $this->end(); ?>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="text-danger"><?= __('Financial Years') ?></h3>
            <hr />
        </div>
        <div class="panel-body">
            <form action="<?php echo $this->Url->build(array("action" => "do-operation")) ?>" method="post">
                <!--Table Wrapper Start-->
                <div class="table-responsive ls-table">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                                                <th><?= $this->Paginator->sort('id') ?></th>
                                                                <th><?= $this->Paginator->sort('year') ?></th>
                                                                <th><?= $this->Paginator->sort('created') ?></th>
                                                                <th><?= $this->Paginator->sort('updated') ?></th>
                                                                <th class="actions"><?= __('Actions') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($financialYears as $financialYear): ?>
                            <tr>
                                                                <td><?= $this->Number->format($financialYear->id) ?></td>
                                                                            <td><?= h($financialYear->year) ?></td>
                                                                            <td><?= h($financialYear->created) ?></td>
                                                                            <td><?= h($financialYear->updated) ?></td>
                                                                            <td class="actions">
                                    <?= $this->Html->link('<i class="icon-tune"></i>', ['action' => 'edit', $financialYear->id], ['class' => 'btn btn-success icon-btn btn-rounded', 'escape' => false]) ?>
                                    <?= $this->Html->link('<i class="icon-cancel2"></i>', ['action' => 'delete', $financialYear->id], ['class' => 'btn btn-danger icon-btn btn-rounded', 'escape' => false, 'confirm' => __('Are you sure you want to delete # {0}?', $financialYear->id)]) ?>


                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>

                </div>
                <div class="row">
                    <div class="col-md-3">
                    </div>
                    <div class="col-md-9 text-left">
                        <ul class="pagination ls-pagination">
                            <?php
                            if ($this->Paginator->hasPrev()) {
                                echo $this->Paginator->prev('< ' . __('previous'));
                            }
                            echo $this->Paginator->numbers();
                            if ($this->Paginator->hasNext()) {
                                echo $this->Paginator->next(__('next') . ' >');
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Financial Year'), ['action' => 'edit', $financialYear->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Financial Year'), ['action' => 'delete', $financialYear->id], ['confirm' => __('Are you sure you want to delete # {0}?', $financialYear->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Financial Years'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Financial Year'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Budget Items'), ['controller' => 'BudgetItems', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Budget Item'), ['controller' => 'BudgetItems', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="financialYears view large-9 medium-8 columns content">
    <h3><?= h($financialYear->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Year') ?></th>
            <td><?= h($financialYear->year) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($financialYear->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($financialYear->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Updated') ?></th>
            <td><?= h($financialYear->updated) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Budget Items') ?></h4>
        <?php if (!empty($financialYear->budget_items)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Item Number') ?></th>
                <th scope="col"><?= __('Item Title') ?></th>
                <th scope="col"><?= __('Active') ?></th>
                <th scope="col"><?= __('Amount') ?></th>
                <th scope="col"><?= __('Financial Year Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Updated') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($financialYear->budget_items as $budgetItems): ?>
            <tr>
                <td><?= h($budgetItems->id) ?></td>
                <td><?= h($budgetItems->item_number) ?></td>
                <td><?= h($budgetItems->item_title) ?></td>
                <td><?= h($budgetItems->active) ?></td>
                <td><?= h($budgetItems->amount) ?></td>
                <td><?= h($budgetItems->financial_year_id) ?></td>
                <td><?= h($budgetItems->created) ?></td>
                <td><?= h($budgetItems->updated) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'BudgetItems', 'action' => 'view', $budgetItems->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'BudgetItems', 'action' => 'edit', $budgetItems->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'BudgetItems', 'action' => 'delete', $budgetItems->id], ['confirm' => __('Are you sure you want to delete # {0}?', $budgetItems->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>

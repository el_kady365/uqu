<?php
/**
 * @var \App\View\AppView $this
 */
?>

<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="text-danger">
                <?php
                if (!$financialItem->id) {
                    echo __('Add Financial Item');
                } else {
                    echo __('Edit Financial Item');
                }
                ?></h3>
            <hr />
        </div>
        <div class="panel-body">
            <?= $this->Form->create($financialItem) ?>

            <?php
            echo $this->Form->input('order_number', ['placeholder' => __('Order Number'), 'class' => 'form-control']);
            echo $this->Form->input('financial_amount', ['placeholder' => __('Financial Amount'), 'class' => 'form-control']);
            ?>

            <div id="Options">
                <?php
//                debug($FinancialAttachments);                exit();
                if (!$FinancialAttachments->isEmpty()) {
                    foreach ($FinancialAttachments as $i => $attchment) {
                        ?>
                        <div class="attachment-div">
                            <?php
                            echo $this->Form->input('financial_attachments.' . $i . '.id', ['class' => 'form-control', 'value' => $attchment->id]);
                            ?>
                            <div class="col-sm-9">
                                <?php
                                echo $this->Form->input('financial_attachments.' . $i . '.attachements_name', ['class' => 'form-control', 'value' => $attchment->attachements_name]);
                                ?>
                            </div>
                            <div class="col-sm-2">
                                <?php
                                echo $this->Form->input('financial_attachments.' . $i . '.attachements_number', ['type' => 'number', 'class' => 'form-control attachments-num', 'value' => $attchment->attachements_number]);
                                ?>
                            </div>
                            <div   class="col-sm-1">
                                <a href="#" class="btn btn-danger btn-sm remove-attachment" ><i class="fa fa-close"></i></a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <?php
                    }
                } else {
                    ?>
                    <div class="attachment-div">
                        <div class="col-sm-9">
                            <?php
                            echo $this->Form->input('financial_attachments.0.attachements_name', ['class' => 'form-control']);
                            ?>
                        </div>
                        <div class="col-sm-2">
                            <?php
                            echo $this->Form->input('financial_attachments.0.attachements_number', ['type' => 'number', 'class' => 'attachments-num form-control']);
                            ?>
                        </div>
                        <div class="col-sm-1" style="margin-top: 2em">
                            <a href="#" class="btn btn-danger btn-sm remove-attachment" ><i class="fa fa-close"></i></a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                <?php } ?>
            </div>
            <div class="row gutter">
                <div class="form-group col-md-3 col-md-offset-9">
                    <label class="control-label"><?php echo __('Total Attachments') ?></label>
                    <input type="text" id="sum" class="form-control" />
                </div>
            </div>

            <div class="col-lg-12">
                <a href="#" class="btn btn-primary add-attachement"><i class="fa fa-plus"></i> <?php echo __('Add attachment') ?></a>
            </div>
 <div class="clear"></div>
            <div class="row">
                <div class="col-lg-4 col-md-4">
                    <?php
                    echo $this->Form->input('signature', ['label' => ['class' => 'checkbox-inline']]);
                    ?>
                </div>
                <div class="col-lg-4 col-md-4">
                    <?php
                    echo $this->Form->input('accepted_order', ['label' => ['class' => 'checkbox-inline']]);
                    ?>
                </div>
            </div>
            <div class="col-lg-12">
                <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-success btn-lg', 'templateVars' => ['icon' => '<i class="icon-check"></i>']]) ?>
                <?= $this->Form->button(__('Cancel'), ['class' => 'btn btn-warning btn-lg', 'id' => 'CancelBtn', 'templateVars' => ['icon' => '<i class="icon-cancel"></i>']]) ?>
            </div>
            <?= $this->Form->end() ?>

        </div>
    </div>
</div>


    <?php echo $this->element('transaction_form') ?>

<?php echo $this->append('script'); ?>

<script>
    $(function () {
        $('.add-attachement').on('click', function () {
            $('#Options').append('<div class="attachment-div" id="attachment' + $('.attachment-div').length + '">'
                    + $('.attachment-div:first').html().replace(/\[0\]/g, '[' + $('.attachment-div').length + ']').replace(/-0-/g, '-'
                    + $('.attachment-div').length + '-')
                    + '</div>');
            $('.attachment-div:last input').val('');
            return false;
        });

    })
    var $val=0;
function removeSum (){
    $val=0;
    $('.attachments-num').each(function () {
            $val += parseInt($(this).val());
        });
        if (!isNaN($val)) {
            $('#sum').val($val);
        }
        else {
            $('#sum').val(0);
        }
       // alert($val);
      
}
    $(document).on('click', '.remove-attachment', function () {
         
          console.log($val);
        if ($('.attachment-div').length > 1) {
            $(this).closest('.attachment-div').remove();
        }
         removeSum();
        return false;
    });
</script>

<script>
    $(document).on('blur', '.attachments-num', function () {
         $val = 0;
         removeSum();
        
    });
    $(function () {
        $('.attachments-num').trigger('blur');
    });

</script>
<?php echo $this->end(); ?>

<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Financial Item'), ['action' => 'edit', $financialItem->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Financial Item'), ['action' => 'delete', $financialItem->id], ['confirm' => __('Are you sure you want to delete # {0}?', $financialItem->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Financial Items'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Financial Item'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Financial Attachements'), ['controller' => 'FinancialAttachements', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Financial Attachement'), ['controller' => 'FinancialAttachements', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="financialItems view large-9 medium-8 columns content">
    <h3><?= h($financialItem->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Order Number') ?></th>
            <td><?= h($financialItem->order_number) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Financial Amount') ?></th>
            <td><?= h($financialItem->financial_amount) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($financialItem->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Financial Attachements') ?></h4>
        <?php if (!empty($financialItem->financial_attachements)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Financial Item Id') ?></th>
                <th scope="col"><?= __('Attachements Name') ?></th>
                <th scope="col"><?= __('Attachements Number') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($financialItem->financial_attachements as $financialAttachements): ?>
            <tr>
                <td><?= h($financialAttachements->id) ?></td>
                <td><?= h($financialAttachements->financial_item_id) ?></td>
                <td><?= h($financialAttachements->attachements_name) ?></td>
                <td><?= h($financialAttachements->attachements_number) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'FinancialAttachements', 'action' => 'view', $financialAttachements->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'FinancialAttachements', 'action' => 'edit', $financialAttachements->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'FinancialAttachements', 'action' => 'delete', $financialAttachements->id], ['confirm' => __('Are you sure you want to delete # {0}?', $financialAttachements->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>

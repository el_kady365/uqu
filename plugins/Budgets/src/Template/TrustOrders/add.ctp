<?php
/**
 * @var \App\View\AppView $this
 */
?>

<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="text-danger">
                <?php
                if (!$trustOrder->id) {
                    echo __('Trust Order');
                } else {
                    echo __('Trust Order');
                }
                ?></h3>
            <hr />
        </div>
        <div class="panel-body">
            <?= $this->Form->create($trustOrder) ?>

            <?php
            echo $this->Form->input('trust_order_no', ['placeholder' => __('Trust Order No'), 'class' => 'form-control']);
            echo $this->Form->input('date', ['placeholder' => __('Trust Order Date'), 'label' => __('Trust Order Date'), 'type' => 'text', 'class' => 'form-control hasDatePicker']);
            echo $this->Form->input('trusted_person', ['placeholder' => __('Trusted Person'), 'class' => 'form-control']);
            echo $this->Form->input('content', ['placeholder' => __('Trust Order Content'), 'placeholder' => __('Trust Order Content'), 'class' => 'editor','templateVars'=>['div_ids'=>'style="margin-bottom: 21px;"']]);
            ?>
            <div class="clear"></div>
            <div class="row">
                <div class="col-lg-4 col-md-4">
                    <?php
                    echo $this->Form->input('permisson_signature', ['label' => ['class' => 'checkbox-inline']]);
                    ?>
                </div>
                <div class="col-lg-4 col-md-4">
                    <?php
                    echo $this->Form->input('send_trust_order', ['label' => ['class' => 'checkbox-inline']]);
                    ?>
                </div>
            </div>

            <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-success btn-lg', 'templateVars' => ['icon' => '<i class="icon-check"></i>']]) ?>
            <?= $this->Form->button(__('Cancel'), ['class' => 'btn btn-warning btn-lg', 'id' => 'CancelBtn', 'templateVars' => ['icon' => '<i class="icon-cancel"></i>']]) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<?php echo $this->element('transaction_form') ?>
<?php
echo $this->Html->css(['wysiwyg-editor/editor.css'], ['block' => true]);
echo $this->Html->script(['wysiwyg-editor/editor.js'], ['block' => true]);
echo $this->append('script')
?>
<script>
    $(document).ready(function () {
        $(".editor").Editor();
    });
</script>
<?php
echo $this->end()?>
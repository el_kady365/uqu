<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="text-danger">
                <?php
                echo __('Transaction Details');
                ?>
            </h3>
            <hr />
        </div>
        <div class="panel-body">
            <?php
            $this->Form->create($transaction);
            echo $this->Form->input('id', ['readonly' => 'readonly', 'placeholder' => __('Transaction Number'), 'type' => 'text', 'label' => __('Transaction Number'), 'class' => 'form-control']);
            
            echo $this->Form->input('date', ['readonly' => 'readonly', 'type' => 'text', 'class' => 'form-control', 'placeholder' => __('Transaction Date'), 'label' => __('Transaction Date')]);
            echo $this->Form->input('vertical_coding_number', ['readonly' => 'readonly', 'placeholder' => __('Vertical Coding Number'), 'class' => 'form-control']);
            echo $this->Form->input('copies_number', ['readonly' => 'readonly', 'placeholder' => __('Copies Number'), 'class' => 'form-control']);
            echo $this->Form->input('contractor_id', ['disabled' => 'disabled', 'options' => $contractors, 'class' => 'form-control', 'empty' => __('Choose Contractor')]);
            echo $this->Form->input('price', ['readonly' => 'readonly', 'label' => __('The Price'), 'placeholder' => __('The Price'), 'class' => 'form-control']);
            echo $this->Form->input('purpose_id', ['disabled' => 'disabled', 'options' => $purposes, 'class' => 'form-control', 'empty' => __('Choose Purpose')]);
            echo $this->Form->input('user_full_name', ['readonly' => 'readonly', 'class' => 'form-control', 'label' => __('Transaction registry user')]);
            echo $this->Form->input('created', ['readonly' => 'readonly', 'class' => 'form-control', 'type' => 'text', 'label' => __('Creation Date')]);
            echo $this->Form->input('last_update_by', ['readonly' => 'readonly', 'class' => 'form-control', 'type' => 'text', 'label' => __('Last Update By')]);
            echo $this->Form->input('updated', ['readonly' => 'readonly', 'class' => 'form-control', 'type' => 'text', 'label' => __('Last Update Date')]);
            $this->Form->end()
            ?>
        </div>
    </div>
</div>

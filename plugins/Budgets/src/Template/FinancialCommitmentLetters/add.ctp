<?php
/**
 * @var \App\View\AppView $this
 */
?>

<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="text-danger">
                <?php
                if (!$financialCommitmentLetter->id) {
                    echo __('Financial Commitment Letter');
                } else {
                    echo __('Financial Commitment Letter');
                }
                ?></h3>
            <hr />
        </div>
        <div class="panel-body">
            <?= $this->Form->create($financialCommitmentLetter) ?>

            <?php
            echo $this->Form->input('letter_no', ['placeholder' => __('Letter No'), 'class' => 'form-control']);
            echo $this->Form->input('date', ['placeholder' => __('Letter Date'), 'label' => __('Letter Date'), 'type' => 'text', 'class' => 'form-control hasDatePicker']);
            echo $this->Form->input('sender', ['placeholder' => __('Sender'), 'class' => 'form-control']);
            echo $this->Form->input('receiver', ['placeholder' => __('To'), 'label' => __('To'), 'class' => 'form-control']);
            echo $this->Form->input('attachments', ['placeholder' => __('Attachments'), 'class' => 'form-control']);
            echo $this->Form->input('financial_commitment_no', ['placeholder' => __('Financial Commitment No.'), 'label' => __('Financial Commitment No.'), 'class' => 'form-control']);
//            echo $this->Form->input('transaction_id', ['placeholder' => __('Transaction Id'), 'class' => 'form-control']);
            ?>

            <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-success btn-lg', 'templateVars' => ['icon' => '<i class="icon-check"></i>']]) ?>
            <?= $this->Form->button(__('Cancel'), ['class' => 'btn btn-warning btn-lg', 'id' => 'CancelBtn', 'templateVars' => ['icon' => '<i class="icon-cancel"></i>']]) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
<?php echo $this->element('transaction_form')?>

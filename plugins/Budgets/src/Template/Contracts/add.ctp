<?php
/**
 * @var \App\View\AppView $this
 */
?>

<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="text-danger">
                <?php
                if (!$contract->id) {
                    echo __('Contract');
                } else {
                    echo __('Contract');
                }
                ?></h3>
            <hr />
        </div>
        <div class="panel-body">
            <?= $this->Form->create($contract) ?>

            <?php
            echo $this->Form->input('contract_no', ['placeholder' => __('Contract No'), 'class' => 'form-control']);
            echo $this->Form->input('date', ['placeholder' => __('Contract Date'), 'label' => __('Contract Date'), 'type' => 'text', 'class' => 'form-control hasDatePicker']);
            echo $this->Form->input('signed_by', ['placeholder' => __('Signed By'), 'class' => 'form-control']);
            echo $this->Form->input('contract_title', ['placeholder' => __('Contract Title'), 'class' => 'form-control']);
            ?>
            <div class="row">
                <div class="col-lg-4 col-md-4">
                    <?php
                    echo $this->Form->input('send_to_revision', ['label' => ['class' => 'checkbox-inline']]);
                    ?>
                </div>
                <div class="col-lg-4 col-md-4">
                    <?php
                    echo $this->Form->input('commitment_received', ['label' => ['class' => 'checkbox-inline']]);
                    ?>
                </div>
                <div class="col-lg-4 col-md-4">
                    <?php
                    echo $this->Form->input('signatured', ['label' => ['class' => 'checkbox-inline']]);
                    ?>
                </div>
            </div>
            <div class="row gutter">
                <h4><?php echo __('Send To Regulators') ?>:</h4>
                <?php
                echo $this->Form->input('send_to_social_security', ['label' => ['text' => __('Social Security'), 'class' => 'checkbox-inline']]);
                echo $this->Form->input('send_to_zakah_income', ['label' => ['text' => __('Zakah Income'), 'class' => 'checkbox-inline']]);
                echo $this->Form->input('send_to_investigation', ['label' => ['text' => __('Investigation'), 'class' => 'checkbox-inline']]);
                echo $this->Form->input('send_to_commecial_chamber', ['label' => ['text' => __('Commercial Chamber'), 'class' => 'checkbox-inline']]);
                echo $this->Form->input('send_to_other', ['label' => ['text' => __('Other'), 'class' => 'checkbox-inline']]);
                echo $this->Form->input('other_sent', ['placeholder' => __('Other'), 'label' => __('Other'), 'class' => 'form-control', 'templateVars' => ['div_classes' => 'hide', 'div_ids' => 'id="Other"']]);
                ?>
            </div>
            <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-success btn-lg', 'templateVars' => ['icon' => '<i class="icon-check"></i>']]) ?>
            <?= $this->Form->button(__('Cancel'), ['class' => 'btn btn-warning btn-lg', 'id' => 'CancelBtn', 'templateVars' => ['icon' => '<i class="icon-cancel"></i>']]) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
<?php echo $this->element('transaction_form') ?>
<?php echo $this->append('script') ?>
<script>
    $(function () {
        $('#send-to-other').on('change', function () {
            if ($(this).prop('checked')) {
                $('#Other').removeClass('hide').show();
            } else {
                $('#Other').addClass('hide').hide();
            }
        }).trigger('change');
    });
</script>
<?php echo $this->end() ?>

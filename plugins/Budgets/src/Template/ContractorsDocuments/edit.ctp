<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $contractorsDocument->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $contractorsDocument->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Contractors Documents'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Contractors'), ['controller' => 'Contractors', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Contractor'), ['controller' => 'Contractors', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="contractorsDocuments form large-9 medium-8 columns content">
    <?= $this->Form->create($contractorsDocument) ?>
    <fieldset>
        <legend><?= __('Edit Contractors Document') ?></legend>
        <?php
            echo $this->Form->input('contractor_id', ['options' => $contractors]);
            echo $this->Form->input('name');
            echo $this->Form->input('expire_date');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>

<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Contractors Document'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Contractors'), ['controller' => 'Contractors', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Contractor'), ['controller' => 'Contractors', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="contractorsDocuments index large-9 medium-8 columns content">
    <h3><?= __('Contractors Documents') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('contractor_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('expire_date') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($contractorsDocuments as $contractorsDocument): ?>
            <tr>
                <td><?= $this->Number->format($contractorsDocument->id) ?></td>
                <td><?= $contractorsDocument->has('contractor') ? $this->Html->link($contractorsDocument->contractor->name, ['controller' => 'Contractors', 'action' => 'view', $contractorsDocument->contractor->id]) : '' ?></td>
                <td><?= h($contractorsDocument->name) ?></td>
                <td><?= h($contractorsDocument->expire_date) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $contractorsDocument->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $contractorsDocument->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $contractorsDocument->id], ['confirm' => __('Are you sure you want to delete # {0}?', $contractorsDocument->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>

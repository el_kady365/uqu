<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Contractors Document'), ['action' => 'edit', $contractorsDocument->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Contractors Document'), ['action' => 'delete', $contractorsDocument->id], ['confirm' => __('Are you sure you want to delete # {0}?', $contractorsDocument->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Contractors Documents'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Contractors Document'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Contractors'), ['controller' => 'Contractors', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Contractor'), ['controller' => 'Contractors', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="contractorsDocuments view large-9 medium-8 columns content">
    <h3><?= h($contractorsDocument->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Contractor') ?></th>
            <td><?= $contractorsDocument->has('contractor') ? $this->Html->link($contractorsDocument->contractor->name, ['controller' => 'Contractors', 'action' => 'view', $contractorsDocument->contractor->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($contractorsDocument->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($contractorsDocument->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Expire Date') ?></th>
            <td><?= h($contractorsDocument->expire_date) ?></td>
        </tr>
    </table>
</div>

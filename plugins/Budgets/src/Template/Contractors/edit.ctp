
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="text-danger">
                <?php if (!$contractor->id) {
                echo __('Add Contractor');
                } else {
                echo __('Edit Contractor');
                }
                ?></h3>
            <hr />
        </div>
        <div class="panel-body">
            <?= $this->Form->create($contractor) ?>

            <?php
                        echo $this->Form->input('username', ['placeholder'=>__('Username'),'class' => 'form-control']);
                                    echo $this->Form->input('password', ['placeholder'=>__('Password'),'class' => 'form-control']);
                                    echo $this->Form->input('name', ['placeholder'=>__('Name'),'class' => 'form-control']);
                                    ?>

            <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-success btn-lg', 'templateVars' => ['icon' => '<i class="icon-check"></i>']]) ?>
            <?= $this->Form->button(__('Cancel'), ['class' => 'btn btn-warning btn-lg', 'id' => 'CancelBtn', 'templateVars' => ['icon' => '<i class="icon-cancel"></i>']]) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

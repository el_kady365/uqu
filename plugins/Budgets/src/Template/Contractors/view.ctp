<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Contractor'), ['action' => 'edit', $contractor->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Contractor'), ['action' => 'delete', $contractor->id], ['confirm' => __('Are you sure you want to delete # {0}?', $contractor->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Contractors'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Contractor'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Transactions'), ['controller' => 'Transactions', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Transaction'), ['controller' => 'Transactions', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="contractors view large-9 medium-8 columns content">
    <h3><?= h($contractor->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Username') ?></th>
            <td><?= h($contractor->username) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Password') ?></th>
            <td><?= h($contractor->password) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($contractor->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($contractor->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($contractor->created) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Transactions') ?></h4>
        <?php if (!empty($contractor->transactions)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Transactions Type Id') ?></th>
                <th scope="col"><?= __('Date') ?></th>
                <th scope="col"><?= __('Contractor Id') ?></th>
                <th scope="col"><?= __('Transaction Id') ?></th>
                <th scope="col"><?= __('Price') ?></th>
                <th scope="col"><?= __('Purpose Id') ?></th>
                <th scope="col"><?= __('Financial Commitment Letter Id') ?></th>
                <th scope="col"><?= __('Trust Order Id') ?></th>
                <th scope="col"><?= __('Contract Id') ?></th>
                <th scope="col"><?= __('Next Exchange Date') ?></th>
                <th scope="col"><?= __('Vertical Coding Number') ?></th>
                <th scope="col"><?= __('Copies Number') ?></th>
                <th scope="col"><?= __('Notes') ?></th>
                <th scope="col"><?= __('Dean Signature') ?></th>
                <th scope="col"><?= __('Finanical Commitment Letter Sent') ?></th>
                <th scope="col"><?= __('Status') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Updated') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($contractor->transactions as $transactions): ?>
            <tr>
                <td><?= h($transactions->id) ?></td>
                <td><?= h($transactions->transactions_type_id) ?></td>
                <td><?= h($transactions->date) ?></td>
                <td><?= h($transactions->contractor_id) ?></td>
                <td><?= h($transactions->transaction_id) ?></td>
                <td><?= h($transactions->price) ?></td>
                <td><?= h($transactions->purpose_id) ?></td>
                <td><?= h($transactions->financial_commitment_letter_id) ?></td>
                <td><?= h($transactions->trust_order_id) ?></td>
                <td><?= h($transactions->contract_id) ?></td>
                <td><?= h($transactions->next_exchange_date) ?></td>
                <td><?= h($transactions->vertical_coding_number) ?></td>
                <td><?= h($transactions->copies_number) ?></td>
                <td><?= h($transactions->notes) ?></td>
                <td><?= h($transactions->dean_signature) ?></td>
                <td><?= h($transactions->finanical_commitment_letter_sent) ?></td>
                <td><?= h($transactions->status) ?></td>
                <td><?= h($transactions->user_id) ?></td>
                <td><?= h($transactions->created) ?></td>
                <td><?= h($transactions->updated) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Transactions', 'action' => 'view', $transactions->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Transactions', 'action' => 'edit', $transactions->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Transactions', 'action' => 'delete', $transactions->id], ['confirm' => __('Are you sure you want to delete # {0}?', $transactions->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>

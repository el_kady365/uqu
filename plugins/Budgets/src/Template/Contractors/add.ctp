
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="text-danger">
                <?php
                if (!$contractor->id) {
                    echo __('Add Contractor');
                } else {
                    echo __('Edit Contractor');
                }
                ?></h3>
            <hr />
        </div>
        <div class="panel-body">
            <?= $this->Form->create($contractor, ['type' => 'file']) ?>

            <?php
            echo $this->Form->input('name', ['placeholder' => __('Name'), 'class' => 'form-control']);
            ?>
               <h3 class="text-danger">الوثائق</h3>        

             <hr>
            
            <div id="Options">
                 <div class="attachment-div">
                 <input name="" class="form-control" id="financial-attachments-0-id" value="1" type="hidden">           
                            <div class="col-sm-6">
                                <div class="form-group ">
                                     <?php  
                                    // echo $this->Form->input('doc["name"][key]', ['type' => 'text', 'class' => 'form-control', 'placeholder' => "اسم الوثيقه", 'label' => "اسم الوثيقه"]);   
                                  ?>
                                    <input name='contacttors_documents[0][name]' type="text" class="form-control" />
                                </div> 
                            </div>
                 
                 <div class="col-sm-2">
                     <div class="form-group ">
                         <?php
                         //echo $this->Form->input('doc["fil"][key]', ['type' => 'file', 'class' => 'form-control hasDatePicker', 'placeholder' => "تاريخ الانتهاء", 'label' => "رفع الوثيقه"]);
                         ?>
                         <input name='contacttors_documents[0][pdf_file]'  type="file"  class="form-control" />
                     </div>                           
                 </div>
                            <div class="col-sm-2">
                                <div class="form-group ">
                                    <?php  
                                    // echo $this->Form->input('date[key]', ['name'=>'doc["expire"][key]','type' => 'text', 'class' => 'form-control hasDatePicker', 'placeholder' => "تاريخ الانتهاء", 'label' => "تاريخ الانتهاء"]);   
                                  ?>
                                  <input name='contacttors_documents[0][expire_date]'  type="text"  class="form-control hasDatePicker" />
                                </div>                           
                            </div>

                            <div class="col-sm-2">
                                <a href="#" class="btn btn-danger btn-sm remove-attachment"><i class="fa fa-close"></i></a>
                            </div>
                            <div class="clearfix"></div>
 </div>
                  
                                        
            </div>

            <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-success btn-lg', 'templateVars' => ['icon' => '<i class="icon-check"></i>']]) ?>
            <?= $this->Form->button(__('Cancel'), ['class' => 'btn btn-warning btn-lg', 'id' => 'CancelBtn', 'templateVars' => ['icon' => '<i class="icon-cancel"></i>']]) ?>
            <button type="button" class=" add-doc btn btn-info btn-lg pull-left">
									<i class="   icon-control_point"></i>
									<b>اضافة وثيقة جديدة</b>
	    </button>
       <?= $this->Form->end() ?>
             
             <div id="document" style="display: none">
                  <div class="attachment-div">
                 <input name="" class="form-control" id="financial-attachments-0-id" value="1" type="hidden">           
                            <div class="col-sm-6">
                                <div class="form-group ">
                                     <?php  
                                    // echo $this->Form->input('doc["name"][key]', ['type' => 'text', 'class' => 'form-control', 'placeholder' => "اسم الوثيقه", 'label' => "اسم الوثيقه"]);   
                                  ?>
                                    <input name='contacttors_documents[key][name]' type="text" class="form-control" />
                                </div> 
                            </div>
                 
                 <div class="col-sm-2">
                     <div class="form-group ">
                         <?php
                         //echo $this->Form->input('doc["fil"][key]', ['type' => 'file', 'class' => 'form-control hasDatePicker', 'placeholder' => "تاريخ الانتهاء", 'label' => "رفع الوثيقه"]);
                         ?>
                         <input name='contacttors_documents[key][fil]'  type="file"  class="form-control" />
                     </div>                           
                 </div>
                            <div class="col-sm-2">
                                <div class="form-group ">
                                    <?php  
                                    // echo $this->Form->input('date[key]', ['name'=>'doc["expire"][key]','type' => 'text', 'class' => 'form-control hasDatePicker', 'placeholder' => "تاريخ الانتهاء", 'label' => "تاريخ الانتهاء"]);   
                                  ?>
                                  <input name='contacttors_documents[key][expire]'  type="text"  class="form-control hasDatePicker" />
                                </div>                           
                            </div>

                            <div class="col-sm-2">
                                <a href="#" class="btn btn-danger btn-sm remove-attachment"><i class="fa fa-close"></i></a>
                            </div>
                            <div class="clearfix"></div>
 </div>
             </div>
        </div>
    </div>
 <?php echo $this->append('script'); ?>   
    <script>


    /*----------------------------------------------add document btn-----------------------------------------------*/
        var i=1;
        $('.add-doc').on('click', function () {
           // alert("ddd");
            var htm=$('#document').html().replace(/key/g, i); ;
            $('#Options').append(htm);
             i++;
            return false;
        });
      /*---------------------------------------------- end add document btn-----------------------------------------------*/ 
      
      
     /*----------------------------------------------  remove document btn-----------------------------------------------*/

    $(document).on('click', '.remove-attachment', function () {
         
          
        if ($('.attachment-div').length > 1) {
            $(this).closest('.attachment-div').remove();
        }

        return false;
    });
   /*---------------------------------------------- end add document btn-----------------------------------------------*/ 
</script>
<?php echo $this->end(); ?>
</div>

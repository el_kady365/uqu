
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="text-danger">
                <?php if (!$transaction->id) {
                echo __('Add Transaction');
                } else {
                echo __('Edit Transaction');
                }
                ?></h3>
            <hr />
        </div>
        <div class="panel-body">
            <?= $this->Form->create($transaction) ?>

            <?php
                        echo $this->Form->input('transaction_type_id', ['placeholder'=>__('Transaction Type Id'),'class' => 'form-control']);
                                    echo $this->Form->input('date', ['empty' => true]);
                                    echo $this->Form->input('contractor_id', ['options' => $contractors, 'empty' => true]);
                                    echo $this->Form->input('transaction_id', ['placeholder'=>__('Transaction Id'),'class' => 'form-control']);
                                    echo $this->Form->input('price', ['placeholder'=>__('Price'),'class' => 'form-control']);
                                    echo $this->Form->input('purpose_id', ['options' => $purposes, 'empty' => true]);
                                    echo $this->Form->input('finanical_commitment_letter_id', ['placeholder'=>__('Finanical Commitment Letter Id'),'class' => 'form-control']);
                                    echo $this->Form->input('trust_order_id', ['options' => $trustOrders, 'empty' => true]);
                                    echo $this->Form->input('contract_id', ['options' => $contracts, 'empty' => true]);
                                    echo $this->Form->input('vertical_coding_number', ['placeholder'=>__('Vertical Coding Number'),'class' => 'form-control']);
                                    echo $this->Form->input('copies_number', ['placeholder'=>__('Copies Number'),'class' => 'form-control']);
                                    echo $this->Form->input('notes', ['placeholder'=>__('Notes'),'class' => 'form-control']);
                                    echo $this->Form->input('dean_signature',['label'=>['class'=>'checkbox-inline']]);
                                    echo $this->Form->input('finanical_commitment_letter_sent',['label'=>['class'=>'checkbox-inline']]);
                                    echo $this->Form->input('status', ['placeholder'=>__('Status'),'class' => 'form-control']);
                                    echo $this->Form->input('user_id', ['options' => $users, 'empty' => true]);
                                    ?>

            <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-success btn-lg', 'templateVars' => ['icon' => '<i class="icon-check"></i>']]) ?>
            <?= $this->Form->button(__('Cancel'), ['class' => 'btn btn-warning btn-lg', 'id' => 'CancelBtn', 'templateVars' => ['icon' => '<i class="icon-cancel"></i>']]) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

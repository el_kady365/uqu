
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="text-danger">
                <?php
                if (!$transaction->id) {
                    echo __('Add Transaction');
                } else {
                    echo __('Edit Transaction');
                }
                ?></h3>
            <hr />
        </div>
        <div class="panel-body">
            <?= $this->Form->create($transaction) ?>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group <?php echo!empty($this->Form->error('transactions_type_id')) ? "has-error" : ''; ?>">
                        <?php echo $this->Form->label('transactions_type_id') ?>
                        <select class="form-control" name="transactions_type_id" id="transactions-type-id">
                            <option value="" ><?php echo __('Transactions Type'); ?></option>
                            <?php
                            foreach ($transactionsTypes as $transactionsType) {
                                $selected = '';
                                if ($transactionsType['id'] == $transaction->transactions_type_id)
                                    $selected = 'selected="selected"';
                                ?>
                                <option <?php echo $selected ?> data-contract="<?php echo (int) $transactionsType['with_contract'] ?>" value="<?php echo $transactionsType['id'] ?>"><?php echo $transactionsType['title'] ?></option>
                            <?php }
                            ?>
                        </select>
                        <?php echo $this->Form->error('transactions_type_id'); ?>
                    </div>
                    <?php
                    echo $this->element('radio_buttons', ['field' => 'TranasctionsType.with_contract', 'disabled' => true, 'options' => [1 => __('Yes'), 0 => __('No')]]);
                    echo $this->element('radio_buttons', ['field' => 'old_transaction', 'labelTitle' => __('Related To Previous Transaction'), 'selected' => $transaction->old_transaction ? $transaction->old_transaction : '0', 'options' => [1 => __('Yes'), 0 => __('No')]]);
                    echo $this->Form->input('transaction_id', ['label' => __('Previous Transaction No.'), 'empty' => __('Select Transaction'), 'class' => 'form-control', 'templateVars' => ['div_classes' => 'hide', 'div_ids' => 'id="TransactionID"']]);
                    echo $this->Form->input('financial_commitment_letter.letter_no', ['placeholder' => __('Financial Commitment Letter'), 'label' => __('Financial Commitment Letter'), 'class' => 'form-control', 'readonly' => 'readonly']);
                    echo $this->Form->input('financial_commitment_letter.financial_commitment_no', ['label' => __('Financial Commitment No.'), 'placeholder' => __('Financial Commitment No.'), 'class' => 'form-control', 'readonly' => 'readonly']);
                    ?>
                </div>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('date', ['type' => 'text', 'class' => 'form-control hasDatePicker', 'placeholder' => __('Transaction Date'), 'label' => __('Transaction Date')]);
                    echo $this->Form->input('contractor_id', ['options' => $contractors, 'class' => 'form-control', 'empty' => __('Choose Contractor')]);
                    echo $this->Form->input('price', ['label' => __('The Price'), 'placeholder' => __('The Price'), 'class' => 'form-control']);
                    echo $this->Form->input('purpose_id', ['options' => $purposes, 'class' => 'form-control', 'empty' => __('Choose Purpose')]);
                    echo $this->Form->input('trust_order.trust_order_no', ['label' => __('Trust Order No'), 'class' => 'form-control', 'readonly' => 'readonly']);
                    echo $this->Form->input('contract.contract_no', ['class' => 'form-control', 'label' => __('Contract No.'), 'placeholder' => __('Contract No.'), 'readonly' => 'readonly']);
                    ?>
                </div>
            </div>

            <?php
            echo $this->Form->input('next_exchange_date', ['type' => 'text', 'class' => 'form-control hasDatePicker', 'placeholder' => __('Next Exchange Date')]);
            ?>
            <div class="row">
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('vertical_coding_number', ['placeholder' => __('Vertical Coding Number'), 'class' => 'form-control']);
                    ?>
                </div>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('copies_number', ['placeholder' => __('Copies Number'), 'class' => 'form-control']);
                    ?>
                </div>
            </div>

            <?php
            echo $this->Form->input('notes', ['placeholder' => __('Notes'), 'class' => 'form-control']);
            ?>
            <div class="row">
                <div class="col-lg-2 col-md-3">
                    <?php
                    echo $this->Form->input('dean_signature', ['label' => ['class' => 'checkbox-inline']]);
                    ?>
                </div>
                <div class="col-lg-2 col-md-3">
                    <?php
                    echo $this->Form->input('finanical_commitment_letter_sent', ['label' => ['class' => 'checkbox-inline']]);
                    ?>
                </div>
            </div>
            <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-success btn-lg', 'templateVars' => ['icon' => '<i class="icon-check"></i>']]) ?>
            <?= $this->Form->button(__('Cancel'), ['class' => 'btn btn-warning btn-lg', 'id' => 'CancelBtn', 'templateVars' => ['icon' => '<i class="icon-cancel"></i>']]) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
<?php echo $this->append('script') ?>
<script>
    $(function () {
        $('#transactions-type-id').on('change', function () {
            var withContract = $(this).find('option:selected').data('contract');
            if (withContract != undefined) {
                $('#tranasctionstype-with-contract-' + withContract).prop('checked', 'checked');
            } else {
                $('#tranasctionstype-with-contract-0').removeProp('checked');
                $('#tranasctionstype-with-contract-1').removeProp('checked');
            }
        }).trigger('change');
        $('#old-transaction-1').on('change', function () {
            if ($(this).prop('checked')) {
                $('#TransactionID').removeClass('hide').show();
                $('#transaction-id').select2({dir: "rtl", width: '100%'});

            }
        }).change();
        
        $('#old-transaction-0').on('change', function () {
            if ($(this).prop('checked')) {
                $('#TransactionID').addClass('hide').hide();
            }
        }).change();
    });
</script>
<?php
echo $this->end()?>
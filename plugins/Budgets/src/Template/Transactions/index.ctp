
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="text-danger"><?= __('Transactions') ?></h3>
            <hr />
        </div>
        <div class="panel-body">
            <form action="<?php echo $this->Url->build(array("action" => "do-operation")) ?>" method="post">
                <!--Table Wrapper Start-->
                <div class="table-responsive ls-table">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th><?= $this->Paginator->sort('id', __('Transaction Number')) ?></th>
                                <th><?= $this->Paginator->sort('transactions_type_id') ?></th>
                                <th><?= $this->Paginator->sort('transaction_item', __('The Item')) ?></th>
                                <th><?= $this->Paginator->sort('price', __('The Price')) ?></th>
                                <th><?= $this->Paginator->sort('reference', __('The Reference')) ?></th>
                                <th><?= $this->Paginator->sort('reference_no', __('The Reference No.')) ?></th>
                                <th><?= $this->Paginator->sort('vertical_coding_number', __('Coding No.')) ?></th>


                                <th class="actions"><?= __('Actions') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($transactions as $transaction):
                                ?>
                                <tr>
                                    <td><?= $this->Number->format($transaction->id) ?></td>
                                    <td><?= h($transaction->transactions_type->title) ?></td>
                                    <td><?=h( $transaction->transactions_type->budget_item->item_number) ?></td>
                                    <td><?= $this->Number->currency($transaction->price, 'SAR', ['pattern' => '#,###.00 ¤']) ?></td>                                  
                                    <td><?= h($transaction->dean_signature) ?></td>
                                    <td><?= h($transaction->finanical_commitment_letter_sent) ?></td>
                                    <td><?= h($transaction->vertical_coding_number) ?></td>
                                    <td class="actions">
                                        <?= $this->Html->link('<i class="icon-tune"></i>', ['action' => 'edit', $transaction->id], ['class' => 'btn btn-success icon-btn btn-rounded', 'escape' => false]) ?>
                                        <?= $this->Html->link('<i class="icon-cancel2"></i>', ['action' => 'delete', $transaction->id], ['class' => 'btn btn-danger icon-btn btn-rounded', 'escape' => false, 'confirm' => __('Are you sure you want to delete # {0}?', $transaction->id)]) ?>


                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>

                </div>
                <div class="row">
                    <div class="col-md-3">
                    </div>
                    <div class="col-md-9 text-left">
                        <ul class="pagination ls-pagination">
                            <?php
                            if ($this->Paginator->hasPrev()) {
                                echo $this->Paginator->prev('< ' . __('previous'));
                            }
                            echo $this->Paginator->numbers();
                            if ($this->Paginator->hasNext()) {
                                echo $this->Paginator->next(__('next') . ' >');
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
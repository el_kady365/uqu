<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Transaction'), ['action' => 'edit', $transaction->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Transaction'), ['action' => 'delete', $transaction->id], ['confirm' => __('Are you sure you want to delete # {0}?', $transaction->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Transactions'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Transaction'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Contractors'), ['controller' => 'Contractors', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Contractor'), ['controller' => 'Contractors', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Purposes'), ['controller' => 'Purposes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Purpose'), ['controller' => 'Purposes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Trust Orders'), ['controller' => 'TrustOrders', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Trust Order'), ['controller' => 'TrustOrders', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Contracts'), ['controller' => 'Contracts', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Contract'), ['controller' => 'Contracts', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Transactions'), ['controller' => 'Transactions', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Transaction'), ['controller' => 'Transactions', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="transactions view large-9 medium-8 columns content">
    <h3><?= h($transaction->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Contractor') ?></th>
            <td><?= $transaction->has('contractor') ? $this->Html->link($transaction->contractor->name, ['controller' => 'Contractors', 'action' => 'view', $transaction->contractor->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Purpose') ?></th>
            <td><?= $transaction->has('purpose') ? $this->Html->link($transaction->purpose->title, ['controller' => 'Purposes', 'action' => 'view', $transaction->purpose->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Trust Order') ?></th>
            <td><?= $transaction->has('trust_order') ? $this->Html->link($transaction->trust_order->id, ['controller' => 'TrustOrders', 'action' => 'view', $transaction->trust_order->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Contract') ?></th>
            <td><?= $transaction->has('contract') ? $this->Html->link($transaction->contract->id, ['controller' => 'Contracts', 'action' => 'view', $transaction->contract->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Vertical Coding Number') ?></th>
            <td><?= h($transaction->vertical_coding_number) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $transaction->has('user') ? $this->Html->link($transaction->user->id, ['controller' => 'Users', 'action' => 'view', $transaction->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($transaction->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Transaction Type Id') ?></th>
            <td><?= $this->Number->format($transaction->transaction_type_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Transaction Id') ?></th>
            <td><?= $this->Number->format($transaction->transaction_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Price') ?></th>
            <td><?= $this->Number->format($transaction->price) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Finanical Commitment Letter Id') ?></th>
            <td><?= $this->Number->format($transaction->finanical_commitment_letter_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Copies Number') ?></th>
            <td><?= $this->Number->format($transaction->copies_number) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= $this->Number->format($transaction->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date') ?></th>
            <td><?= h($transaction->date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($transaction->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Updated') ?></th>
            <td><?= h($transaction->updated) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Dean Signature') ?></th>
            <td><?= $transaction->dean_signature ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Finanical Commitment Letter Sent') ?></th>
            <td><?= $transaction->finanical_commitment_letter_sent ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Notes') ?></h4>
        <?= $this->Text->autoParagraph(h($transaction->notes)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Transactions') ?></h4>
        <?php if (!empty($transaction->transactions)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Transaction Type Id') ?></th>
                <th scope="col"><?= __('Date') ?></th>
                <th scope="col"><?= __('Contractor Id') ?></th>
                <th scope="col"><?= __('Transaction Id') ?></th>
                <th scope="col"><?= __('Price') ?></th>
                <th scope="col"><?= __('Purpose Id') ?></th>
                <th scope="col"><?= __('Finanical Commitment Letter Id') ?></th>
                <th scope="col"><?= __('Trust Order Id') ?></th>
                <th scope="col"><?= __('Contract Id') ?></th>
                <th scope="col"><?= __('Vertical Coding Number') ?></th>
                <th scope="col"><?= __('Copies Number') ?></th>
                <th scope="col"><?= __('Notes') ?></th>
                <th scope="col"><?= __('Dean Signature') ?></th>
                <th scope="col"><?= __('Finanical Commitment Letter Sent') ?></th>
                <th scope="col"><?= __('Status') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Updated') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($transaction->transactions as $transactions): ?>
            <tr>
                <td><?= h($transactions->id) ?></td>
                <td><?= h($transactions->transaction_type_id) ?></td>
                <td><?= h($transactions->date) ?></td>
                <td><?= h($transactions->contractor_id) ?></td>
                <td><?= h($transactions->transaction_id) ?></td>
                <td><?= h($transactions->price) ?></td>
                <td><?= h($transactions->purpose_id) ?></td>
                <td><?= h($transactions->finanical_commitment_letter_id) ?></td>
                <td><?= h($transactions->trust_order_id) ?></td>
                <td><?= h($transactions->contract_id) ?></td>
                <td><?= h($transactions->vertical_coding_number) ?></td>
                <td><?= h($transactions->copies_number) ?></td>
                <td><?= h($transactions->notes) ?></td>
                <td><?= h($transactions->dean_signature) ?></td>
                <td><?= h($transactions->finanical_commitment_letter_sent) ?></td>
                <td><?= h($transactions->status) ?></td>
                <td><?= h($transactions->user_id) ?></td>
                <td><?= h($transactions->created) ?></td>
                <td><?= h($transactions->updated) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Transactions', 'action' => 'view', $transactions->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Transactions', 'action' => 'edit', $transactions->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Transactions', 'action' => 'delete', $transactions->id], ['confirm' => __('Are you sure you want to delete # {0}?', $transactions->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>

<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Transactions Type'), ['action' => 'edit', $transactionsType->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Transactions Type'), ['action' => 'delete', $transactionsType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $transactionsType->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Transactions Types'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Transactions Type'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Budget Items'), ['controller' => 'BudgetItems', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Budget Item'), ['controller' => 'BudgetItems', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="transactionsTypes view large-9 medium-8 columns content">
    <h3><?= h($transactionsType->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Title') ?></th>
            <td><?= h($transactionsType->title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Budget Item') ?></th>
            <td><?= $transactionsType->has('budget_item') ? $this->Html->link($transactionsType->budget_item->id, ['controller' => 'BudgetItems', 'action' => 'view', $transactionsType->budget_item->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($transactionsType->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($transactionsType->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Updated') ?></th>
            <td><?= h($transactionsType->updated) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('With Contract') ?></th>
            <td><?= $transactionsType->with_contract ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('With Trust Order') ?></th>
            <td><?= $transactionsType->with_trust_order ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Active') ?></th>
            <td><?= $transactionsType->active ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
</div>

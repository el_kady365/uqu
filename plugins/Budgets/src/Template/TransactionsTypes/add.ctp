
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="text-danger">
                <?php
                if (!$transactionsType->id) {
                    echo __('Add Transactions Type');
                } else {
                    echo __('Edit Transactions Type');
                }
                ?></h3>
            <hr />
        </div>
        <div class="panel-body">
            <?= $this->Form->create($transactionsType) ?>
            <div class="row">
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('title', ['placeholder' => __('Title'), 'class' => 'form-control']);
                    echo $this->element('radio_buttons', ['field' => 'with_contract', 'options' => [1 => __('Yes'), 0 => __('No')]]);
                    
                    ?>

                </div>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('budget_item_id', ['class' => 'form-control', 'label' => __('Spend on item'), 'options' => $budgetItems, 'empty' => __('Choose Item')]);
                    echo $this->element('radio_buttons', ['field' => 'with_trust_order', 'options' => [1 => __('Yes'), 0 => __('No')]]);
                    
                    ?>
                </div>
            </div>
            <?php
//            echo $this->Form->input('active', ['label' => ['class' => 'checkbox-inline']]);
            ?>
            <hr />
            <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-success btn-lg', 'templateVars' => ['icon' => '<i class="icon-check"></i>']]) ?>
            <?= $this->Form->button(__('Cancel'), ['class' => 'btn btn-warning btn-lg', 'id' => 'CancelBtn', 'templateVars' => ['icon' => '<i class="icon-cancel"></i>']]) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

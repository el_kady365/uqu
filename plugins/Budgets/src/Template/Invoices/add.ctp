<?php
/**
 * @var \App\View\AppView $this
 */
?>


<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
    <?php echo $this->Form->create($invoice, ['type' => 'file']) ?>
    <div class="panel">
        <div class="panel-heading">
            <h3 class="text-danger">
                <?php
                echo __('Invoice Details');
                ?></h3>
            <hr />
        </div>
        <div class="panel-body">
            <?php
            echo $this->Form->input('invoice_islamic_date', ['type' => 'text', 'placeholder' => __('Invoice Islamic Date'), 'class' => 'form-control hasDatePicker']);
            echo $this->Form->input('invoice_gorgian_date', ['type' => 'text', 'placeholder' => __('Invoice Gregorian Date'), 'label' => __('Invoice Gregorian Date'), 'class' => 'form-control hasGorgianDatePicker']);
            echo $this->Form->input('invoice_no', ['placeholder' => __('Invoice No'), 'class' => 'form-control']);
            echo $this->Form->input('order_no', ['placeholder' => __('Order No'), 'class' => 'form-control']);
            echo $this->Form->input('addresses_count', ['placeholder' => __('Addresses Count'), 'class' => 'form-control']);
            ?>
            <div class="row">
                <div class="form-group col-md-6">
                    <?php
                    echo $this->Form->input('copies_count', ['placeholder' => __('Copies Count'), 'class' => 'form-control']);
                    ?>
                </div>
                <div class="form-group col-md-6">
                    <?php
                    echo $this->Form->input('starting_vertical_coding_no', ['placeholder' => __('Starting Vertical Coding No'), 'class' => 'form-control']);
                    ?>
                </div>
            </div>
            <?php
            echo $this->Form->input('price', ['label' => __('Price according to the invoice (SAR)'), 'placeholder' => __('Price'), 'class' => 'form-control']);
            ?>

        </div>
    </div>
    <div class="panel">
        <div class="panel-heading">
            <h3 class="text-danger">
                <?php
                echo __('Shipping Details If Exist');
                ?></h3>
            <hr />
        </div>
        <div class="panel-body">
            <?php
            echo $this->Form->input('shipping_invoice_gorgian_date', ['type' => 'text', 'placeholder' => __('Shipping Invoice Gorgian Date'), 'label' => __('Shipping Invoice Gorgian Date'), 'class' => 'form-control hasGorgianDatePicker']);
            echo $this->Form->input('shipping_invoice_no', ['placeholder' => __('Shipping Invoice No'), 'class' => 'form-control']);
            echo $this->Form->input('shipping_price', ['placeholder' => __('Shipping Price'), 'class' => 'form-control']);
            ?>
        </div>
    </div>
    <div class="panel">
        <div class="panel-heading">
            <h3 class="text-danger">
                <?php
                echo __('Additional Details');
                ?></h3>
            <hr />
        </div>
        <div class="panel-body">
            <?php
            echo $this->Form->input('notes', ['placeholder' => __('Notes'), 'class' => 'form-control editor']);
            ?>


            <?php
            echo $this->Form->input('pdf_file', array('type' => 'file', 'id' => 'filecount', 'label' => '', 'text' => 'icon-event_note', 'class' => 'btn btn-info btn-lg file-loading', 'templateVars' => ['between' => $this->element('image_between', ['file' => $invoice->pdf_file_info])]));
            ?>

            <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-success btn-lg', 'templateVars' => ['icon' => '<i class="icon-check"></i>']]) ?>
            <?= $this->Form->button(__('Cancel'), ['class' => 'btn btn-warning btn-lg', 'id' => 'CancelBtn', 'templateVars' => ['icon' => '<i class="icon-cancel"></i>']]) ?>
        </div>
    </div>
</div>
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="text-danger">
                <?php
                echo __('TrustOrder Details');
                ?></h3>
            <hr />
        </div>
        <div class="panel-body">

            <?php
            echo $this->Form->input('trust_supplier_name', ['placeholder' => __('Trust Supplier Name'), 'class' => 'form-control']);
            echo $this->Form->input('trust_financial_year', ['label' => __('Trust Financials Year'), 'placeholder' => __('Trust Financials Year'), 'class' => 'form-control']);
            echo $this->Form->input('trust_order.trust_order_no', ['class' => 'form-control', 'value' => $trans->trust_order_no]);

            echo $this->Form->input('trust_order.date', ['label' => __('Trust Order Date'), 'class' => 'form-control', 'value' => $trans->date]);
            echo $this->Form->input('trust_approximately_price', ['placeholder' => __('Trust Approximately Price'), 'class' => 'form-control']);
            echo $this->Form->input('trust_container_type', ['placeholder' => __('Trust Container Type'), 'class' => 'form-control']);
            echo $this->Form->input('trust_beneficiary', ['placeholder' => __('Trust Beneficiary'), 'class' => 'form-control']);
            echo $this->Form->input('user_full_name', ['placeholder' => __('User Full Name'), 'class' => 'form-control']);
            echo $this->Form->input('last_update_by', ['placeholder' => __('Last Update By'), 'class' => 'form-control']);
            ?>
        </div>
    </div>

    <?php echo $this->Form->end() ?>



    <?php
    echo $this->Html->css(['wysiwyg-editor/editor.css'], ['block' => true]);
    echo $this->Html->script(['wysiwyg-editor/editor.js'], ['block' => true]);
    echo $this->Html->script(['bootstrap.file-input.js'], ['block' => true]);
    echo $this->append('script')
    ?>
    <script>
        $(document).ready(function () {
            $(".editor").Editor();
        });
        $('#filecount').filestyle({
            input: false,
            buttonName: 'btn btn-info btn-lg',
            iconName: 'icon-event_note',
            buttonText: "الفاتورة المرفقة PDF",

        });
    </script>

    <?php echo $this->end() ?>
 

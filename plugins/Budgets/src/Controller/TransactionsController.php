<?php

namespace Budgets\Controller;

use Budgets\Controller\AppController;
use Cake\Routing\Router;
use Cake\Core\Configure;

/**
 * Transactions Controller
 *
 * @property \Budgets\Model\Table\TransactionsTable $Transactions
 */
class TransactionsController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->paginate = [
            'contain' => ['TransactionsTypes.BudgetItems', 'Contractors', 'Purposes', 'TrustOrders', 'Contracts']
        ];
        $transactions = $this->paginate($this->Transactions);

        $this->set(compact('transactions'));
        $this->set('_serialize', ['transactions']);
    }

    /**
     * View method
     *
     * @param string|null $id Transaction id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $transaction = $this->Transactions->get($id, [
            'contain' => ['TransactionsTypes.BudgetItems', 'Contractors', 'Purposes', 'FinancialCommitmentLetters', 'TrustOrders', 'Contracts', 'Users', 'Transactions']
        ]);

        $this->set('transaction', $transaction);
        $this->set('_serialize', ['transaction']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $transaction = $this->Transactions->newEntity();
        if ($this->request->is('post')) {
            $transaction = $this->Transactions->patchEntity($transaction, $this->request->data);
            if ($this->Transactions->save($transaction)) {
                $this->Flash->success(__('The transaction has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The transaction could not be saved. Please, try again.'));
            }
        }
        $transactionsTypes = $this->Transactions->TransactionsTypes->find('all', ['fields' => array('id', 'title', 'with_contract'), 'limit' => 200]);
        $contractors = $this->Transactions->Contractors->find('list', ['limit' => 200]);
        $purposes = $this->Transactions->Purposes->find('list', ['limit' => 200]);
        
        $trustOrders = $this->Transactions->TrustOrders->find('list', ['limit' => 200]);
        $contracts = $this->Transactions->Contracts->find('list', ['limit' => 200]);
        $transactions = $this->Transactions->find('list', ['limit' => 200]);
        
        $this->set(compact('transactions', 'transaction', 'transactionsTypes', 'contractors', 'purposes', 'trustOrders', 'contracts'));
        $this->set('_serialize', ['transaction']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Transaction id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $transaction = $this->Transactions->get($id, [
            'contain' => ['FinancialCommitmentLetters','TrustOrders','Contracts']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $transaction = $this->Transactions->patchEntity($transaction, $this->request->data);
            if ($this->Transactions->save($transaction)) {
                $this->Flash->success(__('The transaction has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The transaction could not be saved. Please, try again.'));
            }
        }
        $transactionsTypes = $this->Transactions->TransactionsTypes->find('all', ['fields' => array('id', 'title', 'with_contract'), 'limit' => 200]);
        $contractors = $this->Transactions->Contractors->find('list', ['limit' => 200]);
        $purposes = $this->Transactions->Purposes->find('list', ['limit' => 200]);
        
        $trustOrders = $this->Transactions->TrustOrders->find('list', ['limit' => 200]);
        $contracts = $this->Transactions->Contracts->find('list', ['limit' => 200]);
        
        $transactions = $this->Transactions->find('list', ['conditions' => ['Transactions.id != ' => $id], 'limit' => 200]);
        $this->set(compact('transaction', 'transactions', 'transactionsTypes', 'contractors', 'purposes', 'trustOrders', 'contracts'));

        $this->updateButtons('Transactions', 'edit', $id);


        $this->set('_serialize', ['transaction']);
        $this->render('add');
    }

    public function updateButtons($controller, $action, $id) {
        $buttons = Configure::read("Buttons.{$controller}.{$action}");
//        debug($buttons);
        foreach ($buttons as $key => $button) {
            Configure::write("Buttons.$controller.$action.$key.url", $button['url'] . '/' . $id);
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Transaction id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        // $this->request->allowMethod(['post', 'delete']);
        $transaction = $this->Transactions->get($id);
        if ($this->Transactions->delete($transaction)) {
            $this->Flash->success(__('The transaction has been deleted.'));
        } else {
            $this->Flash->error(__('The transaction could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

}

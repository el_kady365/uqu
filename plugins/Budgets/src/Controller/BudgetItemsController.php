<?php

namespace Budgets\Controller;

use Budgets\Controller\AppController;

/**
 * BudgetItems Controller
 *
 * @property \App\Model\Table\BudgetItemsTable $BudgetItems
 */
class BudgetItemsController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
//        $this->paginate = ['limit' => 1];
        $budgetItems = $this->paginate($this->BudgetItems);

        $this->set(compact('budgetItems'));
        $this->set('_serialize', ['budgetItems']);
        
    }

    /**
     * View method
     *
     * @param string|null $id Budget Item id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $budgetItem = $this->BudgetItems->get($id, [
            'contain' => []
        ]);

        $this->set('budgetItem', $budgetItem);
        $this->set('_serialize', ['budgetItem']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $budgetItem = $this->BudgetItems->newEntity();
        if ($this->request->is('post')) {
            $budgetItem = $this->BudgetItems->patchEntity($budgetItem, $this->request->data);
            if ($this->BudgetItems->save($budgetItem)) {
                $this->Flash->success(__('The budget item has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The budget item could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('budgetItem'));
        $this->set('_serialize', ['budgetItem']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Budget Item id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $budgetItem = $this->BudgetItems->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $budgetItem = $this->BudgetItems->patchEntity($budgetItem, $this->request->data);
            if ($this->BudgetItems->save($budgetItem)) {
                $this->Flash->success(__('The budget item has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The budget item could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('budgetItem'));
        $this->set('_serialize', ['budgetItem']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Budget Item id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
//        $this->request->allowMethod(['post', 'delete']);
        $budgetItem = $this->BudgetItems->get($id);
        if ($this->BudgetItems->delete($budgetItem)) {
            $this->Flash->success(__('The budget item has been deleted.'));
        } else {
            $this->Flash->error(__('The budget item could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

}

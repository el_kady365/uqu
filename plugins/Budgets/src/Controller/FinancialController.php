<?php
namespace Budgets\Controller;

use Budgets\Controller\AppController;

/**
 * Financial Controller
 *
 * @property \Budgets\Model\Table\FinancialTable $Financial
 */
class FinancialController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $financial = $this->paginate($this->Financial);

        $this->set(compact('financial'));
        $this->set('_serialize', ['financial']);
    }

    /**
     * View method
     *
     * @param string|null $id Financial id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $financial = $this->Financial->get($id, [
            'contain' => []
        ]);

        $this->set('financial', $financial);
        $this->set('_serialize', ['financial']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $financial = $this->Financial->newEntity();
        if ($this->request->is('post')) {
            $financial = $this->Financial->patchEntity($financial, $this->request->data);
            if ($this->Financial->save($financial)) {
                $this->Flash->success(__('The financial has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The financial could not be saved. Please, try again.'));
        }
        $this->set(compact('financial'));
        $this->set('_serialize', ['financial']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Financial id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $financial = $this->Financial->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $financial = $this->Financial->patchEntity($financial, $this->request->data);
            if ($this->Financial->save($financial)) {
                $this->Flash->success(__('The financial has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The financial could not be saved. Please, try again.'));
        }
        $this->set(compact('financial'));
        $this->set('_serialize', ['financial']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Financial id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $financial = $this->Financial->get($id);
        if ($this->Financial->delete($financial)) {
            $this->Flash->success(__('The financial has been deleted.'));
        } else {
            $this->Flash->error(__('The financial could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}

<?php

namespace Budgets\Controller;

use Budgets\Controller\AppController;
use Cake\Utility\Hash;

/**
 * FinancialItems Controller
 *
 * @property \Budgets\Model\Table\FinancialItemsTable $FinancialItems
 */
class FinancialItemsController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $financialItems = $this->paginate($this->FinancialItems);

        $this->set(compact('financialItems'));
        $this->set('_serialize', ['financialItems']);
    }

    /**
     * View method
     *
     * @param string|null $id Financial Item id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $financialItem = $this->FinancialItems->get($id, [
            'contain' => ['FinancialAttachements']
        ]);

        $this->set('financialItem', $financialItem);
        $this->set('_serialize', ['financialItems']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($transaction_id) {
        $financialItem = $this->FinancialItems->findOrCreate(['transaction_id' => $transaction_id]);
        $FinancialAttachments = $this->FinancialItems->FinancialAttachments->find()
                 ->where(['financial_item_id', $financialItem->id])->all();
//       
        if ($this->request->is(['patch', 'post', 'put'])) {
            $all_array = [];
            if (!$FinancialAttachments->isEmpty()) {
                foreach ($FinancialAttachments as $attach) {
                    $all_array[] = $attach->id;
                }
            }
            $array_dif = array();
            if (!empty($this->request->data['financial_attachments'])) {
                foreach ($this->request->data['financial_attachments'] as $attach) {
                    if (isset($attach['id']))
                        $array_dif[] = $attach['id'];
                }
            }
            
            //Get the total attachments vaules
            
            $array_to_delete = array_diff($all_array, $array_dif);
//            debug($array_to_delete);
            if (!empty($array_to_delete)) {
                if ($this->FinancialItems->FinancialAttachments->deleteAll(array('FinancialAttachments.id IN ' => $array_to_delete))) {
                    
                }
            }
//            debug($this->request->data);
//            exit;
            $financialItem = $this->FinancialItems->patchEntity($financialItem, $this->request->data, [
                'associated' => ['FinancialAttachments']
            ]);
//            debug($financialItem);
//            exit;
            if ($this->FinancialItems->save($financialItem)) {
                $this->Flash->success(__('The financial item has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            
            $this->Flash->error(__('The financial item could not be saved. Please, try again.'));
        }


        $this->_getTransaction($transaction_id);
        $crumbs['controller'] = ['Transactions' => ['controller' => 'transactions', 'action' => 'edit', $transaction_id]];
        $crumbs['action'] = 'FinancialItems';
        $this->updateButtons('FinancialItems', 'add', $transaction_id);
      
        $this->set(compact('financialItem', 'FinancialAttachments', 'crumbs'));
        $this->set('_serialize', ['financialItem']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Financial Item id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $financialItem = $this->FinancialItems->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $financialItem = $this->FinancialItems->patchEntity($financialItem, $this->request->data);
            if ($this->FinancialItems->save($financialItem)) {
                $this->Flash->success(__('The financial item has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The financial item could not be saved. Please, try again.'));
        }
        $this->set(compact('financialItems'));
        $this->set('_serialize', ['financialItems']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Financial Item id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $financialItem = $this->FinancialItems->get($id);
        if ($this->FinancialItems->delete($financialItem)) {
            $this->Flash->success(__('The financial item has been deleted.'));
        } else {
            $this->Flash->error(__('The financial item could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

}

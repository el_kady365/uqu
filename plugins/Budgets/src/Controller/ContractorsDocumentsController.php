<?php
namespace Budgets\Controller;

use Budgets\Controller\AppController;

/**
 * ContractorsDocuments Controller
 *
 * @property \Budgets\Model\Table\ContractorsDocumentsTable $ContractorsDocuments
 */
class ContractorsDocumentsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Contractors']
        ];
        $contractorsDocuments = $this->paginate($this->ContractorsDocuments);

        $this->set(compact('contractorsDocuments'));
        $this->set('_serialize', ['contractorsDocuments']);
    }

    /**
     * View method
     *
     * @param string|null $id Contractors Document id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $contractorsDocument = $this->ContractorsDocuments->get($id, [
            'contain' => ['Contractors']
        ]);

        $this->set('contractorsDocument', $contractorsDocument);
        $this->set('_serialize', ['contractorsDocument']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $contractorsDocument = $this->ContractorsDocuments->newEntity();
        if ($this->request->is('post')) {
            $contractorsDocument = $this->ContractorsDocuments->patchEntity($contractorsDocument, $this->request->data);
            if ($this->ContractorsDocuments->save($contractorsDocument)) {
                $this->Flash->success(__('The contractors document has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The contractors document could not be saved. Please, try again.'));
        }
        $contractors = $this->ContractorsDocuments->Contractors->find('list', ['limit' => 200]);
        $this->set(compact('contractorsDocument', 'contractors'));
        $this->set('_serialize', ['contractorsDocument']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Contractors Document id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $contractorsDocument = $this->ContractorsDocuments->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $contractorsDocument = $this->ContractorsDocuments->patchEntity($contractorsDocument, $this->request->data);
            if ($this->ContractorsDocuments->save($contractorsDocument)) {
                $this->Flash->success(__('The contractors document has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The contractors document could not be saved. Please, try again.'));
        }
        $contractors = $this->ContractorsDocuments->Contractors->find('list', ['limit' => 200]);
        $this->set(compact('contractorsDocument', 'contractors'));
        $this->set('_serialize', ['contractorsDocument']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Contractors Document id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $contractorsDocument = $this->ContractorsDocuments->get($id);
        if ($this->ContractorsDocuments->delete($contractorsDocument)) {
            $this->Flash->success(__('The contractors document has been deleted.'));
        } else {
            $this->Flash->error(__('The contractors document could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}

<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace Budgets\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use Budgets\Controller\AppController;
//use 

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

    /**
     * Displays a view
     *
     * @return void|\Cake\Network\Response
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function home() {
        $today = new \Cake\I18n\Time();
        $today = $today->format('Y-m-d H:i:s');
        $crumbs['controller'] = '';
        $crumbs['action'] = '';
        $this->loadModel('Transactions');
        /* $transactions=$this->loadModel('TransactionsTypes');
         $transactions=$this->paginate = [
            'contain' => ['TransactionsTypes.BudgetItems', 'Contractors', 'Purposes', 'TrustOrders', 'Contracts']
        ]; 
        */
        
         $transactionsType = TableRegistry::get('TransactionsTypes')->find()->all()->toArray();
        // $transactions=  $this->loadModel('Transactions');//TableRegistry::get('Transactions')->find()->all() ;
        // print_r($transactions);die();
         // $x = \Cake\ORM\TableRegistry::get('Transactions')->find()->all()->toArray();
        //$uses = array('Transactions');
         // $transactions= TableRegistry::get('TransactionsTypes')->find()->all() ;
       //  $transactions=$this->loadModel('Transactions');
         // $transactions=$this->loadModel('TransactionsTypes');
         //$transactions->find('all',['contain'=>['TransactionsTypes.BudgetItems', 'Contractors', 'Purposes', 'TrustOrders', 'Contracts']])->toArray();
       
  //  $transactions=$this->loadModel('Transactions');
    //$transactions->find('all',['contain'=>['TransactionsTypes.BudgetItems', 'Contractors', 'Purposes', 'TrustOrders', 'Contracts']])->toArray();
         // $transactionsType = TableRegistry::get('TransactionsTypes')->find()->all();
        $this->set('x', \Cake\ORM\TableRegistry::get('Transactions')->find()->all()->toArray());
        $this->set(compact('crumbs','x','transactionsType'));
        
        $this->pageTitle = __('Home');
    }
    
   
    
            function view($id) {
        $page = $this->Pages->get($id, [
            'contain' => []
        ]);

        $this->set('page', $page);
        $this->set('_serialize', ['page']);
        $this->pageTitle = $page->{$this->lang . '_title'};
    }

}

<?php

namespace Budgets\Controller;

use Budgets\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * FinancialYears Controller
 *
 * @property \Budgets\Model\Table\FinancialYearsTable $FinancialYears
 */
class FinancialYearsController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $financialYears = $this->paginate($this->FinancialYears);

        $this->set(compact('financialYears'));
        $this->set('_serialize', ['financialYears']);
    }

    /**
     * View method
     *
     * @param string|null $id Financial Year id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $budgetItems = TableRegistry::get('BudgetItems')->find()->all();
        $financialYear = $this->FinancialYears->get($id, [
            'contain' => ['FinancialYears', 'BudgetItems']
        ]);

        //$this->set('financialYear', $financialYear);
         $this->set(compact('financialYear', 'budgetItems'));
        $this->set('_serialize', ['financialYear']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add() {
        //$financialYear = $this->FinancialYears->newEntity();
        //$financialBudgetItems = $this->FinancialYears->BudgetItems->find()->where(['financial_year_id', $financialYear->id])->all();
        $financialYear = $this->FinancialYears->newEntity();
        $budgetItems = TableRegistry::get('BudgetItems')->find()->all();


        //$financialBudgetItems = $this->FinancialYears->BudgetItems->find()->where(['financial_year_id', $financialYear->id])->all();
//       
        if ($this->request->is('post')) {
            $financialYear = $this->FinancialYears->patchEntity($financialYear, $this->request->data, ['asscoiated' => 'BudgetItemsYears']);

            if ($this->FinancialYears->save($financialYear)) {

                $this->Flash->success(__('The financial year has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The financial year could not be saved. Please, try again.'));
        }
        $this->set(compact('financialYear', 'budgetItems'));
        $this->set('_serialize', ['financialYear']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Financial Year id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        //$financialYear = $this->FinancialYears->BudgetItems->find()->where(['financial_year_id', $financialYear->id])->all();
        $financialYear = $this->FinancialYears->get($id, [
            'contain' => ['BudgetItemsYears','BudgetItemsYears.BudgetItems']
        ]);
//        debug($financialYear);exit;
        $budgetItems = TableRegistry::get('BudgetItems')->find()->all();
        if ($this->request->is(['patch', 'post', 'put'])) {
            //$financialYear = $this->FinancialYears->BudgetItems->find()->where(['financial_year_id', $financialYear->id])->all();
//            $all_array = [];
//            if (!empty($financialYear->budget_items)) {
//                foreach ($financialYear->budget_items as $item) {
//                    $all_array[] = $item->id;
//                }
//            }
////            debug($all_array);
//            $array_dif = array();
////            debug($this->request->data);
//            if (!empty($this->request->data['budget_items'])) {
//                foreach ($this->request->data['budget_items'] as $item) {
//                    if (isset($item['id']))
//                        $array_dif[] = $item['id'];
//                }
//            }
////            debug($array_dif);
//            $array_to_delete = array_diff($all_array, $array_dif);
////debug($array_to_delete);exit;
//            if (!empty($array_to_delete)) {
//                if ($this->FinancialYears->BudgetItems->deleteAll(array('BudgetItems.id IN ' => $array_to_delete))) {
//                    
//                }
//            }
            // $financialYear = $this->FinancialYears->patchEntity($financialYear, $this->request->data, ['asscoiated' => 'BudgetItems']);
            //  $financialYear = $this->FinancialYears->get($id, [
            //    'contain' => ['BudgetItems']
            //]);
            $financialYear = $this->FinancialYears->patchEntity($financialYear, $this->request->data, [
                'associated' => ['BudgetItemsYears']
            ]);
            if ($this->FinancialYears->save($financialYear)) {
                $this->Flash->success(__('The financial year has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The financial year could not be saved. Please, try again.'));
        }
        $this->set(compact('financialYear','budgetItems'));
        $this->set('_serialize', ['financialYear']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Financial Year id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        // $this->request->allowMethod(['post', 'delete']);
        $financialYear = $this->FinancialYears->get($id);
        if ($this->FinancialYears->delete($financialYear)) {
            $this->Flash->success(__('The financial year has been deleted.'));
        } else {
            $this->Flash->error(__('The financial year could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

}

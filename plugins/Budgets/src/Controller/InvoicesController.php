<?php

namespace Budgets\Controller;

use Budgets\Controller\AppController;

/**
 * Invoices Controller
 *
 * @property \Budgets\Model\Table\InvoicesTable $Invoices
 */
class InvoicesController extends AppController {

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($transaction_id) {
        //$trustOrders =TrustOrders::get('TrustOrder');
        //$trustorder = $trustOrders->newEntity($this->request->getData());

        if (!$transaction_id) {
            $this->Flash->error(__('Invalid Transaction'));
            return $this->redirect(['controller' => 'transactions', 'action' => 'index']);
        }
        $transaction = $this->_getTransaction($transaction_id);
        $invoice = $this->Invoices->findOrCreate(['transaction_id' => $transaction_id]);
        $trans = \Cake\ORM\TableRegistry::get('TrustOrders')->get($transaction_id);
        
         

        if ($this->request->is(['patch', 'post', 'put'])) {
            $invoice = $this->Invoices->patchEntity($invoice, $this->request->data);
          
            if ($this->Invoices->save($invoice)) {
                $this->Flash->success(__('The invoice has been saved.'));
                return $this->redirect(['controller' => 'transactions', 'action' => 'edit', $transaction_id]);
            }
            $this->Flash->error(__('The invoice could not be saved. Please, try again.'));
        }
//        $this->_getTransaction($transaction_id);

        $crumbs['controller'] = ['Transactions' => ['controller' => 'transactions', 'action' => 'edit', $transaction_id]];
        $crumbs['action'] = 'Add Invoice';
        $this->updateButtons('Invoices', 'add', $transaction_id);
        $this->set('fileSettings', $this->Invoices->behaviors()->get('File')->getFileSettings());

        $this->set(compact('invoice', 'crumbs', 'trans','transaction'));
        $this->set('_serialize', ['invoice']);
    }

}

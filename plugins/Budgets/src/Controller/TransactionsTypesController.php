<?php

namespace Budgets\Controller;

use Budgets\Controller\AppController;

/**
 * TransactionsTypes Controller
 *
 * @property \Budgets\Model\Table\TransactionsTypesTable $TransactionsTypes
 */
class TransactionsTypesController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->paginate = [
            'contain' => ['BudgetItems']
        ];
        $transactionsTypes = $this->paginate($this->TransactionsTypes);
        $this->set(compact('transactionsTypes'));
        $this->set('_serialize', ['transactionsTypes']);
    }

    /**
     * View method
     *
     * @param string|null $id Transactions Type id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $transactionsType = $this->TransactionsTypes->get($id, [
            'contain' => ['BudgetItems']
        ]);

        $this->set('transactionsType', $transactionsType);
        $this->set('_serialize', ['transactionsType']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $transactionsType = $this->TransactionsTypes->newEntity();
        if ($this->request->is('post')) {
            $transactionsType = $this->TransactionsTypes->patchEntity($transactionsType, $this->request->data);
            if ($this->TransactionsTypes->save($transactionsType)) {
                $this->Flash->success(__('The transactions type has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The transactions type could not be saved. Please, try again.'));
            }
        }
        $budgetItems = $this->TransactionsTypes->BudgetItems->find('list', ['valueField' => 'no_title', 'limit' => 200]);
        $this->set(compact('transactionsType', 'budgetItems'));
        $this->set('_serialize', ['transactionsType']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Transactions Type id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $transactionsType = $this->TransactionsTypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $transactionsType = $this->TransactionsTypes->patchEntity($transactionsType, $this->request->data);
            if ($this->TransactionsTypes->save($transactionsType)) {
                $this->Flash->success(__('The transactions type has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The transactions type could not be saved. Please, try again.'));
            }
        }
        $budgetItems = $this->TransactionsTypes->BudgetItems->find('list', ['valueField' => 'no_title', 'limit' => 200]);
        $this->set(compact('transactionsType', 'budgetItems'));
        $this->set('_serialize', ['transactionsType']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Transactions Type id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        // $this->request->allowMethod(['post', 'delete']);
        $transactionsType = $this->TransactionsTypes->get($id);
        if ($this->TransactionsTypes->delete($transactionsType)) {
            $this->Flash->success(__('The transactions type has been deleted.'));
        } else {
            $this->Flash->error(__('The transactions type could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

}

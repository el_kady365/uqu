<?php

namespace Budgets\Controller;

use Budgets\Controller\AppController;

/**
 * FinancialCommitmentLetters Controller
 *
 * @property \Budgets\Model\Table\FinancialCommitmentLettersTable $FinancialCommitmentLetters
 */
class FinancialCommitmentLettersController extends AppController {
   
    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($transaction_id = false) {
        if (!$transaction_id) {
            $this->Flash->error(__('Invalid Transaction'));
            return $this->redirect(['controller' => 'transactions', 'action' => 'index']);
        }
        $financialCommitmentLetter = $this->FinancialCommitmentLetters->findOrCreate(['transaction_id' => $transaction_id]);
//        debug($this->request);exit;

        if ($this->request->is(['patch', 'post', 'put'])) {
//            debug($this->request->data);
//            exit;
            $financialCommitmentLetter = $this->FinancialCommitmentLetters->patchEntity($financialCommitmentLetter, $this->request->data);
            if ($this->FinancialCommitmentLetters->save($financialCommitmentLetter)) {
                $this->Flash->success(__('The financial commitment letter has been saved.'));
                return $this->redirect(['controller' => 'transactions', 'action' => 'edit', $transaction_id]);
            }
            $this->Flash->error(__('The financial commitment letter could not be saved. Please, try again.'));
        }
        $this->_getTransaction($transaction_id);
        $crumbs['controller'] = ['Transactions' => ['controller' => 'transactions', 'action' => 'edit', $transaction_id]];
        $crumbs['action'] = 'Financial Commitment Letter';
        $this->updateButtons('FinancialCommitmentLetters', 'add', $transaction_id);

        $this->set(compact('financialCommitmentLetter','crumbs'));
        $this->set('_serialize', ['financialCommitmentLetter']);
    }

        

}

<?php

namespace Budgets\Controller;

use Budgets\Controller\AppController;

/**
 * TrustOrders Controller
 *
 * @property \Budgets\Model\Table\TrustOrdersTable $TrustOrders
 */
class TrustOrdersController extends AppController {

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($transaction_id) {
        if (!$transaction_id) {
            $this->Flash->error(__('Invalid Transaction'));
            return $this->redirect(['controller' => 'transactions', 'action' => 'index']);
        }
        $trustOrder = $this->TrustOrders->findOrCreate(['transaction_id' => $transaction_id]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $trustOrder = $this->TrustOrders->patchEntity($trustOrder, $this->request->data);
            if ($this->TrustOrders->save($trustOrder)) {
                $this->Flash->success(__('The trust order has been saved.'));

                return $this->redirect(['controller' => 'transactions', 'action' => 'edit', $transaction_id]);
            }
            $this->Flash->error(__('The trust order could not be saved. Please, try again.'));
        }
        $this->_getTransaction($transaction_id);
        $crumbs['controller'] = ['Transactions' => ['controller' => 'transactions', 'action' => 'edit', $transaction_id]];
        $crumbs['action'] = 'Trust Order';
        $this->updateButtons('TrustOrders', 'add', $transaction_id);

        $this->set(compact('trustOrder', 'transactions', 'crumbs'));
        $this->set('_serialize', ['trustOrder']);
    }

}

<?php

namespace Budgets\Controller;

use Budgets\Controller\AppController;

/**
 * Contracts Controller
 *
 * @property \Budgets\Model\Table\ContractsTable $Contracts
 */
class ContractsController extends AppController {

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($transaction_id) {
        if (!$transaction_id) {
            $this->Flash->error(__('Invalid Transaction'));
            return $this->redirect(['controller' => 'transactions', 'action' => 'index']);
        }
        $contract = $this->Contracts->findOrCreate(['transaction_id' => $transaction_id]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $contract = $this->Contracts->patchEntity($contract, $this->request->data);
            if ($this->Contracts->save($contract)) {
                $this->Flash->success(__('The contract has been saved.'));
                return $this->redirect(['controller' => 'transactions', 'action' => 'edit', $transaction_id]);
            }
            $this->Flash->error(__('The contract could not be saved. Please, try again.'));
        }
        $this->_getTransaction($transaction_id);
        $crumbs['controller'] = ['Transactions' => ['controller' => 'transactions', 'action' => 'edit', $transaction_id]];
        $crumbs['action'] = 'Contract';
        $this->updateButtons('Contracts', 'add', $transaction_id);

        $this->set(compact('contract','crumbs'));
        $this->set('_serialize', ['contract']);
    }

}

<?php
namespace Budgets\Controller;

use Budgets\Controller\AppController;

/**
 * FinalFinancialYears Controller
 *
 * @property \Budgets\Model\Table\FinalFinancialYearsTable $FinalFinancialYears
 */
class FinalFinancialYearsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $finalFinancialYears = $this->paginate($this->FinalFinancialYears);

        $this->set(compact('finalFinancialYears'));
        $this->set('_serialize', ['finalFinancialYears']);
    }

    /**
     * View method
     *
     * @param string|null $id Final Financial Year id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $finalFinancialYear = $this->FinalFinancialYears->get($id, [
            'contain' => []
        ]);

        $this->set('finalFinancialYear', $finalFinancialYear);
        $this->set('_serialize', ['finalFinancialYear']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $finalFinancialYear = $this->FinalFinancialYears->newEntity();
        if ($this->request->is('post')) {
            $finalFinancialYear = $this->FinalFinancialYears->patchEntity($finalFinancialYear, $this->request->data);
            if ($this->FinalFinancialYears->save($finalFinancialYear)) {
                $this->Flash->success(__('The final financial year has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The final financial year could not be saved. Please, try again.'));
        }
        $this->set(compact('finalFinancialYear'));
        $this->set('_serialize', ['finalFinancialYear']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Final Financial Year id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $finalFinancialYear = $this->FinalFinancialYears->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $finalFinancialYear = $this->FinalFinancialYears->patchEntity($finalFinancialYear, $this->request->data);
            if ($this->FinalFinancialYears->save($finalFinancialYear)) {
                $this->Flash->success(__('The final financial year has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The final financial year could not be saved. Please, try again.'));
        }
        $this->set(compact('finalFinancialYear'));
        $this->set('_serialize', ['finalFinancialYear']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Final Financial Year id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $finalFinancialYear = $this->FinalFinancialYears->get($id);
        if ($this->FinalFinancialYears->delete($finalFinancialYear)) {
            $this->Flash->success(__('The final financial year has been deleted.'));
        } else {
            $this->Flash->error(__('The final financial year could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}

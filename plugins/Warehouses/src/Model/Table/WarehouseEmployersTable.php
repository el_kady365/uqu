<?php
namespace Warehouses\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * WarehouseEmployers Model
 *
 * @method \Warehouses\Model\Entity\WarehouseEmployer get($primaryKey, $options = [])
 * @method \Warehouses\Model\Entity\WarehouseEmployer newEntity($data = null, array $options = [])
 * @method \Warehouses\Model\Entity\WarehouseEmployer[] newEntities(array $data, array $options = [])
 * @method \Warehouses\Model\Entity\WarehouseEmployer|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Warehouses\Model\Entity\WarehouseEmployer patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Warehouses\Model\Entity\WarehouseEmployer[] patchEntities($entities, array $data, array $options = [])
 * @method \Warehouses\Model\Entity\WarehouseEmployer findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class WarehouseEmployersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('warehouse_employers');
        $this->displayField('emp_name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('emp_number', 'create')
            ->notEmpty('emp_number');

        $validator
            ->requirePresence('emp_name', 'create')
            ->notEmpty('emp_name');

            $validator->requirePresence('emp_site', 'create')
            ->notEmpty('emp_site');

        return $validator;
    }
}

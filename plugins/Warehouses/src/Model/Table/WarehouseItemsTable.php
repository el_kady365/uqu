<?php
namespace Warehouses\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * WarehouseItems Model
 *
 * @method \Warehouses\Model\Entity\WarehouseItem get($primaryKey, $options = [])
 * @method \Warehouses\Model\Entity\WarehouseItem newEntity($data = null, array $options = [])
 * @method \Warehouses\Model\Entity\WarehouseItem[] newEntities(array $data, array $options = [])
 * @method \Warehouses\Model\Entity\WarehouseItem|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Warehouses\Model\Entity\WarehouseItem patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Warehouses\Model\Entity\WarehouseItem[] patchEntities($entities, array $data, array $options = [])
 * @method \Warehouses\Model\Entity\WarehouseItem findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class WarehouseItemsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('warehouse_items');
        $this->displayField('item_name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('warehouse_item_number', 'create')
            ->notEmpty('warehouse_item_number');

        $validator
            ->requirePresence('warehouse_item_name', 'create')
            ->notEmpty('warehouse_item_name');

        $validator
            ->requirePresence('warehouse_category_item_name', 'create')
            ->notEmpty('warehouse_category_item_name');

        return $validator;
    }
}

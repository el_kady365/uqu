<?php
namespace Warehouses\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * WarehouseManagements Model
 *
 * @method \Warehouses\Model\Entity\WarehouseManagement get($primaryKey, $options = [])
 * @method \Warehouses\Model\Entity\WarehouseManagement newEntity($data = null, array $options = [])
 * @method \Warehouses\Model\Entity\WarehouseManagement[] newEntities(array $data, array $options = [])
 * @method \Warehouses\Model\Entity\WarehouseManagement|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Warehouses\Model\Entity\WarehouseManagement patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Warehouses\Model\Entity\WarehouseManagement[] patchEntities($entities, array $data, array $options = [])
 * @method \Warehouses\Model\Entity\WarehouseManagement findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class WarehouseItemsInfoTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('warehouse_items_info');
        $this->displayField('info_item_name');
        
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('info_item_name', 'create')
            ->notEmpty('info_item_name');

        $validator
            ->requirePresence('info_owner_number', 'create')
            ->notEmpty('info_owner_number');
        
         $validator
            ->requirePresence('info_serial_number', 'create')
            ->notEmpty('info_serial_number');
         
          $validator
            ->requirePresence('contract_number', 'create')
            ->notEmpty('contract_number');
          
          $validator
            ->requirePresence('start_date_guarantee', 'create')
            ->notEmpty('start_date_guarantee');
          
          $validator
            ->requirePresence('end_date_guarantee', 'create')
            ->notEmpty('end_date_guarantee');
          
          $validator
            ->requirePresence('supplier_name', 'create')
            ->notEmpty('supplier_name');
          
          $validator
            ->requirePresence('state', 'create')
            ->notEmpty('state');
          
           $validator
            ->requirePresence('Employer_number', 'create')
            ->notEmpty('Employer_number');

           
            $validator
            ->requirePresence('notes', 'create')
            ->notEmpty('notes');

            
          

        return $validator;
    }
}

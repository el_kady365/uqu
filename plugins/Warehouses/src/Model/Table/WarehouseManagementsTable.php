<?php
namespace Warehouses\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * WarehouseManagements Model
 *
 * @method \Warehouses\Model\Entity\WarehouseManagement get($primaryKey, $options = [])
 * @method \Warehouses\Model\Entity\WarehouseManagement newEntity($data = null, array $options = [])
 * @method \Warehouses\Model\Entity\WarehouseManagement[] newEntities(array $data, array $options = [])
 * @method \Warehouses\Model\Entity\WarehouseManagement|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Warehouses\Model\Entity\WarehouseManagement patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Warehouses\Model\Entity\WarehouseManagement[] patchEntities($entities, array $data, array $options = [])
 * @method \Warehouses\Model\Entity\WarehouseManagement findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class WarehouseManagementsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('warehouse_managements');
        $this->displayField('title');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('management_number', 'create')
            ->notEmpty('management_number');

        $validator
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        return $validator;
    }
}

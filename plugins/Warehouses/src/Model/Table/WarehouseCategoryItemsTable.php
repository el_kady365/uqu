<?php
namespace Warehouses\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * WarehouseCategoryItems Model
 *
 * @method \Warehouses\Model\Entity\WarehouseCategoryItem get($primaryKey, $options = [])
 * @method \Warehouses\Model\Entity\WarehouseCategoryItem newEntity($data = null, array $options = [])
 * @method \Warehouses\Model\Entity\WarehouseCategoryItem[] newEntities(array $data, array $options = [])
 * @method \Warehouses\Model\Entity\WarehouseCategoryItem|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Warehouses\Model\Entity\WarehouseCategoryItem patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Warehouses\Model\Entity\WarehouseCategoryItem[] patchEntities($entities, array $data, array $options = [])
 * @method \Warehouses\Model\Entity\WarehouseCategoryItem findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class WarehouseCategoryItemsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('category_item_number', 'create')
            ->notEmpty('category_item_number');

        $validator
            ->requirePresence('category_item_name', 'create')
            ->notEmpty('category_item_name');

        return $validator;
    }
}

<?php
namespace Warehouses\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * WarehouseSites Model
 *
 * @method \Warehouses\Model\Entity\WarehouseSite get($primaryKey, $options = [])
 * @method \Warehouses\Model\Entity\WarehouseSite newEntity($data = null, array $options = [])
 * @method \Warehouses\Model\Entity\WarehouseSite[] newEntities(array $data, array $options = [])
 * @method \Warehouses\Model\Entity\WarehouseSite|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Warehouses\Model\Entity\WarehouseSite patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Warehouses\Model\Entity\WarehouseSite[] patchEntities($entities, array $data, array $options = [])
 * @method \Warehouses\Model\Entity\WarehouseSite findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class WarehouseSitesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('warehouse_sites');
        $this->displayField('site_name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('site_number', 'create')
            ->notEmpty('site_number');

        $validator
            ->requirePresence('site_name', 'create')
            ->notEmpty('site_name');

        return $validator;
    }
}

<?php
namespace Warehouses\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * WarehouseStores Model
 *
 * @method \Warehouses\Model\Entity\WarehouseStore get($primaryKey, $options = [])
 * @method \Warehouses\Model\Entity\WarehouseStore newEntity($data = null, array $options = [])
 * @method \Warehouses\Model\Entity\WarehouseStore[] newEntities(array $data, array $options = [])
 * @method \Warehouses\Model\Entity\WarehouseStore|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Warehouses\Model\Entity\WarehouseStore patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Warehouses\Model\Entity\WarehouseStore[] patchEntities($entities, array $data, array $options = [])
 * @method \Warehouses\Model\Entity\WarehouseStore findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class WarehouseStoresTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->displayField('store_title');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('store_number', 'create')
            ->notEmpty('store_number');

        $validator
            ->requirePresence('store_title', 'create')
            ->notEmpty('store_title');

        return $validator;
    }
}
        $this->table('warehouse_stores');

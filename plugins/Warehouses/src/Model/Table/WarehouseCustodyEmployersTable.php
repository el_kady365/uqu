<?php
namespace Warehouses\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * WarehouseCustodyEmployers Model
 *
 * @method \Warehouses\Model\Entity\WarehouseCustodyEmployer get($primaryKey, $options = [])
 * @method \Warehouses\Model\Entity\WarehouseCustodyEmployer newEntity($data = null, array $options = [])
 * @method \Warehouses\Model\Entity\WarehouseCustodyEmployer[] newEntities(array $data, array $options = [])
 * @method \Warehouses\Model\Entity\WarehouseCustodyEmployer|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Warehouses\Model\Entity\WarehouseCustodyEmployer patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Warehouses\Model\Entity\WarehouseCustodyEmployer[] patchEntities($entities, array $data, array $options = [])
 * @method \Warehouses\Model\Entity\WarehouseCustodyEmployer findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class WarehouseCustodyEmployersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('warehouse_custody_employers');
        $this->displayField('custody_item_name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('custody_item_name', 'create')
            ->notEmpty('custody_item_name');

        $validator
            ->requirePresence('custody_number', 'create')
            ->notEmpty('custody_number');

        $validator
            ->requirePresence('custody_serial', 'create')
            ->notEmpty('custody_serial');

        $validator
            ->requirePresence('custody_state', 'create')
            ->notEmpty('custody_state');

        $validator
            ->requirePresence('custody_notes', 'create')
            ->notEmpty('custody_notes');

        return $validator;
    }
}

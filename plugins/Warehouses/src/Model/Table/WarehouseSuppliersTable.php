<?php
namespace Warehouses\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * WarehouseSuppliers Model
 *
 * @method \Warehouses\Model\Entity\WarehouseSupplier get($primaryKey, $options = [])
 * @method \Warehouses\Model\Entity\WarehouseSupplier newEntity($data = null, array $options = [])
 * @method \Warehouses\Model\Entity\WarehouseSupplier[] newEntities(array $data, array $options = [])
 * @method \Warehouses\Model\Entity\WarehouseSupplier|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Warehouses\Model\Entity\WarehouseSupplier patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Warehouses\Model\Entity\WarehouseSupplier[] patchEntities($entities, array $data, array $options = [])
 * @method \Warehouses\Model\Entity\WarehouseSupplier findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class WarehouseSuppliersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('warehouse_suppliers');
        $this->displayField('supplier_name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('supplier_number', 'create')
            ->notEmpty('supplier_number');

        $validator
            ->requirePresence('supplier_name', 'create')
            ->notEmpty('supplier_name');

        return $validator;
    }
}

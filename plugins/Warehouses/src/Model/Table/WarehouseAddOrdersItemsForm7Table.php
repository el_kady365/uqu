<?php
namespace Warehouses\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * WarehouseManagements Model
 *
 * @method \Warehouses\Model\Entity\WarehouseManagement get($primaryKey, $options = [])
 * @method \Warehouses\Model\Entity\WarehouseManagement newEntity($data = null, array $options = [])
 * @method \Warehouses\Model\Entity\WarehouseManagement[] newEntities(array $data, array $options = [])
 * @method \Warehouses\Model\Entity\WarehouseManagement|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Warehouses\Model\Entity\WarehouseManagement patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Warehouses\Model\Entity\WarehouseManagement[] patchEntities($entities, array $data, array $options = [])
 * @method \Warehouses\Model\Entity\WarehouseManagement findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class WarehouseAddOrdersItemsForm7Table extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('add_orders_items_form_7');
        $this->displayField('order_item_name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('order_item_number', 'create')
            ->notEmpty('order_item_number');

        $validator
            ->requirePresence('order_item_name', 'create')
            ->notEmpty('order_item_name');
        
         $validator
            ->requirePresence('order_item_type', 'create')
            ->notEmpty('order_item_type');
         
         $validator
            ->requirePresence('order_item_category', 'create')
            ->notEmpty('order_item_category');
         
         $validator
            ->requirePresence('order_item_count', 'create')
            ->notEmpty('order_item_count');
         
         $validator
            ->requirePresence('order_item_amount_recieved', 'create')
            ->notEmpty('order_item_amount_recieved');
         
         $validator
            ->requirePresence('order_item_price', 'create')
            ->notEmpty('order_item_price');
         
          $validator
            ->requirePresence('order_total_price', 'create')
            ->notEmpty('order_total_price');
          
           $validator
            ->requirePresence('notes', 'create')
            ->notEmpty('notes');

        return $validator;
    }
}

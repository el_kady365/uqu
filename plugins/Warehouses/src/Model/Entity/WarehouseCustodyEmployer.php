<?php
namespace Warehouses\Model\Entity;

use Cake\ORM\Entity;

/**
 * WarehouseCustodyEmployer Entity
 *
 * @property int $id
 * @property string $custody_item_name
 * @property string $custody_number
 * @property string $custody_serial
 * @property string $custody_state
 * @property string $custody_notes
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $updated
 */
class WarehouseCustodyEmployer extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}

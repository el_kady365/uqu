<?php
namespace Warehouses\Model\Entity;

use Cake\ORM\Entity;

/**
 * WarehouseStore Entity
 *
 * @property int $id
 * @property string $store_number
 * @property string $store_title
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $updated
 */
class WarehouseStore extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}

<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Warehouse Site'), ['action' => 'edit', $warehouseSite->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Warehouse Site'), ['action' => 'delete', $warehouseSite->id], ['confirm' => __('Are you sure you want to delete # {0}?', $warehouseSite->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Warehouse Sites'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Warehouse Site'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="warehouseSites view large-9 medium-8 columns content">
    <h3><?= h($warehouseSite->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Site Number') ?></th>
            <td><?= h($warehouseSite->site_number) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Site Name') ?></th>
            <td><?= h($warehouseSite->site_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($warehouseSite->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($warehouseSite->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Updated') ?></th>
            <td><?= h($warehouseSite->updated) ?></td>
        </tr>
    </table>
</div>

<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Warehouse Employer'), ['action' => 'edit', $warehouseEmployer->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Warehouse Employer'), ['action' => 'delete', $warehouseEmployer->id], ['confirm' => __('Are you sure you want to delete # {0}?', $warehouseEmployer->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Warehouse Employers'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Warehouse Employer'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="warehouseEmployers view large-9 medium-8 columns content">
    <h3><?= h($warehouseEmployer->emp_name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Emp Number') ?></th>
            <td><?= h($warehouseEmployer->emp_number) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Emp Name') ?></th>
            <td><?= h($warehouseEmployer->emp_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Emp Code') ?></th>
            <td><?= h($warehouseEmployer->emp_code) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Emp Management') ?></th>
            <td><?= h($warehouseEmployer->emp_management) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Emp Site') ?></th>
            <td><?= h($warehouseEmployer->emp_site) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($warehouseEmployer->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($warehouseEmployer->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Updated') ?></th>
            <td><?= h($warehouseEmployer->updated) ?></td>
        </tr>
    </table>
</div>

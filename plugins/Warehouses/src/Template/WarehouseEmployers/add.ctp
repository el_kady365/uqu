<?php
/**
  * @var \App\View\AppView $this
  */
?>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="text-danger">
                <?php if (!$warehouseEmployer->id) {
                echo __('Add Warehouse Employer');
                } else {
                echo __('Edit Warehouse Employer');
                }
                ?></h3>
            <hr />
        </div>
        <div class="panel-body">
            <?= $this->Form->create($warehouseEmployer) ?>

            <?php
                        echo $this->Form->input('emp_number', ['placeholder'=>__('Emp Number'),'class' => 'form-control']);
                                    echo $this->Form->input('emp_name', ['placeholder'=>__('Emp Name'),'class' => 'form-control']);
                                    echo $this->Form->input('emp_code', ['placeholder'=>__('Emp Code'),'class' => 'form-control']);
                                    echo $this->Form->input('emp_management', ['placeholder'=>__('Emp Management'),'class' => 'form-control']);
                                    echo $this->Form->input('emp_site', ['placeholder'=>__('Emp Site'),'class' => 'form-control']);
                                    ?>

            <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-success btn-lg', 'templateVars' => ['icon' => '<i class="icon-check"></i>']]) ?>
            <?= $this->Form->button(__('Cancel'), ['class' => 'btn btn-warning btn-lg', 'id' => 'CancelBtn', 'templateVars' => ['icon' => '<i class="icon-cancel"></i>']]) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

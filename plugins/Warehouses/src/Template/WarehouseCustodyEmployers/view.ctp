<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Warehouse Custody Employer'), ['action' => 'edit', $warehouseCustodyEmployer->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Warehouse Custody Employer'), ['action' => 'delete', $warehouseCustodyEmployer->id], ['confirm' => __('Are you sure you want to delete # {0}?', $warehouseCustodyEmployer->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Warehouse Custody Employers'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Warehouse Custody Employer'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="warehouseCustodyEmployers view large-9 medium-8 columns content">
    <h3><?= h($warehouseCustodyEmployer->custody_item_name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Custody Item Name') ?></th>
            <td><?= h($warehouseCustodyEmployer->custody_item_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Custody Number') ?></th>
            <td><?= h($warehouseCustodyEmployer->custody_number) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Custody Serial') ?></th>
            <td><?= h($warehouseCustodyEmployer->custody_serial) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Custody State') ?></th>
            <td><?= h($warehouseCustodyEmployer->custody_state) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($warehouseCustodyEmployer->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($warehouseCustodyEmployer->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Updated') ?></th>
            <td><?= h($warehouseCustodyEmployer->updated) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Custody Notes') ?></h4>
        <?= $this->Text->autoParagraph(h($warehouseCustodyEmployer->custody_notes)); ?>
    </div>
</div>

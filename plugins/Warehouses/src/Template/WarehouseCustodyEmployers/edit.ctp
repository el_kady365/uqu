<?php
/**
  * @var \App\View\AppView $this
  */
?>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="text-danger">
                <?php if (!$warehouseCustodyEmployer->id) {
                echo __('Add Warehouse Custody Employer');
                } else {
                echo __('Edit Warehouse Custody Employer');
                }
                ?></h3>
            <hr />
        </div>
        <div class="panel-body">
            <?= $this->Form->create($warehouseCustodyEmployer) ?>

            <?php
                        echo $this->Form->input('custody_item_name', ['placeholder'=>__('Custody Item Name'),'class' => 'form-control']);
                                    echo $this->Form->input('custody_number', ['placeholder'=>__('Custody Number'),'class' => 'form-control']);
                                    echo $this->Form->input('custody_serial', ['placeholder'=>__('Custody Serial'),'class' => 'form-control']);
                                    echo $this->Form->input('custody_state', ['placeholder'=>__('Custody State'),'class' => 'form-control']);
                                    echo $this->Form->input('custody_notes', ['placeholder'=>__('Custody Notes'),'class' => 'form-control']);
                                    ?>

            <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-success btn-lg', 'templateVars' => ['icon' => '<i class="icon-check"></i>']]) ?>
            <?= $this->Form->button(__('Cancel'), ['class' => 'btn btn-warning btn-lg', 'id' => 'CancelBtn', 'templateVars' => ['icon' => '<i class="icon-cancel"></i>']]) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

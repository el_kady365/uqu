
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="text-danger"><?= __('Warehouse Add Orders Items Form7') ?></h3>
            <hr />
        </div>
        <div class="panel-body">
            <form action="<?php echo $this->Url->build(array("action" => "do-operation")) ?>" method="post">
                <!--Table Wrapper Start-->
                <div class="table-responsive ls-table">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                                                <th><?= $this->Paginator->sort('id') ?></th>
                                                                <th><?= $this->Paginator->sort('order_item_number') ?></th>
                                                                <th><?= $this->Paginator->sort('order_item_name') ?></th>
                                                                <th><?= $this->Paginator->sort('order_item_type') ?></th>
                                                                <th><?= $this->Paginator->sort('order_item_category') ?></th>
                                                                <th><?= $this->Paginator->sort('order_item_count') ?></th>
                                                                <th><?= $this->Paginator->sort('order_item_amount_recieved') ?></th>
                                                                <th><?= $this->Paginator->sort('order_item_price') ?></th>
                                                                <th><?= $this->Paginator->sort('order_total_price') ?></th>
                                                                <th><?= $this->Paginator->sort('created') ?></th>
                                                                <th><?= $this->Paginator->sort('updated') ?></th>
                                                                <th class="actions"><?= __('Actions') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($warehouseAddOrdersItemsForm7 as $warehouseAddOrdersItemsForm7): ?>
                            <tr>
                                                                <td><?= $this->Number->format($warehouseAddOrdersItemsForm7->id) ?></td>
                                                                            <td><?= h($warehouseAddOrdersItemsForm7->order_item_number) ?></td>
                                                                            <td><?= h($warehouseAddOrdersItemsForm7->order_item_name) ?></td>
                                                                            <td><?= h($warehouseAddOrdersItemsForm7->order_item_type) ?></td>
                                                                            <td><?= h($warehouseAddOrdersItemsForm7->order_item_category) ?></td>
                                                                            <td><?= $this->Number->format($warehouseAddOrdersItemsForm7->order_item_count) ?></td>
                                                                            <td><?= $this->Number->format($warehouseAddOrdersItemsForm7->order_item_amount_recieved) ?></td>
                                                                            <td><?= h($warehouseAddOrdersItemsForm7->order_item_price) ?></td>
                                                                            <td><?= h($warehouseAddOrdersItemsForm7->order_total_price) ?></td>
                                                                            <td><?= h($warehouseAddOrdersItemsForm7->created) ?></td>
                                                                            <td><?= h($warehouseAddOrdersItemsForm7->updated) ?></td>
                                                                            <td class="actions">
                                    <?= $this->Html->link('<i class="icon-tune"></i>', ['action' => 'edit', $warehouseAddOrdersItemsForm7->id], ['class' => 'btn btn-success icon-btn btn-rounded', 'escape' => false]) ?>
                                    <?= $this->Html->link('<i class="icon-cancel2"></i>', ['action' => 'delete', $warehouseAddOrdersItemsForm7->id], ['class' => 'btn btn-danger icon-btn btn-rounded', 'escape' => false, 'confirm' => __('Are you sure you want to delete # {0}?', $warehouseAddOrdersItemsForm7->id)]) ?>


                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>

                </div>
                <div class="row">
                    <div class="col-md-3">
                    </div>
                    <div class="col-md-9 text-left">
                        <ul class="pagination ls-pagination">
                            <?php
                            if ($this->Paginator->hasPrev()) {
                                echo $this->Paginator->prev('< ' . __('previous'));
                            }
                            echo $this->Paginator->numbers();
                            if ($this->Paginator->hasNext()) {
                                echo $this->Paginator->next(__('next') . ' >');
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Warehouse Add Orders Items Form7'), ['action' => 'edit', $warehouseAddOrdersItemsForm7->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Warehouse Add Orders Items Form7'), ['action' => 'delete', $warehouseAddOrdersItemsForm7->id], ['confirm' => __('Are you sure you want to delete # {0}?', $warehouseAddOrdersItemsForm7->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Warehouse Add Orders Items Form7'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Warehouse Add Orders Items Form7'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="warehouseAddOrdersItemsForm7 view large-9 medium-8 columns content">
    <h3><?= h($warehouseAddOrdersItemsForm7->order_item_name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Order Item Number') ?></th>
            <td><?= h($warehouseAddOrdersItemsForm7->order_item_number) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Order Item Name') ?></th>
            <td><?= h($warehouseAddOrdersItemsForm7->order_item_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Order Item Type') ?></th>
            <td><?= h($warehouseAddOrdersItemsForm7->order_item_type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Order Item Category') ?></th>
            <td><?= h($warehouseAddOrdersItemsForm7->order_item_category) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Order Item Price') ?></th>
            <td><?= h($warehouseAddOrdersItemsForm7->order_item_price) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Order Total Price') ?></th>
            <td><?= h($warehouseAddOrdersItemsForm7->order_total_price) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($warehouseAddOrdersItemsForm7->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Order Item Count') ?></th>
            <td><?= $this->Number->format($warehouseAddOrdersItemsForm7->order_item_count) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Order Item Amount Recieved') ?></th>
            <td><?= $this->Number->format($warehouseAddOrdersItemsForm7->order_item_amount_recieved) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($warehouseAddOrdersItemsForm7->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Updated') ?></th>
            <td><?= h($warehouseAddOrdersItemsForm7->updated) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Notes') ?></h4>
        <?= $this->Text->autoParagraph(h($warehouseAddOrdersItemsForm7->notes)); ?>
    </div>
</div>

<?php
/**
  * @var \App\View\AppView $this
  */
?>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="text-danger">
                <?php if (!$warehouseAddOrdersItemsForm7->id) {
                echo __('Add Warehouse Add Orders Items Form7');
                } else {
                echo __('Edit Warehouse Add Orders Items Form7');
                }
                ?></h3>
            <hr />
        </div>
        <div class="panel-body">
            <?= $this->Form->create($warehouseAddOrdersItemsForm7) ?>

            <?php
                        echo $this->Form->input('order_item_number', ['placeholder'=>__('Order Item Number'),'class' => 'form-control']);
                                    echo $this->Form->input('order_item_name', ['placeholder'=>__('Order Item Name'),'class' => 'form-control']);
                                    echo $this->Form->input('order_item_type', ['placeholder'=>__('Order Item Type'),'class' => 'form-control']);
                                    echo $this->Form->input('order_item_category', ['placeholder'=>__('Order Item Category'),'class' => 'form-control']);
                                    echo $this->Form->input('order_item_count', ['placeholder'=>__('Order Item Count'),'class' => 'form-control']);
                                    echo $this->Form->input('order_item_amount_recieved', ['placeholder'=>__('Order Item Amount Recieved'),'class' => 'form-control']);
                                    echo $this->Form->input('order_item_price', ['placeholder'=>__('Order Item Price'),'class' => 'form-control']);
                                    echo $this->Form->input('order_total_price', ['placeholder'=>__('Order Total Price'),'class' => 'form-control']);
                                    echo $this->Form->input('notes', ['placeholder'=>__('Notes'),'class' => 'form-control']);
                                    ?>

            <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-success btn-lg', 'templateVars' => ['icon' => '<i class="icon-check"></i>']]) ?>
            <?= $this->Form->button(__('Cancel'), ['class' => 'btn btn-warning btn-lg', 'id' => 'CancelBtn', 'templateVars' => ['icon' => '<i class="icon-cancel"></i>']]) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Warehouse Categories Item'), ['action' => 'edit', $warehouseCategoriesItem->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Warehouse Categories Item'), ['action' => 'delete', $warehouseCategoriesItem->id], ['confirm' => __('Are you sure you want to delete # {0}?', $warehouseCategoriesItem->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Warehouse Categories Items'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Warehouse Categories Item'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="warehouseCategoriesItems view large-9 medium-8 columns content">
    <h3><?= h($warehouseCategoriesItem->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Category Item Name') ?></th>
            <td><?= h($warehouseCategoriesItem->category_item_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($warehouseCategoriesItem->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Category Item Number') ?></th>
            <td><?= $this->Number->format($warehouseCategoriesItem->category_item_number) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($warehouseCategoriesItem->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Updated') ?></th>
            <td><?= h($warehouseCategoriesItem->updated) ?></td>
        </tr>
    </table>
</div>

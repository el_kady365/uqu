<?php
/**
  * @var \App\View\AppView $this
  */
?>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="text-danger">
                <?php if (!$warehouseCategoriesItem->id) {
                echo __('Add Warehouse Categories Item');
                } else {
                echo __('Edit Warehouse Categories Item');
                }
                ?></h3>
            <hr />
        </div>
        <div class="panel-body">
            <?= $this->Form->create($warehouseCategoriesItem) ?>

            <?php
                        echo $this->Form->input('category_item_number', ['placeholder'=>__('Category Item Number'),'class' => 'form-control']);
                                    echo $this->Form->input('category_item_name', ['placeholder'=>__('Category Item Name'),'class' => 'form-control']);
                                    ?>

            <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-success btn-lg', 'templateVars' => ['icon' => '<i class="icon-check"></i>']]) ?>
            <?= $this->Form->button(__('Cancel'), ['class' => 'btn btn-warning btn-lg', 'id' => 'CancelBtn', 'templateVars' => ['icon' => '<i class="icon-cancel"></i>']]) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

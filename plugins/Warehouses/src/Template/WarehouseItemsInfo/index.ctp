
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="text-danger"><?= __('Warehouse Items Info') ?></h3>
            <hr />
        </div>
        <div class="panel-body">
            <form action="<?php echo $this->Url->build(array("action" => "do-operation")) ?>" method="post">
                <!--Table Wrapper Start-->
                <div class="table-responsive ls-table">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                                                <th><?= $this->Paginator->sort('id') ?></th>
                                                                <th><?= $this->Paginator->sort('info_item_name') ?></th>
                                                                <th><?= $this->Paginator->sort('info_owner_number') ?></th>
                                                                <th><?= $this->Paginator->sort('info_serial_number') ?></th>
                                                                <th><?= $this->Paginator->sort('contract_number') ?></th>
                                                                <th><?= $this->Paginator->sort('start_date_guarantee') ?></th>
                                                                <th><?= $this->Paginator->sort('end_date_guarantee') ?></th>
                                                                <th><?= $this->Paginator->sort('supplier_name') ?></th>
                                                                <th><?= $this->Paginator->sort('state') ?></th>
                                                                <th><?= $this->Paginator->sort('Employer_number') ?></th>
                                                                <th><?= $this->Paginator->sort('notes') ?></th>
                                                                <th><?= $this->Paginator->sort('created') ?></th>
                                                                <th><?= $this->Paginator->sort('updated') ?></th>
                                                                <th class="actions"><?= __('Actions') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($warehouseItemsInfo as $warehouseItemsInfo): ?>
                            <tr>
                                                                <td><?= $this->Number->format($warehouseItemsInfo->id) ?></td>
                                                                          
                                                                            <td><?= h($warehouseItemsInfo->info_item_name) ?></td>
                                                                            <td><?= h($warehouseItemsInfo->info_owner_number) ?></td>
                                                                            <td><?= h($warehouseItemsInfo->info_serial_number) ?></td>
                                                                            <td><?= h($warehouseItemsInfo->contract_number) ?></td>
                                                                            <td><?= h($warehouseItemsInfo->start_date_guarantee) ?></td>
                                                                            <td><?= h($warehouseItemsInfo->end_date_guarantee) ?></td>
                                                                            <td><?= h($warehouseItemsInfo->supplier_name) ?></td>
                                                                            <td><?= h($warehouseItemsInfo->state) ?></td>
                                                                            <td><?= h($warehouseItemsInfo->Employer_number) ?></td>
                                                                            <td><?= h($warehouseItemsInfo->notes) ?></td>
                                                                            <td><?= h($warehouseItemsInfo->created) ?></td>
                                                                            <td><?= h($warehouseItemsInfo->updated) ?></td>
                                                                            <td class="actions">
                                    <?= $this->Html->link('<i class="icon-tune"></i>', ['action' => 'edit', $warehouseItemsInfo->id], ['class' => 'btn btn-success icon-btn btn-rounded', 'escape' => false]) ?>
                                    <?= $this->Html->link('<i class="icon-cancel2"></i>', ['action' => 'delete', $warehouseItemsInfo->id], ['class' => 'btn btn-danger icon-btn btn-rounded', 'escape' => false, 'confirm' => __('Are you sure you want to delete # {0}?', $warehouseItemsInfo->id)]) ?>


                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>

                </div>
                <div class="row">
                    <div class="col-md-3">
                    </div>
                    <div class="col-md-9 text-left">
                        <ul class="pagination ls-pagination">
                            <?php
                            if ($this->Paginator->hasPrev()) {
                                echo $this->Paginator->prev('< ' . __('previous'));
                            }
                            echo $this->Paginator->numbers();
                            if ($this->Paginator->hasNext()) {
                                echo $this->Paginator->next(__('next') . ' >');
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

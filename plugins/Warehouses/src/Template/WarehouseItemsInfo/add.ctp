<?php
/**
  * @var \App\View\AppView $this
  */
?>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="text-danger">
                <?php if (!$warehouseItemsInfo->id) {
                echo __('Add Warehouse Items Info');
                } else {
                echo __('Edit Warehouse Items Info');
                }
                ?></h3>
            <hr />
        </div>
        <div class="panel-body">
            <?= $this->Form->create($warehouseItemsInfo) ?>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <?php
                            echo $this->Form->input('info_item_name', ['placeholder'=>__('Info Item Name'),'class' => 'form-control']);

                        ?>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <?php
                             echo $this->Form->input('contract_number', ['placeholder'=>__('Contract Number'),'class' => 'form-control']);
                        ?>

                    </div>
                </div>
            </div>
                <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                         <?php
                            echo $this->Form->input('info_owner_number', ['placeholder'=>__('Info Owner Number'),'class' => 'form-control']);
                        ?>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <?php
                               echo $this->Form->input('start_date_guarantee', ['placeholder'=>__('Start Date Guarantee'),'class' => 'form-control']);
                        ?>

                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                         <?php
                            echo $this->Form->input('info_serial_number', ['placeholder'=>__('Info Serial Number'),'class' => 'form-control']);
                        ?>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <?php
                               echo $this->Form->input('end_date_guarantee', ['placeholder'=>__('End Date Guarantee'),'class' => 'form-control']);
                        ?>

                    </div>
                </div>
            </div>
                        
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                         <?php
                            echo $this->Form->input('Employer_number', ['placeholder'=>__('Employer Number'),'class' => 'form-control']);
                        ?>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <?php
                               echo $this->Form->input('supplier_name', ['placeholder'=>__('Supplier Name'),'class' => 'form-control']);
                        ?>

                    </div>
                </div>
            </div>
                        
                         
            <?php
                        echo $this->Form->input('state', ['placeholder'=>__('State'),'class' => 'form-control']);
                        echo $this->Form->input('notes', ['placeholder'=>__('Notes'),'class' => 'form-control']);
                                    ?>
             
            <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-success btn-lg', 'templateVars' => ['icon' => '<i class="icon-check"></i>']]) ?>
            <?= $this->Form->button(__('Cancel'), ['class' => 'btn btn-warning btn-lg', 'id' => 'CancelBtn', 'templateVars' => ['icon' => '<i class="icon-cancel"></i>']]) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Warehouse Items Info'), ['action' => 'edit', $warehouseItemsInfo->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Warehouse Items Info'), ['action' => 'delete', $warehouseItemsInfo->id], ['confirm' => __('Are you sure you want to delete # {0}?', $warehouseItemsInfo->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Warehouse Items Info'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Warehouse Items Info'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="warehouseItemsInfo view large-9 medium-8 columns content">
    <h3><?= h($warehouseItemsInfo->info_item_name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Info Owner Number') ?></th>
            <td><?= h($warehouseItemsInfo->info_owner_number) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Info Serial Number') ?></th>
            <td><?= h($warehouseItemsInfo->info_serial_number) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Contract Number') ?></th>
            <td><?= h($warehouseItemsInfo->contract_number) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Supplier Name') ?></th>
            <td><?= h($warehouseItemsInfo->supplier_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('State') ?></th>
            <td><?= h($warehouseItemsInfo->state) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Employer Number') ?></th>
            <td><?= h($warehouseItemsInfo->Employer_number) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($warehouseItemsInfo->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Info Item Name') ?></th>
            <td><?= $this->Number->format($warehouseItemsInfo->info_item_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Start Date Guarantee') ?></th>
            <td><?= h($warehouseItemsInfo->start_date_guarantee) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('End Date Guarantee') ?></th>
            <td><?= h($warehouseItemsInfo->end_date_guarantee) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($warehouseItemsInfo->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Updated') ?></th>
            <td><?= h($warehouseItemsInfo->updated) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Notes') ?></h4>
        <?= $this->Text->autoParagraph(h($warehouseItemsInfo->notes)); ?>
    </div>
</div>

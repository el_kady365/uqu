<?php
/**
  * @var \App\View\AppView $this
  */
?>

<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Recieved Orders Items Form7'), ['action' => 'edit', $recievedOrdersItemsForm7->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Recieved Orders Items Form7'), ['action' => 'delete', $recievedOrdersItemsForm7->id], ['confirm' => __('Are you sure you want to delete # {0}?', $recievedOrdersItemsForm7->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Recieved Orders Items Form7'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Recieved Orders Items Form7'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="recievedOrdersItemsForm7 view large-9 medium-8 columns content">
    <h3><?= h($recievedOrdersItemsForm7->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Order Item Number') ?></th>
            <td><?= h($recievedOrdersItemsForm7->order_item_number) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Order Item Name') ?></th>
            <td><?= h($recievedOrdersItemsForm7->order_item_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Order Item Type') ?></th>
            <td><?= h($recievedOrdersItemsForm7->order_item_type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Order Item Category') ?></th>
            <td><?= h($recievedOrdersItemsForm7->order_item_category) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Order Item Price') ?></th>
            <td><?= h($recievedOrdersItemsForm7->order_item_price) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Order Total Price') ?></th>
            <td><?= h($recievedOrdersItemsForm7->order_total_price) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Notes') ?></th>
            <td><?= h($recievedOrdersItemsForm7->notes) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($recievedOrdersItemsForm7->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Order Item Count') ?></th>
            <td><?= $this->Number->format($recievedOrdersItemsForm7->order_item_count) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Order Item Amount Recieved') ?></th>
            <td><?= $this->Number->format($recievedOrdersItemsForm7->order_item_amount_recieved) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($recievedOrdersItemsForm7->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Updated') ?></th>
            <td><?= h($recievedOrdersItemsForm7->updated) ?></td>
        </tr>
    </table>
</div>

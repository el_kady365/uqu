<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Warehouse Management'), ['action' => 'edit', $warehouseManagement->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Warehouse Management'), ['action' => 'delete', $warehouseManagement->id], ['confirm' => __('Are you sure you want to delete # {0}?', $warehouseManagement->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Warehouse Managements'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Warehouse Management'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="warehouseManagements view large-9 medium-8 columns content">
    <h3><?= h($warehouseManagement->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Management Number') ?></th>
            <td><?= h($warehouseManagement->management_number) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Title') ?></th>
            <td><?= h($warehouseManagement->title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($warehouseManagement->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($warehouseManagement->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Updated') ?></th>
            <td><?= h($warehouseManagement->updated) ?></td>
        </tr>
    </table>
</div>

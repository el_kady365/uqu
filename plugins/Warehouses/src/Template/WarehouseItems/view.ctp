<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Warehouse Item'), ['action' => 'edit', $warehouseItem->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Warehouse Item'), ['action' => 'delete', $warehouseItem->id], ['confirm' => __('Are you sure you want to delete # {0}?', $warehouseItem->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Warehouse Items'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Warehouse Item'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="warehouseItems view large-9 medium-8 columns content">
    <h3><?= h($warehouseItem->item_name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Item Number') ?></th>
            <td><?= h($warehouseItem->item_number) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($warehouseItem->id) ?></td>
        </tr>
      
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($warehouseItem->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Updated') ?></th>
            <td><?= h($warehouseItem->updated) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Item Name') ?></h4>
        <?= $this->Text->autoParagraph(h($warehouseItem->item_name)); ?>
    </div>
    <div class="row">
        <h4><?= __('Category Item Name') ?></h4>
        <?= $this->Text->autoParagraph(h($warehouseItem->category_item_name)); ?>
    </div>
</div>

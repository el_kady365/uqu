<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Warehouse Store'), ['action' => 'edit', $warehouseStore->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Warehouse Store'), ['action' => 'delete', $warehouseStore->id], ['confirm' => __('Are you sure you want to delete # {0}?', $warehouseStore->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Warehouse Stores'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Warehouse Store'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="warehouseStores view large-9 medium-8 columns content">
    <h3><?= h($warehouseStore->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Store Number') ?></th>
            <td><?= h($warehouseStore->store_number) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Store Title') ?></th>
            <td><?= h($warehouseStore->store_title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($warehouseStore->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($warehouseStore->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Updated') ?></th>
            <td><?= h($warehouseStore->updated) ?></td>
        </tr>
    </table>
</div>

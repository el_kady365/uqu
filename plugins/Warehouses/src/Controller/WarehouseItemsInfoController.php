<?php
namespace Warehouses\Controller;

use Warehouses\Controller\AppController;

/**
 * WarehouseItemsInfo Controller
 *
 * @property \Warehouses\Model\Table\WarehouseItemsInfoTable $WarehouseItemsInfo
 */
class WarehouseItemsInfoController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $warehouseItemsInfo = $this->paginate($this->WarehouseItemsInfo);

        $this->set(compact('warehouseItemsInfo'));
        $this->set('_serialize', ['warehouseItemsInfo']);
    }

    /**
     * View method
     *
     * @param string|null $id Warehouse Items Info id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $warehouseItemsInfo = $this->WarehouseItemsInfo->get($id, [
            'contain' => []
        ]);

        $this->set('warehouseItemsInfo', $warehouseItemsInfo);
        $this->set('_serialize', ['warehouseItemsInfo']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $warehouseItemsInfo = $this->WarehouseItemsInfo->newEntity();
        if ($this->request->is('post')) {
            $warehouseItemsInfo = $this->WarehouseItemsInfo->patchEntity($warehouseItemsInfo, $this->request->data);
            if ($this->WarehouseItemsInfo->save($warehouseItemsInfo)) {
                $this->Flash->success(__('The warehouse items info has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The warehouse items info could not be saved. Please, try again.'));
        }
        $this->set(compact('warehouseItemsInfo'));
        $this->set('_serialize', ['warehouseItemsInfo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Warehouse Items Info id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $warehouseItemsInfo = $this->WarehouseItemsInfo->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $warehouseItemsInfo = $this->WarehouseItemsInfo->patchEntity($warehouseItemsInfo, $this->request->data);
            if ($this->WarehouseItemsInfo->save($warehouseItemsInfo)) {
                $this->Flash->success(__('The warehouse items info has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The warehouse items info could not be saved. Please, try again.'));
        }
        $this->set(compact('warehouseItemsInfo'));
        $this->set('_serialize', ['warehouseItemsInfo']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Warehouse Items Info id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        //        $this->request->allowMethod(['post', 'delete']);
        $itemsInfo = $this->WarehouseItemsInfo->get($id);
        if ($this->WarehouseItemsInfo->delete($itemsInfo)) {
            $this->Flash->success(__('The Items Info  has been deleted.'));
        } else {
            $this->Flash->error(__('The Items Info  could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}

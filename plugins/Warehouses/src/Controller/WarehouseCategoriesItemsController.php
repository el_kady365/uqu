<?php
namespace Warehouses\Controller;

use Warehouses\Controller\AppController;

/**
 * WarehouseCategoriesItems Controller
 *
 * @property \Warehouses\Model\Table\WarehouseCategoriesItemsTable $WarehouseCategoriesItems
 */
class WarehouseCategoriesItemsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $warehouseCategoriesItems = $this->paginate($this->WarehouseCategoriesItems);

        $this->set(compact('warehouseCategoriesItems'));
        $this->set('_serialize', ['warehouseCategoriesItems']);
    }

    /**
     * View method
     *
     * @param string|null $id Warehouse Categories Item id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $warehouseCategoriesItem = $this->WarehouseCategoriesItems->get($id, [
            'contain' => []
        ]);

        $this->set('warehouseCategoriesItem', $warehouseCategoriesItem);
        $this->set('_serialize', ['warehouseCategoriesItem']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $warehouseCategoriesItem = $this->WarehouseCategoriesItems->newEntity();
        if ($this->request->is('post')) {
            $warehouseCategoriesItem = $this->WarehouseCategoriesItems->patchEntity($warehouseCategoriesItem, $this->request->data);
            if ($this->WarehouseCategoriesItems->save($warehouseCategoriesItem)) {
                $this->Flash->success(__('The warehouse categories item has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The warehouse categories item could not be saved. Please, try again.'));
        }
        $this->set(compact('warehouseCategoriesItem'));
        $this->set('_serialize', ['warehouseCategoriesItem']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Warehouse Categories Item id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $warehouseCategoriesItem = $this->WarehouseCategoriesItems->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $warehouseCategoriesItem = $this->WarehouseCategoriesItems->patchEntity($warehouseCategoriesItem, $this->request->data);
            if ($this->WarehouseCategoriesItems->save($warehouseCategoriesItem)) {
                $this->Flash->success(__('The warehouse categories item has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The warehouse categories item could not be saved. Please, try again.'));
        }
        $this->set(compact('warehouseCategoriesItem'));
        $this->set('_serialize', ['warehouseCategoriesItem']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Warehouse Categories Item id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
      //        $this->request->allowMethod(['post', 'delete']);
        $cateogriesItems = $this->WarehouseCategoryItems->get($id);
        if ($this->WarehouseCategoryItems->delete($cateogriesItems)) {
            $this->Flash->success(__('The Custody Employer  has been deleted.'));
        } else {
            $this->Flash->error(__('The Custody Employer  could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}

<?php
namespace Warehouses\Controller;

use Warehouses\Controller\AppController;

/**
 * WarehouseCustodyEmployers Controller
 *
 * @property \Warehouses\Model\Table\WarehouseCustodyEmployersTable $WarehouseCustodyEmployers
 */
class WarehouseCustodyEmployersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $warehouseCustodyEmployers = $this->paginate($this->WarehouseCustodyEmployers);

        $this->set(compact('warehouseCustodyEmployers'));
        $this->set('_serialize', ['warehouseCustodyEmployers']);
    }

    /**
     * View method
     *
     * @param string|null $id Warehouse Custody Employer id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $warehouseCustodyEmployer = $this->WarehouseCustodyEmployers->get($id, [
            'contain' => []
        ]);

        $this->set('warehouseCustodyEmployer', $warehouseCustodyEmployer);
        $this->set('_serialize', ['warehouseCustodyEmployer']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $warehouseCustodyEmployer = $this->WarehouseCustodyEmployers->newEntity();
        if ($this->request->is('post')) {
            $warehouseCustodyEmployer = $this->WarehouseCustodyEmployers->patchEntity($warehouseCustodyEmployer, $this->request->data);
            if ($this->WarehouseCustodyEmployers->save($warehouseCustodyEmployer)) {
                $this->Flash->success(__('The warehouse custody employer has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The warehouse custody employer could not be saved. Please, try again.'));
        }
        $this->set(compact('warehouseCustodyEmployer'));
        $this->set('_serialize', ['warehouseCustodyEmployer']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Warehouse Custody Employer id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $warehouseCustodyEmployer = $this->WarehouseCustodyEmployers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $warehouseCustodyEmployer = $this->WarehouseCustodyEmployers->patchEntity($warehouseCustodyEmployer, $this->request->data);
            if ($this->WarehouseCustodyEmployers->save($warehouseCustodyEmployer)) {
                $this->Flash->success(__('The warehouse custody employer has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The warehouse custody employer could not be saved. Please, try again.'));
        }
        $this->set(compact('warehouseCustodyEmployer'));
        $this->set('_serialize', ['warehouseCustodyEmployer']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Warehouse Custody Employer id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        //        $this->request->allowMethod(['post', 'delete']);
        $custodyEmployers = $this->WarehouseCustodyEmp->get($id);
        if ($this->WarehouseCustodyEmp->delete($custodyEmployers)) {
            $this->Flash->success(__('The Custody Employer  has been deleted.'));
        } else {
            $this->Flash->error(__('The Custody Employer  could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}

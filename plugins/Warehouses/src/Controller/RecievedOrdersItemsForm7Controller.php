<?php
namespace Warehouses\Controller;

use Warehouses\Controller\AppController;

/**
 * RecievedOrdersItemsForm7 Controller
 *
 * @property \Warehouses\Model\Table\RecievedOrdersItemsForm7Table $RecievedOrdersItemsForm7
 */
class RecievedOrdersItemsForm7Controller extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $recievedOrdersItemsForm7 = $this->paginate($this->RecievedOrdersItemsForm7);

        $this->set(compact('recievedOrdersItemsForm7'));
        $this->set('_serialize', ['recievedOrdersItemsForm7']);
    }

    /**
     * View method
     *
     * @param string|null $id Recieved Orders Items Form7 id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $recievedOrdersItemsForm7 = $this->RecievedOrdersItemsForm7->get($id, [
            'contain' => []
        ]);

        $this->set('recievedOrdersItemsForm7', $recievedOrdersItemsForm7);
        $this->set('_serialize', ['recievedOrdersItemsForm7']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $recievedOrdersItemsForm7 = $this->RecievedOrdersItemsForm7->newEntity();
        if ($this->request->is('post')) {
            $recievedOrdersItemsForm7 = $this->RecievedOrdersItemsForm7->patchEntity($recievedOrdersItemsForm7, $this->request->data);
            if ($this->RecievedOrdersItemsForm7->save($recievedOrdersItemsForm7)) {
                $this->Flash->success(__('The recieved orders items form7 has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The recieved orders items form7 could not be saved. Please, try again.'));
        }
        $this->set(compact('recievedOrdersItemsForm7'));
        $this->set('_serialize', ['recievedOrdersItemsForm7']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Recieved Orders Items Form7 id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $recievedOrdersItemsForm7 = $this->RecievedOrdersItemsForm7->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $recievedOrdersItemsForm7 = $this->RecievedOrdersItemsForm7->patchEntity($recievedOrdersItemsForm7, $this->request->data);
            if ($this->RecievedOrdersItemsForm7->save($recievedOrdersItemsForm7)) {
                $this->Flash->success(__('The recieved orders items form7 has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The recieved orders items form7 could not be saved. Please, try again.'));
        }
        $this->set(compact('recievedOrdersItemsForm7'));
        $this->set('_serialize', ['recievedOrdersItemsForm7']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Recieved Orders Items Form7 id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
       // $this->request->allowMethod(['post', 'delete']);
        $recievedOrdersItemsForm7 = $this->RecievedOrdersItemsForm7->get($id);
        if ($this->RecievedOrdersItemsForm7->delete($recievedOrdersItemsForm7)) {
            $this->Flash->success(__('The recieved orders items form7 has been deleted.'));
        } else {
            $this->Flash->error(__('The recieved orders items form7 could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}

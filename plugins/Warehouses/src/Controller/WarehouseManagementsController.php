<?php
namespace Warehouses\Controller;

use Warehouses\Controller\AppController;
/**
 * WarehouseManagements Controller
 *
 * @property \Warehouses\Model\Table\WarehouseManagementsTable $WarehouseManagements
 */
class WarehouseManagementsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $warehouseManagements = $this->paginate($this->WarehouseManagements);

        $this->set(compact('warehouseManagements'));
        $this->set('_serialize', ['warehouseManagements']);
    }

    /**
     * View method
     *
     * @param string|null $id Warehouse Management id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $warehouseManagement = $this->WarehouseManagements->get($id, [
            'contain' => []
        ]);

        $this->set('warehouseManagement', $warehouseManagement);
        $this->set('_serialize', ['warehouseManagement']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $warehouseManagement = $this->WarehouseManagements->newEntity();
        if ($this->request->is('post')) {
            $warehouseManagement = $this->WarehouseManagements->patchEntity($warehouseManagement, $this->request->data);
            if ($this->WarehouseManagements->save($warehouseManagement)) {
                $this->Flash->success(__('The warehouse management has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The warehouse management could not be saved. Please, try again.'));
        }
        $this->set(compact('warehouseManagement'));
        $this->set('_serialize', ['warehouseManagement']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Warehouse Management id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $warehouseManagement = $this->WarehouseManagements->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $warehouseManagement = $this->WarehouseManagements->patchEntity($warehouseManagement, $this->request->data);
            if ($this->WarehouseManagements->save($warehouseManagement)) {
                $this->Flash->success(__('The warehouse management has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The warehouse management could not be saved. Please, try again.'));
        }
        $this->set(compact('warehouseManagement'));
        $this->set('_serialize', ['warehouseManagement']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Warehouse Management id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        //        $this->request->allowMethod(['post', 'delete']);
        $managments = $this->WarehouseManagements->get($id);
        if ($this->WarehouseManagements->delete($managments)) {
            $this->Flash->success(__('The budget item has been deleted.'));
        } else {
            $this->Flash->error(__('The budget item could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}

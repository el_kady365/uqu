<?php
namespace Warehouses\Controller;

use Warehouses\Controller\AppController;

/**
 * WarehouseEmployers Controller
 *
 * @property \Warehouses\Model\Table\WarehouseEmployersTable $WarehouseEmployers
 */
class WarehouseEmployersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $warehouseEmployers = $this->paginate($this->WarehouseEmployers);

        $this->set(compact('warehouseEmployers'));
        $this->set('_serialize', ['warehouseEmployers']);
    }

    /**
     * View method
     *
     * @param string|null $id Warehouse Employer id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $warehouseEmployer = $this->WarehouseEmployers->get($id, [
            'contain' => []
        ]);

        $this->set('warehouseEmployer', $warehouseEmployer);
        $this->set('_serialize', ['warehouseEmployer']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $warehouseEmployer = $this->WarehouseEmployers->newEntity();
        if ($this->request->is('post')) {
            $warehouseEmployer = $this->WarehouseEmployers->patchEntity($warehouseEmployer, $this->request->data);
            if ($this->WarehouseEmployers->save($warehouseEmployer)) {
                $this->Flash->success(__('The warehouse employer has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The warehouse employer could not be saved. Please, try again.'));
        }
        $this->set(compact('warehouseEmployer'));
        $this->set('_serialize', ['warehouseEmployer']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Warehouse Employer id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $warehouseEmployer = $this->WarehouseEmployers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $warehouseEmployer = $this->WarehouseEmployers->patchEntity($warehouseEmployer, $this->request->data);
            if ($this->WarehouseEmployers->save($warehouseEmployer)) {
                $this->Flash->success(__('The warehouse employer has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The warehouse employer could not be saved. Please, try again.'));
        }
        $this->set(compact('warehouseEmployer'));
        $this->set('_serialize', ['warehouseEmployer']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Warehouse Employer id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
         //        $this->request->allowMethod(['post', 'delete']);
        $employers = $this->WarehouseEmployers->get($id);
        if ($this->WarehouseEmployers->delete($employers)) {
            $this->Flash->success(__('The Employer  has been deleted.'));
        } else {
            $this->Flash->error(__('The Employer  could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}

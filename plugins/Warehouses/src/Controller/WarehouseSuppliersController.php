<?php
namespace Warehouses\Controller;

use Warehouses\Controller\AppController;

/**
 * WarehouseSuppliers Controller
 *
 * @property \Warehouses\Model\Table\WarehouseSuppliersTable $WarehouseSuppliers
 */
class WarehouseSuppliersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $warehouseSuppliers = $this->paginate($this->WarehouseSuppliers);

        $this->set(compact('warehouseSuppliers'));
        $this->set('_serialize', ['warehouseSuppliers']);
    }

    /**
     * View method
     *
     * @param string|null $id Warehouse Supplier id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $warehouseSupplier = $this->WarehouseSuppliers->get($id, [
            'contain' => []
        ]);

        $this->set('warehouseSupplier', $warehouseSupplier);
        $this->set('_serialize', ['warehouseSupplier']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $warehouseSupplier = $this->WarehouseSuppliers->newEntity();
        if ($this->request->is('post')) {
            $warehouseSupplier = $this->WarehouseSuppliers->patchEntity($warehouseSupplier, $this->request->data);
            if ($this->WarehouseSuppliers->save($warehouseSupplier)) {
                $this->Flash->success(__('The warehouse supplier has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The warehouse supplier could not be saved. Please, try again.'));
        }
        $this->set(compact('warehouseSupplier'));
        $this->set('_serialize', ['warehouseSupplier']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Warehouse Supplier id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $warehouseSupplier = $this->WarehouseSuppliers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $warehouseSupplier = $this->WarehouseSuppliers->patchEntity($warehouseSupplier, $this->request->data);
            if ($this->WarehouseSuppliers->save($warehouseSupplier)) {
                $this->Flash->success(__('The warehouse supplier has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The warehouse supplier could not be saved. Please, try again.'));
        }
        $this->set(compact('warehouseSupplier'));
        $this->set('_serialize', ['warehouseSupplier']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Warehouse Supplier id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
       //        $this->request->allowMethod(['post', 'delete']);
        $suppliers = $this->WarehouseSuppliers->get($id);
        if ($this->WarehouseSuppliers->delete($suppliers)) {
            $this->Flash->success(__('The Warehouse suppliers has been deleted.'));
        } else {
            $this->Flash->error(__('The Warehouse suppliers could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}

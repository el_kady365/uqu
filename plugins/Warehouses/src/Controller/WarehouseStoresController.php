<?php
namespace Warehouses\Controller;

use Warehouses\Controller\AppController;

/**
 * WarehouseStores Controller
 *
 * @property \Warehouses\Model\Table\WarehouseStoresTable $WarehouseStores
 */
class WarehouseStoresController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $warehouseStores = $this->paginate($this->WarehouseStores);

        $this->set(compact('warehouseStores'));
        $this->set('_serialize', ['warehouseStores']);
    }

    /**
     * View method
     *
     * @param string|null $id Warehouse Store id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $warehouseStore = $this->WarehouseStores->get($id, [
            'contain' => []
        ]);

        $this->set('warehouseStore', $warehouseStore);
        $this->set('_serialize', ['warehouseStore']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $warehouseStore = $this->WarehouseStores->newEntity();
        if ($this->request->is('post')) {
            $warehouseStore = $this->WarehouseStores->patchEntity($warehouseStore, $this->request->data);
            if ($this->WarehouseStores->save($warehouseStore)) {
                $this->Flash->success(__('The warehouse store has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The warehouse store could not be saved. Please, try again.'));
        }
        $this->set(compact('warehouseStore'));
        $this->set('_serialize', ['warehouseStore']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Warehouse Store id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $warehouseStore = $this->WarehouseStores->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $warehouseStore = $this->WarehouseStores->patchEntity($warehouseStore, $this->request->data);
            if ($this->WarehouseStores->save($warehouseStore)) {
                $this->Flash->success(__('The warehouse store has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The warehouse store could not be saved. Please, try again.'));
        }
        $this->set(compact('warehouseStore'));
        $this->set('_serialize', ['warehouseStore']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Warehouse Store id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
       //        $this->request->allowMethod(['post', 'delete']);
        $managments = $this->WarehouseStores->get($id);
        if ($this->WarehouseStores->delete($managments)) {
            $this->Flash->success(__('The warehouse store item has been deleted.'));
        } else {
            $this->Flash->error(__('The warehouse store item could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}

<?php

namespace Warehouses\Controller;

use Warehouses\Controller\AppController;

/**
 * WarehouseItems Controller
 *
 * @property \Warehouses\Model\Table\WarehouseItemsTable $WarehouseItems
 */
class WarehouseItemsController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $warehouseItems = $this->paginate($this->WarehouseItems);

        $this->set(compact('warehouseItems'));
        $this->set('_serialize', ['warehouseItems']);
    }

    /**
     * View method
     *
     * @param string|null $id Warehouse Item id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $warehouseItem = $this->WarehouseItems->get($id, [
            'contain' => []
        ]);

        $this->set('warehouseItem', $warehouseItem);
        $this->set('_serialize', ['warehouseItem']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $warehouseItem = $this->WarehouseItems->newEntity();
        if ($this->request->is('post')) {
            $warehouseItem = $this->WarehouseItems->patchEntity($warehouseItem, $this->request->data);
            if ($this->WarehouseItems->save($warehouseItem)) {
                $this->Flash->success(__('The warehouse item has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The warehouse item could not be saved. Please, try again.'));
        }
        $this->set(compact('warehouseItem'));
        $this->set('_serialize', ['warehouseItem']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Warehouse Item id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $warehouseItem = $this->WarehouseItems->get($id, [
            'contain' => []
        ]);
//        debug($warehouseItem);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $warehouseItem = $this->WarehouseItems->patchEntity($warehouseItem, $this->request->data);
            if ($this->WarehouseItems->save($warehouseItem)) {
                $this->Flash->success(__('The warehouse item has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The warehouse item could not be saved. Please, try again.'));
        }
        $this->set(compact('warehouseItem'));
        $this->set('_serialize', ['warehouseItem']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Warehouse Item id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        //        $this->request->allowMethod(['post', 'delete']);
        $items = $this->WarehouseItem->get($id);
        if ($this->WarehouseItem->delete($items)) {
            $this->Flash->success(__('The Warehouse  item has been deleted.'));
        } else {
            $this->Flash->error(__('The Warehouse item could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

}

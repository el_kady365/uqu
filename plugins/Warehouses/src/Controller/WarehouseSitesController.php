<?php
namespace Warehouses\Controller;

use Warehouses\Controller\AppController;

/**
 * WarehouseSites Controller
 *
 * @property \Warehouses\Model\Table\WarehouseSitesTable $WarehouseSites
 */
class WarehouseSitesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $warehouseSites = $this->paginate($this->WarehouseSites);

        $this->set(compact('warehouseSites'));
        $this->set('_serialize', ['warehouseSites']);
    }

    /**
     * View method
     *
     * @param string|null $id Warehouse Site id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $warehouseSite = $this->WarehouseSites->get($id, [
            'contain' => []
        ]);

        $this->set('warehouseSite', $warehouseSite);
        $this->set('_serialize', ['warehouseSite']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $warehouseSite = $this->WarehouseSites->newEntity();
        if ($this->request->is('post')) {
            $warehouseSite = $this->WarehouseSites->patchEntity($warehouseSite, $this->request->data);
            if ($this->WarehouseSites->save($warehouseSite)) {
                $this->Flash->success(__('The warehouse site has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The warehouse site could not be saved. Please, try again.'));
        }
        $this->set(compact('warehouseSite'));
        $this->set('_serialize', ['warehouseSite']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Warehouse Site id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $warehouseSite = $this->WarehouseSites->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $warehouseSite = $this->WarehouseSites->patchEntity($warehouseSite, $this->request->data);
            if ($this->WarehouseSites->save($warehouseSite)) {
                $this->Flash->success(__('The warehouse site has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The warehouse site could not be saved. Please, try again.'));
        }
        $this->set(compact('warehouseSite'));
        $this->set('_serialize', ['warehouseSite']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Warehouse Site id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
       //        $this->request->allowMethod(['post', 'delete']);
        $sites = $this->WarehouseSites->get($id);
        if ($this->WarehouseSites->delete($sites)) {
            $this->Flash->success(__('The warehouse site item has been deleted.'));
        } else {
            $this->Flash->error(__('The warehouse site item could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    
    }
}

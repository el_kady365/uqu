<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace Warehouses\Controller;

use App\Controller\AppController as BaseController;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\I18n\I18n;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends BaseController {

    public $config = [];
    public $lang = '';

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize() {
        parent::initialize();

        $this->loadComponent('Paginator');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->load_config();
        $this->_setLanguage('ar_AR');
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event) {
        parent::beforeRender($event);
        if (!array_key_exists('_serialize', $this->viewVars) &&
                in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
        $currentDepartment['url'] = '/warehouses';
        $currentDepartment['text'] = __('Warehouses');
        $this->__change_title($currentDepartment['text']);
        $this->set('currentDepartment', $currentDepartment);
        $this->set('menuId', 'FinancialDepartment');

      
    }

    private function __change_title($current_department = false) {
        $params = $this->request->params;
        $titleArr = array();
        $titleArr[] = $this->config['site_name_' . $this->lang];
        if ($current_department) {
            $titleArr[] = $current_department;
        }
        if (!empty($this->pageTitle)) {
            $titleArr[] = $this->pageTitle;
        } else {
            $titleArr[] = __(Inflector::humanize(Inflector::underscore($params['controller'])));
            if ($params['action'] != 'index') {
                $titleArr[] = __(Inflector::camelize($params['action']));
            }
        }

        $this->pageTitle = implode(' - ', $titleArr);
        $this->set('title_for_layout', $this->pageTitle);
    }

    

    public function updateButtons($controller, $action, $id) {
        $buttons = Configure::read("Buttons.{$controller}.{$action}");
//        debug($buttons);
        foreach ($buttons as $key => $button) {
            Configure::write("Buttons.$controller.$action.$key.url", $button['url'] . '/' . $id);
        }
    }

}

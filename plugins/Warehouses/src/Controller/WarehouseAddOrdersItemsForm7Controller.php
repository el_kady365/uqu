<?php
namespace Warehouses\Controller;

use Warehouses\Controller\AppController;

/**
 * WarehouseAddOrdersItemsForm7 Controller
 *
 * @property \Warehouses\Model\Table\WarehouseAddOrdersItemsForm7Table $WarehouseAddOrdersItemsForm7
 */
class WarehouseAddOrdersItemsForm7Controller extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $warehouseAddOrdersItemsForm7 = $this->paginate($this->WarehouseAddOrdersItemsForm7);

        $this->set(compact('warehouseAddOrdersItemsForm7'));
        $this->set('_serialize', ['warehouseAddOrdersItemsForm7']);
    }

    /**
     * View method
     *
     * @param string|null $id Warehouse Add Orders Items Form7 id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $warehouseAddOrdersItemsForm7 = $this->WarehouseAddOrdersItemsForm7->get($id, [
            'contain' => []
        ]);

        $this->set('warehouseAddOrdersItemsForm7', $warehouseAddOrdersItemsForm7);
        $this->set('_serialize', ['warehouseAddOrdersItemsForm7']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $warehouseAddOrdersItemsForm7 = $this->WarehouseAddOrdersItemsForm7->newEntity();
        if ($this->request->is('post')) {
            $warehouseAddOrdersItemsForm7 = $this->WarehouseAddOrdersItemsForm7->patchEntity($warehouseAddOrdersItemsForm7, $this->request->data);
            if ($this->WarehouseAddOrdersItemsForm7->save($warehouseAddOrdersItemsForm7)) {
                $this->Flash->success(__('The warehouse add orders items form7 has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The warehouse add orders items form7 could not be saved. Please, try again.'));
        }
        $this->set(compact('warehouseAddOrdersItemsForm7'));
        $this->set('_serialize', ['warehouseAddOrdersItemsForm7']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Warehouse Add Orders Items Form7 id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $warehouseAddOrdersItemsForm7 = $this->WarehouseAddOrdersItemsForm7->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $warehouseAddOrdersItemsForm7 = $this->WarehouseAddOrdersItemsForm7->patchEntity($warehouseAddOrdersItemsForm7, $this->request->data);
            if ($this->WarehouseAddOrdersItemsForm7->save($warehouseAddOrdersItemsForm7)) {
                $this->Flash->success(__('The warehouse add orders items form7 has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The warehouse add orders items form7 could not be saved. Please, try again.'));
        }
        $this->set(compact('warehouseAddOrdersItemsForm7'));
        $this->set('_serialize', ['warehouseAddOrdersItemsForm7']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Warehouse Add Orders Items Form7 id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        
         //        $this->request->allowMethod(['post', 'delete']);
        $warehouseAddOrdersItemsForm7 = $this->WarehouseAddOrdersItemsForm7->get($id);
        if ($this->WarehouseAddOrdersItemsForm7->delete($warehouseAddOrdersItemsForm7)) {
            $this->Flash->success(__('The warehouse add orders items form7 has been deleted.'));
        } else {
            $this->Flash->error(__('The warehouse add orders items form7 could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}

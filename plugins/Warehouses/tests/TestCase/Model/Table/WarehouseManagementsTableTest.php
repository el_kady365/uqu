<?php
namespace warehouses\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use warehouses\Model\Table\WarehouseManagementsTable;

/**
 * warehouses\Model\Table\WarehouseManagementsTable Test Case
 */
class WarehouseManagementsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Warehouses\Model\Table\WarehouseManagementsTable
     */
    public $WarehouseManagements;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.warehouses.warehouse_managements'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('WarehouseManagements') ? [] : ['className' => 'warehouses\Model\Table\WarehouseManagementsTable'];
        $this->WarehouseManagements = TableRegistry::get('WarehouseManagements', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->WarehouseManagements);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

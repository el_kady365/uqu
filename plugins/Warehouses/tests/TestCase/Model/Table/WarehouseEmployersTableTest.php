<?php
namespace Warehouses\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Warehouses\Model\Table\WarehouseEmployersTable;

/**
 * Warehouses\Model\Table\WarehouseEmployersTable Test Case
 */
class WarehouseEmployersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Warehouses\Model\Table\WarehouseEmployersTable
     */
    public $WarehouseEmployers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.warehouses.warehouse_employers'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('WarehouseEmployers') ? [] : ['className' => 'Warehouses\Model\Table\WarehouseEmployersTable'];
        $this->WarehouseEmployers = TableRegistry::get('WarehouseEmployers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->WarehouseEmployers);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

<?php
namespace Warehouses\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Warehouses\Model\Table\WarehouseSuppliersTable;

/**
 * Warehouses\Model\Table\WarehouseSuppliersTable Test Case
 */
class WarehouseSuppliersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Warehouses\Model\Table\WarehouseSuppliersTable
     */
    public $WarehouseSuppliers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.warehouses.warehouse_suppliers'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('WarehouseSuppliers') ? [] : ['className' => 'Warehouses\Model\Table\WarehouseSuppliersTable'];
        $this->WarehouseSuppliers = TableRegistry::get('WarehouseSuppliers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->WarehouseSuppliers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

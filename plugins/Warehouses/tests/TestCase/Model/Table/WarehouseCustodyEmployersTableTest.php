<?php
namespace Warehouses\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Warehouses\Model\Table\WarehouseCustodyEmployersTable;

/**
 * Warehouses\Model\Table\WarehouseCustodyEmployersTable Test Case
 */
class WarehouseCustodyEmployersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Warehouses\Model\Table\WarehouseCustodyEmployersTable
     */
    public $WarehouseCustodyEmployers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.warehouses.warehouse_custody_employers'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('WarehouseCustodyEmployers') ? [] : ['className' => 'Warehouses\Model\Table\WarehouseCustodyEmployersTable'];
        $this->WarehouseCustodyEmployers = TableRegistry::get('WarehouseCustodyEmployers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->WarehouseCustodyEmployers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

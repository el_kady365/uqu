<?php
namespace Warehouses\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Warehouses\Model\Table\WarehouseCategoryItemsTable;

/**
 * Warehouses\Model\Table\WarehouseCategoryItemsTable Test Case
 */
class WarehouseCategoryItemsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Warehouses\Model\Table\WarehouseCategoryItemsTable
     */
    public $WarehouseCategoryItems;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.warehouses.warehouse_category_items'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('WarehouseCategoryItems') ? [] : ['className' => 'Warehouses\Model\Table\WarehouseCategoryItemsTable'];
        $this->WarehouseCategoryItems = TableRegistry::get('WarehouseCategoryItems', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->WarehouseCategoryItems);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

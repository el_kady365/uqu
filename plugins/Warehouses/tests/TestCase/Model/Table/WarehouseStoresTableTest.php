<?php
namespace Warehouses\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Warehouses\Model\Table\WarehouseStoresTable;

/**
 * Warehouses\Model\Table\WarehouseStoresTable Test Case
 */
class WarehouseStoresTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Warehouses\Model\Table\WarehouseStoresTable
     */
    public $WarehouseStores;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.warehouses.warehouse_stores'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('WarehouseStores') ? [] : ['className' => 'Warehouses\Model\Table\WarehouseStoresTable'];
        $this->WarehouseStores = TableRegistry::get('WarehouseStores', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->WarehouseStores);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

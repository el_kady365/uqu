<?php

use Cake\Routing\Route\DashedRoute;
use Cake\Routing\Router;

Router::plugin('Warehouses', ['path' => '/warehouses'], function ($routes) {
    $routes->connect('/', ['controller' => 'Pages', 'action' => 'home']);

    $routes->fallbacks(DashedRoute::class);
});

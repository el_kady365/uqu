/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  mohammed
 * Created: Feb 21, 2017
 */


ALTER TABLE `financial_commitment_letters`
ADD COLUMN `transaction_id`  int(11) NULL AFTER `receiver`;

ALTER TABLE `transactions`
DROP COLUMN `financial_commitment_letter_id`,
MODIFY COLUMN `trust_order_id`  int(11) NULL DEFAULT NULL AFTER `purpose_id`;

ALTER TABLE `financial_commitment_letters`
ADD COLUMN `attachments`  varchar(255) NULL AFTER `transaction_id`;

ALTER TABLE `transactions`
MODIFY COLUMN `date`  date NULL DEFAULT NULL AFTER `transactions_type_id`,
MODIFY COLUMN `next_exchange_date`  date NULL DEFAULT NULL AFTER `contract_id`;

ALTER TABLE `transactions`
CHANGE COLUMN `user_id` `user_full_name`  varchar(255) NULL DEFAULT NULL AFTER `status`,
ADD COLUMN `last_update_by`  varchar(255) NULL AFTER `user_full_name`;


ALTER TABLE `trust_orders`
ADD COLUMN `transaction_id`  int NULL AFTER `send_trust_order`;

ALTER TABLE `contracts`
ADD COLUMN `transaction_id`  int NULL AFTER `other_sent`;

ALTER TABLE `contracts`
CHANGE COLUMN `contractor` `signed_by`  varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL AFTER `date`;

ALTER TABLE `contracts`
CHANGE COLUMN `signature` `signatured`  tinyint(1) NULL DEFAULT NULL AFTER `send_to_revision`,
ADD COLUMN `commitment_received`  tinyint(1) NULL AFTER `signatured`;


/* NEW */

ALTER TABLE `transactions`
MODIFY COLUMN `date`  varchar(50) NULL DEFAULT NULL AFTER `transactions_type_id`,
MODIFY COLUMN `next_exchange_date`  varchar(50) NULL DEFAULT NULL AFTER `purpose_id`;

ALTER TABLE `contracts`
MODIFY COLUMN `date`  varchar(50) NULL DEFAULT NULL AFTER `contract_no`;

ALTER TABLE `financial_commitment_letters`
MODIFY COLUMN `date`  varchar(50) NULL DEFAULT NULL AFTER `financial_commitment_no`;

ALTER TABLE `trust_orders`
MODIFY COLUMN `date`  varchar(50) NULL DEFAULT NULL AFTER `trust_order_no`;


--INVOICES
CREATE TABLE `invoices` (
  `id` int(11) NOT NULL,
  `invoice_islamic_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invoice_gorgian_date` date DEFAULT NULL,
  `invoice_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `addresses_count` int(11) DEFAULT NULL,
  `copies_count` int(11) DEFAULT NULL,
  `starting_vertical_coding_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` float DEFAULT NULL,
  `shipping_invoice_gorgian_date` date DEFAULT NULL,
  `shipping_invoice_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_price` float DEFAULT NULL,
  `notes` text COLLATE utf8_unicode_ci,
  `pdf_file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `trust_supplier_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `trust_financial_year` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `trust_order_id` int(11) DEFAULT NULL,
  `trust_approximately_price` float DEFAULT NULL,
  `trust_container_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `trust_beneficiary` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `user_full_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_update_by` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci
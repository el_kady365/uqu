/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Digital-Lines
 * Created: Mar 30, 2017
 */
-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 30, 2017 at 04:48 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `uqu_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `financial_attachments`
--

CREATE TABLE `financial_attachments` (
  `id` int(11) NOT NULL,
  `financial_item_id` int(11) NOT NULL,
  `attachements_name` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `attachements_number` varchar(500) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `financial_items`
--

CREATE TABLE `financial_items` (
  `id` int(11) NOT NULL,
  `order_number` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `financial_amount` varchar(500) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `financial_attachments`
--
ALTER TABLE `financial_attachments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `attachements_id` (`financial_item_id`);

--
-- Indexes for table `financial_items`
--
ALTER TABLE `financial_items`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `financial_attachments`
--

--
-- Constraints for dumped tables
--

--
-- Constraints for table `financial_attachments`
--
ALTER TABLE `financial_attachments`
  ADD CONSTRAINT `financial_attachments_ibfk_1` FOREIGN KEY (`financial_item_id`) REFERENCES `financial_items` (`id`);

